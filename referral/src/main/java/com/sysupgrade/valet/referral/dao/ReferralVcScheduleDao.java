package com.sysupgrade.valet.referral.dao;

import com.sysupgrade.valet.referral.orm.ReferralVcSchedule;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ReferralVcScheduleDao extends PagingAndSortingRepository<ReferralVcSchedule, String> {

    @Query(nativeQuery = true,
            value = "SELECT * from referral_vc_schedule " +
                    "where last_schedule_at < DATE_SUB(CURDATE(), INTERVAL :number_of_days DAY)")
    Optional<ReferralVcSchedule> findOldSchedule(@Param("number_of_days") int numberOfDays);
}
