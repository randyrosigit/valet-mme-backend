package com.sysupgrade.valet.referral.dao;


import com.sysupgrade.valet.referral.orm.ReferralMarketing;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ReferralMarketingDao extends PagingAndSortingRepository<ReferralMarketing, String> {
}
