package com.sysupgrade.valet.referral.dao;

import com.sysupgrade.valet.referral.orm.ReferralTransaction;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ReferralTransactionsDao extends PagingAndSortingRepository<ReferralTransaction, String> {
}
