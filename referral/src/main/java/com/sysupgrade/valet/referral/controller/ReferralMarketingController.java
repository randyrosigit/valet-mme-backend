package com.sysupgrade.valet.referral.controller;

import com.sysupgrade.valet.referral.dao.ReferralMarketingDao;
import com.sysupgrade.valet.referral.orm.ReferralMarketing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api/referralMarketing/marketing")
public class ReferralMarketingController {

    private ReferralMarketingDao dao;

    @GetMapping("/")
    public Page<ReferralMarketing> list(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @PutMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public ReferralMarketing save(@RequestBody @Valid ReferralMarketing referralMarketing) {
        referralMarketing.setStatus(ReferralMarketing.STATUS_ACTIVE);
        dao.save(referralMarketing);
        return referralMarketing;
    }

    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public ReferralMarketing update(@RequestBody @Valid ReferralMarketing referralMarketing) {
        dao.save(referralMarketing);
        return referralMarketing;
    }

    @PostMapping("/delete")
    @ResponseStatus(HttpStatus.CREATED)
    public ReferralMarketing delete(@RequestBody ReferralMarketing referralMarketing) {
        referralMarketing.setStatus(ReferralMarketing.STATUS_DELETE);
        dao.save(referralMarketing);
        return referralMarketing;
    }

    @GetMapping("/{id}")
    public Optional<ReferralMarketing> get(@PathVariable String id) {
        return dao.findById(id);
    }
}
