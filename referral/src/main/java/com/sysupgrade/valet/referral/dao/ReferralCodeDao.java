package com.sysupgrade.valet.referral.dao;

import com.sysupgrade.valet.referral.orm.ReferralCode;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ReferralCodeDao extends PagingAndSortingRepository<ReferralCode, String> {

    Optional<ReferralCode> findByUserId(String userId);

    Optional<ReferralCode> findByCode(String code);
}
