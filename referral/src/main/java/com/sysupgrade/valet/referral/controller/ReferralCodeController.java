package com.sysupgrade.valet.referral.controller;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.referral.model.referralcode.CreateReferralCodeForm;
import com.sysupgrade.valet.referral.model.referralcode.FindByCodeForm;
import com.sysupgrade.valet.referral.model.referralcode.GetReferralCodeForm;
import com.sysupgrade.valet.referral.orm.ReferralCode;
import com.sysupgrade.valet.referral.service.ReferralCodeService;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api/referral/code/")
public class ReferralCodeController {

    @Autowired
    private ReferralCodeService referralCodeService;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @PreAuthorize("hasAnyAuthority('USER')")
    @PutMapping("generate")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CreateReferralCodeForm> generateCode() {

        CreateReferralCodeForm form = new CreateReferralCodeForm();
        String userId = authenticationFacade.getMyFace().getId();
        form.setUserId(userId);

        try {
            ReferralCode referralCode = referralCodeService.generate(form);
            form.setReferralCode(referralCode);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @GetMapping("public/find-by-code")
    public ResponseEntity<FindByCodeForm> findByCode(@Valid FindByCodeForm form,
                                                     BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            Optional<ReferralCode> findByCodeFormOptional = referralCodeService.findByCode(form.getCode());
            if(!findByCodeFormOptional.isPresent()) {
                throw new Exception(ErrorHelper.DATA_NOT_FOUND);
            }

            form.setReferralCode(findByCodeFormOptional.get());
            return ResponseEntity.ok(form);
        }
        catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("get")
    public ResponseEntity<GetReferralCodeForm> getMyReferralCode() {
        GetReferralCodeForm form = new GetReferralCodeForm();
        form.setUserId(authenticationFacade.getMyFace().getId());
        try {
            Optional<ReferralCode> referralCode = referralCodeService.getMyReferralCode(form);
            referralCode.ifPresent(
                    value -> form.setReferralCode(value)
            );
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
        return ResponseEntity.ok(form);
    }
}