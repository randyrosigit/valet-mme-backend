package com.sysupgrade.valet.referral.service;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.referral.dao.ReferralCodeDao;
import com.sysupgrade.valet.referral.dao.ReferralDao;
import com.sysupgrade.valet.referral.model.referal.CreateReferralForm;
import com.sysupgrade.valet.referral.orm.Referral;
import com.sysupgrade.valet.referral.orm.ReferralCode;
import com.sysupgrade.valet.wallet.dao.WalletCreditDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ReferralService {

    @Autowired
    private ReferralDao dao;

    @Autowired
    ReferralCodeDao referralCodeDao;

    @Autowired
    ReferralLevelService referralLevelService;

    @Autowired
    WalletCreditDao walletCreditDao;

    @Transactional
    public Referral create(CreateReferralForm form) throws Exception {

        Optional<ReferralCode> referralCodeOptional = referralCodeDao.findByCode(form.getUsedCode());

        if(!referralCodeOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }
        ReferralCode referralCode = referralCodeOptional.get();
        Referral referral = new Referral();
        referral.setSourceUserId(referralCode.getUserId());
        referral.setUsedCode(referralCode.getCode());
        referral.setReferredUserId(form.getReferredUserId());
        referral.setStatus(Referral.STATUS_ACTIVE);

        dao.save(referral);

        return referral;
    }

    public Optional<Referral> getUpline(String userId) {
        return dao.findByReferredUserId(userId);
    }

    public Page<Referral> list(String userId, Pageable pageable) {
        return dao.findBySourceUserId(userId, pageable);
    }

}