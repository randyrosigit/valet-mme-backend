package com.sysupgrade.valet.referral.model.referal;

import com.sysupgrade.valet.user.model.user.PublicUser;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ListReferralForm {

    private String userId;

    private String error;

    private Page<PublicUser> publicUsers;

    private Page<WalletCredit> walletCredits;
}
