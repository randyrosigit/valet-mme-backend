package com.sysupgrade.valet.referral.model.referralcode;

import com.sysupgrade.valet.referral.orm.ReferralCode;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FindByCodeForm {

    @NotNull
    private String code;

    private String error;

    private ReferralCode referralCode;
}
