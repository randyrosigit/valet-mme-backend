package com.sysupgrade.valet.referral.model.referrallevel;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GiveRewardForm {

    private String error;

    @NotNull
    private String userId;
}
