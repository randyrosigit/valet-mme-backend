package com.sysupgrade.valet.referral.service;

import com.sysupgrade.valet.referral.dao.ReferralDao;
import com.sysupgrade.valet.referral.dao.ReferralLevelDao;
import com.sysupgrade.valet.referral.model.referrallevel.CreateReferralLevelForm;
import com.sysupgrade.valet.referral.model.referrallevel.ListReferralLevelForm;
import com.sysupgrade.valet.referral.orm.Referral;
import com.sysupgrade.valet.referral.orm.ReferralLevel;
import com.sysupgrade.valet.wallet.model.credit.AdjustmentForm;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import com.sysupgrade.valet.wallet.service.WalletCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ReferralLevelService {

    @Autowired
    ReferralLevelDao levelDao;

    @Autowired
    ReferralDao referralDao;

    @Autowired
    WalletCreditService walletCreditService;

    @Transactional
    public ReferralLevel create(CreateReferralLevelForm form) {

        ReferralLevel referralLevel = new ReferralLevel();

        referralLevel.setId(form.getId());
        referralLevel.setStatus(ReferralLevel.STATUS_ACTIVE);
        referralLevel.setLevel(form.getLevel());
        referralLevel.setCurrencyId(form.getCurrencyId());
        referralLevel.setReturnAmount(form.getReturnAmount());
        return levelDao.save(referralLevel);

    }

    @Transactional
    public void giveRewardByApprovedUser(String userId) {
        Optional<Referral> referralOptional;

        referralOptional = getUserReferral(userId);
        if(!referralOptional.isPresent()) {
            return;
        }

        List<ReferralLevel> levels = levelDao.findByLevel(1);
        for( ReferralLevel level : levels) {
            adjustmentWalletCreditUser(level.getReturnAmount(),
                    level.getCurrencyId(),
                    referralOptional.get().getSourceUserId());
        }

        referralOptional = getUserReferral(referralOptional.get().getSourceUserId());
        if(!referralOptional.isPresent()) {
            return;
        }

        levels = levelDao.findByLevel(2);
        for( ReferralLevel level : levels) {
            adjustmentWalletCreditUser(level.getReturnAmount(),
                    level.getCurrencyId(),
                    referralOptional.get().getSourceUserId());
        }
    }

    private void adjustmentWalletCreditUser(BigDecimal amount, String currencyId, String userId) {
        AdjustmentForm form = new AdjustmentForm();
        form.setAmount(amount);
        form.setCurrencyId(currencyId);
        form.setUserId(userId);
        walletCreditService.adjustment(form, WalletCredit.REWARD_ACTIVITY_TYPE);
    }

    private Optional<Referral> getUserReferral(String referralUserId) {
        return referralDao.findByReferredUserId(referralUserId);
    }

    public Page<ReferralLevel> list(ListReferralLevelForm form, Pageable pageable) {
        return levelDao.findAll(pageable);
    }
}