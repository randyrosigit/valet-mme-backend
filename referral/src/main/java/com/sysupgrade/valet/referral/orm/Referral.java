package com.sysupgrade.valet.referral.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "referral")
@Data
public class Referral {

    public final static int STATUS_DELETE = 0;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_PENDING = 11;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "source_user_id")
    private String sourceUserId;

    @NotNull
    @Column(name = "referred_user_id")
    private String referredUserId;

    @Column(name = "used_code")
    private String usedCode;

    @NotNull
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;

}
