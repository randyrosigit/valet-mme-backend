package com.sysupgrade.valet.referral.model.referrallevel;

import com.sysupgrade.valet.referral.orm.ReferralLevel;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ListReferralLevelForm {

    private String error;

    private Page<ReferralLevel> referralLevels;
}
