package com.sysupgrade.valet.referral.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "referral_level")
@Data
public class ReferralLevel {

    public final static int STATUS_DELETE = 0;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_PENDING = 11;

    @Id
    private int id;

    @Column(nullable = false)
    private int level;

    @Column(name = "currency_id", nullable = false)
    private String currencyId;

    @Column(name = "return_amount", nullable = false)
    private BigDecimal returnAmount;

    @Column(nullable = false)
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;
}
