package com.sysupgrade.valet.referral.service;

import com.sysupgrade.valet.referral.dao.ReferralCodeDao;
import com.sysupgrade.valet.referral.model.referralcode.CreateReferralCodeForm;
import com.sysupgrade.valet.referral.model.referralcode.GetReferralCodeForm;
import com.sysupgrade.valet.referral.orm.ReferralCode;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ReferralCodeService {

    @Autowired
    private ReferralCodeDao dao;

    public Optional<ReferralCode> findByCode(String code) {
        return dao.findByCode(code);
    }

    @Transactional
    public ReferralCode generate(CreateReferralCodeForm form) {

        String code = findUniqueCode();

        Optional<ReferralCode> referralCodeOptional = dao.findByUserId(form.getUserId());

        ReferralCode referralCode;
        if (!referralCodeOptional.isPresent()) {
            referralCode = new ReferralCode();
            referralCode.setUserId(form.getUserId());
            referralCode.setCode(code);
            referralCode.setStatus(ReferralCode.STATUS_ACTIVE);
        }else {
            referralCode = referralCodeOptional.get();
            referralCode.setCode(code);
        }

        dao.save(referralCode);
        return referralCode;
    }

    public Optional<ReferralCode> getMyReferralCode(GetReferralCodeForm form){
        return dao.findByUserId(form.getUserId());
    }

    private String findUniqueCode() {
        while (true) {
            String code = RandomString.make(6).toLowerCase();
            Optional<ReferralCode> referralCodeOptional =
                    dao.findByCode(code);
            if(!referralCodeOptional.isPresent()) {
               return code;
            }
        }
    }
}