package com.sysupgrade.valet.referral.service;

import com.sysupgrade.valet.referral.dao.ReferralVcScheduleDao;
import com.sysupgrade.valet.referral.orm.ReferralVcSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReferralVcService {

    @Autowired
    private ReferralVcScheduleDao referralVcScheduleDao;

    public Optional<ReferralVcSchedule> findOldSchedule(int numberOfDays) {
        return referralVcScheduleDao.findOldSchedule(numberOfDays);
    }

}
