package com.sysupgrade.valet.referral.model.referralcode;

import com.sysupgrade.valet.referral.orm.ReferralCode;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
public class GetReferralCodeForm {

    @NotNull
    @Column(nullable = false)
    private String userId;

    private ReferralCode referralCode;

    private String error;

}
