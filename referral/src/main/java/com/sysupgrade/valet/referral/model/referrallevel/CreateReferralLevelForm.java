package com.sysupgrade.valet.referral.model.referrallevel;

import com.sysupgrade.valet.referral.orm.ReferralLevel;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CreateReferralLevelForm {

    private String error;

    private int id;

    @Min(1)
    @Max(2)
    private int level;

    @NotNull
    private String currencyId;

    @NotNull
    private BigDecimal returnAmount;

    private ReferralLevel referralLevel;

}
