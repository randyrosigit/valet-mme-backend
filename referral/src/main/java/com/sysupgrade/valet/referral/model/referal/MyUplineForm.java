package com.sysupgrade.valet.referral.model.referal;

import com.sysupgrade.valet.user.model.user.PublicUser;
import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MyUplineForm {

    private PublicUser user;

    private String error;
}
