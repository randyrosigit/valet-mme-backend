package com.sysupgrade.valet.referral.controller;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.referral.model.referrallevel.CreateReferralLevelForm;
import com.sysupgrade.valet.referral.model.referrallevel.GiveRewardForm;
import com.sysupgrade.valet.referral.model.referrallevel.ListReferralLevelForm;
import com.sysupgrade.valet.referral.orm.ReferralLevel;
import com.sysupgrade.valet.referral.service.ReferralLevelService;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/referral/level")
public class ReferralLevelController {

    @Autowired
    private ReferralLevelService referralLevelService;

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CreateReferralLevelForm> create(@RequestBody @Valid CreateReferralLevelForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            ReferralLevel referralLevel = referralLevelService.create(form);
            form.setReferralLevel(referralLevel);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("give-reward-by-approved-user")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GiveRewardForm> giveRewardByApprovedUser(@RequestBody @Valid GiveRewardForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            referralLevelService.giveRewardByApprovedUser(form.getUserId());
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ListReferralLevelForm> listReferralLevel(@Valid ListReferralLevelForm form,
                                                                BindingResult bindingResult,
                                                                Pageable pageable) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            Page<ReferralLevel> list = referralLevelService.list(form, pageable);
            form.setReferralLevels(list);

        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }
}
