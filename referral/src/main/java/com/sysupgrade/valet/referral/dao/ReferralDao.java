package com.sysupgrade.valet.referral.dao;

import com.sysupgrade.valet.referral.orm.Referral;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface ReferralDao extends PagingAndSortingRepository<Referral, String> {

    Page<Referral> findBySourceUserId(String UserId, Pageable pageable);

    Optional<Referral> findBySourceUserId(String sourceUserId);

    Optional<Referral> findByReferredUserId(String referralUserId);

    List<Referral> findByReferredUserIdAndStatus(String userId, Integer status);
}
