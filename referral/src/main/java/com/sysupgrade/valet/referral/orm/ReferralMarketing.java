package com.sysupgrade.valet.referral.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "referral_marketing")
@Data
public class ReferralMarketing {

    public final static int STATUS_DELETE = 0;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_PENDING = 11;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "currency_id")
    private String currencyId;

    @NotNull
    @Column(name = "total_credit")
    private BigDecimal totalCredit;

    @NotNull
    @Column(name = "total_currency_credit")
    private BigDecimal totalCurrencyCredit;

    @NotNull
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;
}

