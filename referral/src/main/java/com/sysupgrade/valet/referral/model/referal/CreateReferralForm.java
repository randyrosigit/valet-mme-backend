package com.sysupgrade.valet.referral.model.referal;

import com.sysupgrade.valet.referral.orm.Referral;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateReferralForm {

    @NotNull
    private String referredUserId;

    @NotNull
    private String usedCode;

    private String error;

    private Referral referral;
}
