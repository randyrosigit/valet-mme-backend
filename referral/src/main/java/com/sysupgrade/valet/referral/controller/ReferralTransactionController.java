package com.sysupgrade.valet.referral.controller;

import com.sysupgrade.valet.referral.dao.ReferralTransactionsDao;
import com.sysupgrade.valet.referral.orm.ReferralTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api/referral/transaction")
public class ReferralTransactionController {

    private ReferralTransactionsDao dao;

    @GetMapping("/")
    public Page<ReferralTransaction> list(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @PutMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public ReferralTransaction save(@RequestBody @Valid ReferralTransaction referralTransaction) {
        referralTransaction.setStatus(ReferralTransaction.STATUS_ACTIVE);
        dao.save(referralTransaction);
        return referralTransaction;
    }

    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public ReferralTransaction update(@RequestBody @Valid ReferralTransaction referralTransaction) {
        dao.save(referralTransaction);
        return referralTransaction;
    }

    @PostMapping("/delete")
    @ResponseStatus(HttpStatus.CREATED)
    public ReferralTransaction delete(@RequestBody ReferralTransaction referralTransaction) {
        referralTransaction.setStatus(ReferralTransaction.STATUS_DELETE);
        dao.save(referralTransaction);
        return referralTransaction;
    }

    @GetMapping("/{id}")
    public Optional<ReferralTransaction> get(@PathVariable String id) {
        return dao.findById(id);
    }
}
