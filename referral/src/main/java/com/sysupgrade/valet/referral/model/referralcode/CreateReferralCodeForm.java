package com.sysupgrade.valet.referral.model.referralcode;

import com.sysupgrade.valet.referral.orm.ReferralCode;
import lombok.Data;

@Data
public class CreateReferralCodeForm {

    private String userId;

    private String status;

    private String code;

    private ReferralCode referralCode;

    private String error;
}
