package com.sysupgrade.valet.referral.dao;

import com.sysupgrade.valet.referral.orm.ReferralLevel;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface ReferralLevelDao extends PagingAndSortingRepository<ReferralLevel, Integer> {

    Optional<ReferralLevel> findByCurrencyIdAndLevel(String currencyId, Integer level);

    List<ReferralLevel> findByCurrencyId(String currencyId);

    List<ReferralLevel> findByLevel(int level);

}
