package com.sysupgrade.valet.referral.orm;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "referral_vc_schedule")
@Data
public class ReferralVcSchedule {

    @Id
    @Column(name = "user_id")
    private String userId;

    @Column(name = "last_schedule_at")
    private Date lastScheduleAt;

    @Column(name = "total_past_scheduled")
    private int totalPastScheduled;
}
