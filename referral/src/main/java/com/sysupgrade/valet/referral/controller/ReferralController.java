package com.sysupgrade.valet.referral.controller;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.referral.model.referal.CreateReferralForm;
import com.sysupgrade.valet.referral.model.referal.ListReferralForm;
import com.sysupgrade.valet.referral.model.referal.MyUplineForm;
import com.sysupgrade.valet.referral.orm.Referral;
import com.sysupgrade.valet.referral.service.ReferralService;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import com.sysupgrade.valet.user.model.user.PublicUser;
import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.service.UserService;
import com.sysupgrade.valet.wallet.service.WalletCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Optional;


@RestController
@RequestMapping("/api/referral")
public class ReferralController {

    @Autowired
    private ReferralService referralService;

    @Autowired
    private UserService userService;

    @Autowired
    private WalletCreditService walletCreditService;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @Autowired
    private SmartValidator validator;

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("join")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CreateReferralForm> create(@RequestBody CreateReferralForm form, BindingResult bindingResult) {

        form.setReferredUserId(authenticationFacade.getMyFace().getId());
        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            Referral referral = referralService.create(form);
            form.setReferral(referral);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("my-upline")
    public ResponseEntity<MyUplineForm> getMyUpline() {
        MyUplineForm form = new MyUplineForm();
        try {
            String userId = authenticationFacade.getMyFace().getId();
            Optional<Referral> referralOptional = referralService.getUpline(userId);
            if (referralOptional.isPresent()) {
                Optional<PublicUser> userOptional = userService.getByIdForPublic(referralOptional.get().getSourceUserId());
                userOptional.ifPresent(
                        (value) -> form.setUser(value)
                );
            }

            return ResponseEntity.ok(form);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
    }

    /**
     * Get List Downline
     *
     * @param form
     * @param bindingResult
     * @param pageable
     * @return
     */
    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("")
    public ResponseEntity<ListReferralForm> listMyReferral(@Valid ListReferralForm form,
                                                           BindingResult bindingResult,
                                                           Pageable pageable) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {

            form.setUserId(authenticationFacade.getMyFace().getId());

            Page<Referral> list = referralService.list(form.getUserId(), pageable);

            if (list.getTotalElements() > 0) {
                ArrayList<String> userIds = new ArrayList<>();
                list.getContent().stream().forEach(
                        (referral -> userIds.add(referral.getReferredUserId()))
                );
                Page<PublicUser> publicUsers = userService.findAllByUserIds(userIds);
                form.setPublicUsers(publicUsers);

            } else {
                form.setPublicUsers(null);
                form.setWalletCredits(null);
            }

        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }
}