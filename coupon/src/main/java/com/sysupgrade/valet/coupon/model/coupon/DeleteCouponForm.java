package com.sysupgrade.valet.coupon.model.coupon;

import com.sysupgrade.valet.coupon.orm.Coupon;
import lombok.Data;

@Data
public class DeleteCouponForm {


    private String id;
    private String error;

    private Coupon coupon;
}
