package com.sysupgrade.valet.coupon.model.usercoupon;

import lombok.Data;
import org.springframework.data.domain.Page;
import com.sysupgrade.valet.coupon.orm.UserCoupon;

@Data
public class ListUserCouponForm {


    private String error;

    private Page<UserCoupon> usercoupons;
}
