package com.sysupgrade.valet.coupon.orm;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "coupon")
@Data
public class Coupon {
    public static final int STATUS_DELETED = 0;
//
//    @Id
//    @GeneratedValue(generator = "uuid")
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
//    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "expired_date")
    private String expiredDate;

    @Column(name = "code")
    private String code;

    @Column(name = "amount")
    private BigDecimal amount;
//
//    @Column(name = "percentage")
//    private int percentage;

    @Column(name = "status")
    private int status;

    @Column(name = "deleted_status")
    private int deletedStatus;

    @Column(name = "user_id")
    private String userId;


    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;


}
