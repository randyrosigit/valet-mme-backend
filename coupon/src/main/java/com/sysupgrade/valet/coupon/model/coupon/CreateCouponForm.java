package com.sysupgrade.valet.coupon.model.coupon;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sysupgrade.valet.coupon.orm.Coupon;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;


@Data
public class CreateCouponForm {

    @NotNull
    private String name;

    private String expiredDate;

    @NotNull
    private String code;

    private BigDecimal amount;

    private int percentage;

    private int status;

    private String userId;


    private Coupon coupon;

    private String error;
}
