package com.sysupgrade.valet.coupon.model.usercoupon;

import com.sysupgrade.valet.coupon.orm.UserCoupon;
import lombok.Data;

@Data
public class DeleteUserCouponForm {


    private String id;
    private String error;

    private UserCoupon userCoupon;
}
