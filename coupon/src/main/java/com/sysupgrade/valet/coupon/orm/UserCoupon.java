package com.sysupgrade.valet.coupon.orm;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "coupon_users_coupons")
@Data
public class UserCoupon {
    public static final int STATUS_DELETED = 0;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "coupon_id")
    private String couponId;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "status")
    private int status;

    @Column(name = "deleted_status")
    private int deletedStatus;


    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;


}
