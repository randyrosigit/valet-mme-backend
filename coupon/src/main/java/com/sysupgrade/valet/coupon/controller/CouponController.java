package com.sysupgrade.valet.coupon.controller;

import com.sysupgrade.valet.coupon.model.coupon.CreateCouponForm;
import com.sysupgrade.valet.coupon.model.coupon.DeleteCouponForm;
import com.sysupgrade.valet.coupon.model.coupon.ListCouponForm;
import com.sysupgrade.valet.coupon.model.coupon.UpdateCouponForm;
import com.sysupgrade.valet.coupon.model.usercoupon.ListUserCouponForm;
import com.sysupgrade.valet.coupon.orm.Coupon;
import com.sysupgrade.valet.coupon.orm.UserCoupon;
import com.sysupgrade.valet.coupon.service.CouponService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/coupon/")
public class CouponController {


    @Autowired
    private CouponService couponService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ListCouponForm> listCoupons(ListCouponForm listCouponForm, Pageable pageable) {
        try {
            Page<Coupon> coupon = couponService.listCoupon(pageable);

            listCouponForm.setCoupons(coupon);
        } catch (Exception e) {
            listCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listCouponForm);
        }
        return ResponseEntity.ok(listCouponForm);
    }

    //    for edit and view
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<Coupon> getById(@PathVariable String id) {

        Coupon coupon = new Coupon();

        try {
            coupon = couponService.getById(id);
        } catch (Exception e) {
            return ResponseEntity.status(404).body(coupon);
        }

        return ResponseEntity.ok(coupon);

    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/get-all-coupon-by-userid/{userId}")
    public ResponseEntity<ListUserCouponForm> getAllCouponByUserId(@PathVariable String userId, ListUserCouponForm listUserCouponForm, Pageable pageable) {
        try {
            Page<UserCoupon> coupon = couponService.getCouponByUserId(pageable, userId);

            listUserCouponForm.setUsercoupons(coupon);
        } catch (Exception e) {
            listUserCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listUserCouponForm);
        }
        return ResponseEntity.ok(listUserCouponForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("create")
    public ResponseEntity<CreateCouponForm> createCoupon(@RequestBody @Valid CreateCouponForm createCouponForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createCouponForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createCouponForm);
        }

        try {
            createCouponForm.setUserId(iAuthenticationFacade.getMyFace().getId());
            Coupon coupon = couponService.create(createCouponForm);

            createCouponForm.setCoupon(coupon);
        } catch (Exception e) {
            createCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createCouponForm);
        }
        return ResponseEntity.ok(createCouponForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("update")
    public ResponseEntity<UpdateCouponForm> updateCoupon(@RequestBody @Valid UpdateCouponForm updateCouponForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            updateCouponForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(updateCouponForm);
        }

        try {
            Coupon coupon = couponService.updateCoupon(updateCouponForm);
            updateCouponForm.setCoupon(coupon);
        } catch (Exception e) {
            updateCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updateCouponForm);
        }
        return ResponseEntity.ok(updateCouponForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("delete")
    public ResponseEntity<DeleteCouponForm> deleteCoupon(@RequestBody @Valid DeleteCouponForm deleteCouponForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            deleteCouponForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(deleteCouponForm);
        }

        try {
            Coupon coupon = couponService.deleteCoupon(deleteCouponForm);
            deleteCouponForm.setCoupon(coupon);
        } catch (Exception e) {
            deleteCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(deleteCouponForm);
        }
        return ResponseEntity.ok(deleteCouponForm);
    }

}
