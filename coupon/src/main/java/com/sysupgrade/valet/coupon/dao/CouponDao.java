package com.sysupgrade.valet.coupon.dao;

import com.sysupgrade.valet.coupon.orm.Coupon;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;


public interface CouponDao extends PagingAndSortingRepository<Coupon, String> {
    // find by userId
    @Query(nativeQuery = true, value = "SELECT * FROM coupon where code=:code limit 1")
    Optional<Coupon> findByCouponCode(@Param("code") String code);

    Optional<Coupon> findByCode(String couponId);

}
