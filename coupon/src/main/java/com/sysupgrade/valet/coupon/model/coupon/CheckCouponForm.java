package com.sysupgrade.valet.coupon.model.coupon;

import com.sysupgrade.valet.coupon.orm.Coupon;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CheckCouponForm {

    @NotNull
    private String code;

    private String error;

    private Coupon coupon;
}
