package com.sysupgrade.valet.coupon.controller;

import com.sysupgrade.valet.coupon.model.coupon.CheckCouponForm;
import com.sysupgrade.valet.coupon.model.usercoupon.*;
import com.sysupgrade.valet.coupon.orm.Coupon;
import com.sysupgrade.valet.coupon.orm.UserCoupon;
import com.sysupgrade.valet.coupon.service.UserCouponService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api/user-coupon/")
public class UserCouponController {

    public int check;

    @Autowired
    private UserCouponService userCouponService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ListUserCouponForm> listUserCoupons(ListUserCouponForm listUserCouponForm, Pageable pageable) {
        try {
            Page<UserCoupon> userCoupon = userCouponService.listUserCoupon(pageable);

            listUserCouponForm.setUsercoupons(userCoupon);
        } catch (Exception e) {
            listUserCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listUserCouponForm);
        }
        return ResponseEntity.ok(listUserCouponForm);
    }



    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("create")
    public ResponseEntity<CreateUserCouponForm> createUserCoupon(@RequestBody @Valid CreateUserCouponForm createUserCouponForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createUserCouponForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createUserCouponForm);
        }

        try {
//            createUserCouponForm.setUserId(iAuthenticationFacade.getMyFace().getId());
            UserCoupon userCoupon = userCouponService.create(createUserCouponForm);

            createUserCouponForm.setUserCoupon(userCoupon);
        } catch (Exception e) {
            createUserCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createUserCouponForm);
        }
        return ResponseEntity.ok(createUserCouponForm);
    }


    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("update")
    public ResponseEntity<UpdateUserCouponForm> updateUserCoupon(@RequestBody @Valid UpdateUserCouponForm updateUserCouponForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            updateUserCouponForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(updateUserCouponForm);
        }

        try {
            UserCoupon userCoupon = userCouponService.updateUserCoupon(updateUserCouponForm);
            updateUserCouponForm.setUserCoupon(userCoupon);
        } catch (Exception e) {
            updateUserCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updateUserCouponForm);
        }
        return ResponseEntity.ok(updateUserCouponForm);
    }



    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("delete")
    public ResponseEntity<DeleteUserCouponForm> deleteUserCoupon(@RequestBody @Valid DeleteUserCouponForm deleteUserCouponForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            deleteUserCouponForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(deleteUserCouponForm);
        }

        try {
            UserCoupon userCoupon = userCouponService.deleteUserCoupon(deleteUserCouponForm);
            deleteUserCouponForm.setUserCoupon(userCoupon);
        } catch (Exception e) {
            deleteUserCouponForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(deleteUserCouponForm);
        }
        return ResponseEntity.ok(deleteUserCouponForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<UserCoupon> getById(@PathVariable String id) {

        UserCoupon userCoupon = new UserCoupon();

        try {
            userCoupon = userCouponService.getById(id);
        } catch (Exception e){
            return ResponseEntity.status(404).body(userCoupon);
        }

        return ResponseEntity.ok(userCoupon);

    }


    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/check-if-user-get-coupon/{couponId}/{userId}")
    public boolean checkIfUserGetCoupon(@PathVariable String couponId, @PathVariable String userId){
        check = userCouponService.checkIfUserGetCouponService(couponId, userId);
        if (check == 1){
            return true;
        }else{
            return false;
        }
    }

}
