package com.sysupgrade.valet.coupon.dao;

import com.sysupgrade.valet.coupon.orm.UserCoupon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


public interface UserCouponDao extends PagingAndSortingRepository<UserCoupon, String> {


    @Query(nativeQuery = true, value = "SELECT * FROM coupon_users_coupons WHERE user_id=:user_id AND status=0 AND deleted_status=0")
    Page<UserCoupon> getCouponByUserId(@Param("user_id") String userId, Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM coupon_users_coupons WHERE coupon_id=:coupon_id AND user_id=:user_id AND status=0 AND deleted_status=0")
    Integer checkIfUserGetCouponDao(@Param("coupon_id") String couponId, @Param("user_id") String userId);

}
