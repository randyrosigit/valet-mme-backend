package com.sysupgrade.valet.coupon.service;

import com.sysupgrade.valet.coupon.dao.CouponDao;
import com.sysupgrade.valet.coupon.dao.UserCouponDao;
import com.sysupgrade.valet.coupon.model.coupon.CreateCouponForm;
import com.sysupgrade.valet.coupon.model.coupon.DeleteCouponForm;
import com.sysupgrade.valet.coupon.model.coupon.UpdateCouponForm;
import com.sysupgrade.valet.coupon.orm.Coupon;
import com.sysupgrade.valet.coupon.orm.UserCoupon;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CouponService {

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;


    @Autowired
    private CouponDao couponDao;

    @Autowired
    private UserCouponDao userCouponDao;

    public Page<Coupon> listCoupon(Pageable pageable) {
        return couponDao.findAll(pageable);
    }


    @Transactional
    public Coupon create(CreateCouponForm createCouponForm) {
        Coupon coupon = new Coupon();

        coupon.setName(createCouponForm.getName());
        coupon.setExpiredDate(createCouponForm.getExpiredDate());
        coupon.setCode(createCouponForm.getCode());
        coupon.setAmount(createCouponForm.getAmount());
//        coupon.setPercentage(createCouponForm.getPercentage());
        coupon.setStatus(createCouponForm.getStatus());
        coupon.setUserId(createCouponForm.getUserId());
        this.couponDao.save(coupon);
        return coupon;
    }

    @Transactional
    public Coupon updateCoupon(UpdateCouponForm updateCouponForm) throws Exception {
        Optional<Coupon> couponOptional = couponDao.findById(updateCouponForm.getId());

        if (!couponOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Coupon coupon = couponOptional.get();

        coupon.setName(updateCouponForm.getName());
        coupon.setExpiredDate(updateCouponForm.getExpiredDate());
        coupon.setCode(updateCouponForm.getCode());
        coupon.setAmount(updateCouponForm.getAmount());
//        coupon.setPercentage(updateCouponForm.getPercentage());
        coupon.setStatus(updateCouponForm.getStatus());

        this.couponDao.save(coupon);

        return coupon;
    }

    @Transactional
    public Coupon deleteCoupon(DeleteCouponForm deleteCouponForm) throws Exception {
        Optional<Coupon> couponOptional = couponDao.findById(deleteCouponForm.getId());
        if (!couponOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Coupon coupon = couponOptional.get();
        coupon.setDeletedStatus(Coupon.STATUS_DELETED);
        this.couponDao.save(coupon);

        return coupon;
    }


    public Coupon getById(String Id) throws Exception {
        Optional<Coupon> optionalCoupon = couponDao.findById(Id);
        if (!optionalCoupon.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Coupon coupon = new Coupon();
        coupon.setName(optionalCoupon.get().getName());
        coupon.setCode(optionalCoupon.get().getCode());
        coupon.setAmount(optionalCoupon.get().getAmount());
        coupon.setStatus(optionalCoupon.get().getStatus());
        coupon.setExpiredDate(optionalCoupon.get().getExpiredDate());


        return optionalCoupon.get();
    }


    public Page<UserCoupon> getCouponByUserId(Pageable pageable, String userId) {
        return userCouponDao.getCouponByUserId(userId, pageable);
    }

    public Optional<Coupon> findById(String couponid) {
        return couponDao.findByCode(couponid);
    }
}
