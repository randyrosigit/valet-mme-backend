package com.sysupgrade.valet.coupon.model.stampcard;

import com.sysupgrade.valet.coupon.orm.StampCard;
import org.springframework.data.domain.Page;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class ListStampCardForm {


    private int stampCollected;

    private int canUse;



    private Page<StampCard> stampcards;

    private String error;
}
