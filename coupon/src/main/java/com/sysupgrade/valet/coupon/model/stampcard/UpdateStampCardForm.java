package com.sysupgrade.valet.coupon.model.stampcard;

import com.sysupgrade.valet.coupon.orm.StampCard;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class UpdateStampCardForm {

    @NotNull
    private String userId;

    @NotNull
    private int stampCollected;

    @NotNull
    private int canUse;



    private StampCard stampCard;

    private String error;
}
