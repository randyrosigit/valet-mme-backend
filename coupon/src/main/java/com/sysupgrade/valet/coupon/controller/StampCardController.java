package com.sysupgrade.valet.coupon.controller;

import com.sysupgrade.valet.coupon.model.coupon.ListCouponForm;
import com.sysupgrade.valet.coupon.model.stampcard.*;
import com.sysupgrade.valet.coupon.orm.Coupon;
import com.sysupgrade.valet.coupon.orm.StampCard;
import com.sysupgrade.valet.coupon.service.StampCardService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/stamp-card/")
public class StampCardController {


    @Autowired
    private StampCardService stampCardService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ListStampCardForm> listStampCards(ListStampCardForm listStampCardForm, Pageable pageable) {
        try {
            Page<StampCard> stampCard = stampCardService.listStampCard(pageable);

            listStampCardForm.setStampcards(stampCard);
        } catch (Exception e) {
            listStampCardForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listStampCardForm);
        }
        return ResponseEntity.ok(listStampCardForm);
    }


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<StampCard> getById(@PathVariable String id) {

        StampCard stampCard = new StampCard();

        try {
            stampCard = stampCardService.getById(id);
        } catch (Exception e){
            return ResponseEntity.status(404).body(stampCard);
        }

        return ResponseEntity.ok(stampCard);

    }


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("create")
    public ResponseEntity<CreateStampCardForm> createStampCard(@RequestBody @Valid CreateStampCardForm createStampCardForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createStampCardForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createStampCardForm);
        }

        try {
//            createStampCardForm.setUserId(iAuthenticationFacade.getMyFace().getId());
            StampCard stampCard = stampCardService.create(createStampCardForm);

            createStampCardForm.setStampCard(stampCard);
        } catch (Exception e) {
            createStampCardForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createStampCardForm);
        }
        return ResponseEntity.ok(createStampCardForm);
    }


    // give free stamp card with rides

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("give-free-stamp-card")
    public ResponseEntity<CreateStampCardForm> giveFreeStampCard(@RequestBody @Valid CreateStampCardForm createStampCardForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createStampCardForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createStampCardForm);
        }

        try {
            StampCard stampCard = stampCardService.giveFreeStampCard(createStampCardForm);
            createStampCardForm.setStampCard(stampCard);
        } catch (Exception e) {
            createStampCardForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createStampCardForm);
        }
        return ResponseEntity.ok(createStampCardForm);
    }




    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("update-free-stamp-card")
    public ResponseEntity<UpdateStampCardForm> updateFreeStampCard(@RequestBody @Valid UpdateStampCardForm updateStampCardForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            updateStampCardForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(updateStampCardForm);
        }

        try {
            StampCard stampCard = stampCardService.updateFreeStampCard(updateStampCardForm);
            updateStampCardForm.setStampCard(stampCard);
        } catch (Exception e) {
            updateStampCardForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updateStampCardForm);
        }
        return ResponseEntity.ok(updateStampCardForm);
    }



    // ech user ride stampcard coollection + 1
    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("update")
    public ResponseEntity<UpdateStampCardForm> updateStampCard(@RequestBody @Valid UpdateStampCardForm updateStampCardForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            updateStampCardForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(updateStampCardForm);
        }

        try {
            StampCard stampCard = stampCardService.updateStampCard(updateStampCardForm);
            updateStampCardForm.setStampCard(stampCard);
        } catch (Exception e) {
            updateStampCardForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updateStampCardForm);
        }
        return ResponseEntity.ok(updateStampCardForm);
    }



}
