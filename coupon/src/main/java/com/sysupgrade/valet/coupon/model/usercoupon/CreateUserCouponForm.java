package com.sysupgrade.valet.coupon.model.usercoupon;

import com.sysupgrade.valet.coupon.orm.UserCoupon;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class CreateUserCouponForm {

    @NotNull
    private String couponId;

    @NotNull
    private String userId;

    @NotNull
    private int status;



    private UserCoupon userCoupon;

    private String error;
}
