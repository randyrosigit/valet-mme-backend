package com.sysupgrade.valet.coupon.dao;

import com.sysupgrade.valet.coupon.orm.StampCard;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface StampCardDao extends PagingAndSortingRepository<StampCard, String> {

    // find by userId
    @Query(nativeQuery = true, value = "SELECT * FROM coupon_stamp_card where user_id=:user_id limit 1")
    Optional<StampCard> findByUserId(@Param("user_id") String userId);


}
