package com.sysupgrade.valet.coupon.service;

import com.sysupgrade.valet.coupon.dao.StampCardDao;
import com.sysupgrade.valet.coupon.model.stampcard.*;
import com.sysupgrade.valet.coupon.orm.StampCard;
import com.sysupgrade.valet.coupon.orm.StampCard;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class StampCardService {

    @Autowired
    private StampCardDao stampCardDao;

    public Page<StampCard> listStampCard(Pageable pageable) {
        return stampCardDao.findAll(pageable);
    }

    @Transactional
    public StampCard create(CreateStampCardForm createStampCardForm){
        // cek dl userId ybs sdh ada belum jika belum bisa insert baru

        Optional<StampCard> stampCardOptional = stampCardDao.findByUserId(createStampCardForm.getUserId());
        StampCard stampCard = new StampCard();

        if (!stampCardOptional.isPresent()) {
            //throw new Exception(ErrorHelper.DATA_NOT_FOUND);


            stampCard.setUserId(createStampCardForm.getUserId());
            stampCard.setStampCollected(1);
            this.stampCardDao.save(stampCard);

        }
        return stampCard;
    }


    public StampCard getById(String Id) throws Exception{
        Optional<StampCard> optionalStampCard = stampCardDao.findById(Id);
        if (!optionalStampCard.isPresent()){
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        StampCard stampCard = new StampCard();
        stampCard.setUserId(optionalStampCard.get().getUserId());
        stampCard.setCanUse(optionalStampCard.get().getCanUse());


        return optionalStampCard.get();
    }




    // give free stamp card with rides
    @Transactional
    public StampCard giveFreeStampCard(CreateStampCardForm createStampCardForm){

        StampCard stampCard = new StampCard();

        stampCard.setUserId(createStampCardForm.getUserId());
        stampCard.setCanUse(createStampCardForm.getCanUse());

        this.stampCardDao.save(stampCard);

        return stampCard;
    }

    // UPDATE FREE STAMP CARD
    @Transactional
    public StampCard updateFreeStampCard(UpdateStampCardForm updateStampCardForm) throws Exception {

        Optional<StampCard> stampCardOptional = stampCardDao.findByUserId(updateStampCardForm.getUserId());

        if (!stampCardOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        StampCard stampCard = stampCardOptional.get();

        stampCard.setId(stampCard.getId());
        stampCard.setUserId(updateStampCardForm.getUserId());
        stampCard.setCanUse(updateStampCardForm.getCanUse());


        this.stampCardDao.save(stampCard);

        return stampCard;
    }


    @Transactional
    public StampCard updateStampCard(UpdateStampCardForm updateStampCardForm) throws Exception {

        Optional<StampCard> stampCardOptional = stampCardDao.findByUserId(updateStampCardForm.getUserId());

        if (!stampCardOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        StampCard stampCard = stampCardOptional.get();

        stampCard.setStampCollected(stampCard.getStampCollected() + 1);

        // if stampCard.getStampCollected == 10 update can use jadi 1


        this.stampCardDao.save(stampCard);

        return stampCard;
    }



}
