package com.sysupgrade.valet.coupon.service;

import com.sysupgrade.valet.coupon.dao.UserCouponDao;
import com.sysupgrade.valet.coupon.model.usercoupon.*;
import com.sysupgrade.valet.coupon.orm.UserCoupon;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserCouponService {

    @Autowired
    private UserCouponDao userCouponDao;


    public Page<UserCoupon> listUserCoupon(Pageable pageable) {
        return userCouponDao.findAll(pageable);
    }


    @Transactional
    public UserCoupon create(CreateUserCouponForm createUserCouponForm){
        UserCoupon userCoupon = new UserCoupon();

        userCoupon.setUserId(createUserCouponForm.getUserId());
        userCoupon.setCouponId(createUserCouponForm.getCouponId());
        userCoupon.setStatus(createUserCouponForm.getStatus());

        this.userCouponDao.save(userCoupon);
        return userCoupon;
    }

    @Transactional
    public UserCoupon updateUserCoupon(UpdateUserCouponForm updateUserCouponForm) throws Exception {
        Optional<UserCoupon> userCouponOptional = userCouponDao.findById(updateUserCouponForm.getId());

        if (!userCouponOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        UserCoupon userCoupon = userCouponOptional.get();

        userCoupon.setUserId(updateUserCouponForm.getUserId());
        userCoupon.setCouponId(updateUserCouponForm.getCouponId());
        userCoupon.setStatus(updateUserCouponForm.getStatus());

        this.userCouponDao.save(userCoupon);

        return userCoupon;
    }

    @Transactional
    public UserCoupon deleteUserCoupon(DeleteUserCouponForm deleteUserCouponForm) throws Exception {
        Optional<UserCoupon> userCouponOptional = userCouponDao.findById(deleteUserCouponForm.getId());
        if (!userCouponOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        UserCoupon userCoupon = userCouponOptional.get();
        userCoupon.setDeletedStatus(UserCoupon.STATUS_DELETED);
        this.userCouponDao.save(userCoupon);

        return userCoupon;
    }


    public UserCoupon getById(String Id) throws Exception{
        Optional<UserCoupon> optionalUserCoupon = userCouponDao.findById(Id);
        if (!optionalUserCoupon.isPresent()){
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setId(optionalUserCoupon.get().getId());
        userCoupon.setCouponId(optionalUserCoupon.get().getCouponId());
        userCoupon.setUserId(optionalUserCoupon.get().getUserId());
        userCoupon.setStatus(optionalUserCoupon.get().getStatus());

        return optionalUserCoupon.get();
    }

    public int checkIfUserGetCouponService(String couponId, String userId) {
        return userCouponDao.checkIfUserGetCouponDao(couponId, userId);
    }
}
