package com.sysupgrade.valet.coupon.model.coupon;

import lombok.Data;
import org.springframework.data.domain.Page;
import com.sysupgrade.valet.coupon.orm.Coupon;

@Data
public class ListCouponForm {


    private String error;

    private Page<Coupon> coupons;
}
