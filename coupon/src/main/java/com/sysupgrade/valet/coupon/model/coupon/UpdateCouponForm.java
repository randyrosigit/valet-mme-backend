package com.sysupgrade.valet.coupon.model.coupon;

import com.sysupgrade.valet.coupon.orm.Coupon;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;


@Data
public class UpdateCouponForm {

    @NotNull
    private String name;

    private String expiredDate;

    @NotNull
    private String code;

    private BigDecimal amount;

    private int percentage;

    private int status;
    private String id;


    private Coupon coupon;

    private String error;
}
