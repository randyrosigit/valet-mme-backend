package com.sysupgrade.valet.library.config.config;

import java.math.BigDecimal;

public class CurrencyConfig {

    public static final String ETH_ID = "ETH-1534218246";

    public static final String HC_ID = "HC-1zxz";

    public static final String RMB_CURRENCY_ID = "RMBsadjlada";

    public static final String SGD_CURRENCY_ID = "SGD-221133";

    public static final String RMB_CURRENCY_CODE = "RMB";

    public static final String USD_CURRENCY_CODE = "USD";

    public static final String IDR_CURRENCY_CODE = "IDR";

    public static final String SGD_CURRENCY_CODE = "SGD";

    public static final String HC_CURRENCY_CODE = "HC";

    public static final String ETH_CURRENCY_CODE = "ETH";

    public static final String RMB_GROUP_ID = "RMBidlkfdjfs";

    public static final String HC_GROUP_ID = "HC-1234";

    public static final String ETH_GROUP_ID = "ETH-1234";

    public static final String SGD_GROUP_ID = "SGD-1234";

    public static final String KINGPAY_PRODUCTION_NAME = "KINGPAY-order";

    public static final String HC_RMB_RATE = "7.2";

    public static final String HC_USD_RATE = "1";

    public static final String HC_IDR_RATE = "14000";

}
