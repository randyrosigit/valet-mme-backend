package com.sysupgrade.valet.library.config.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;

@Component
public class CryptoHelper {

    private byte[] key;

    private static final String ALGORITHM = "AES";

    private static final String SPECIFIC_ALGORITHM = "AES/ECB/PKCS5Padding";

    private Logger logger = LoggerFactory.getLogger(CryptoHelper.class);

    public CryptoHelper() {

    }

    public void setKey(String key) {
        this.key = key.getBytes();
    }


    /**
     * Encryp   ts the given plain text
     *
     * @param plainText The plain text to encrypt
     */
    public String encrypt(String plainText) throws Exception
    {
        SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
        Cipher cipher = Cipher.getInstance(SPECIFIC_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        logger.info(new String(encryptedBytes));
        return new BASE64Encoder().encode(encryptedBytes);
    }

    /**
     * Decrypts the given byte array
     *
     * @param cipherText The data to decrypt
     */
    public String decrypt(String cipherText) throws Exception
    {
        SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
        Cipher cipher = Cipher.getInstance(SPECIFIC_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        byte[] decodedValue = new BASE64Decoder().decodeBuffer(cipherText);
        byte[] decryptedBytes = cipher.doFinal(decodedValue);
        return new String(decryptedBytes);
    }

    public static String hmacSHA1Digest(String msg, String keyString, String algorithm) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algorithm);
            Mac mac = Mac.getInstance(algorithm);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (byte eachByte : bytes) {
                String hex = Integer.toHexString(0xFF & eachByte);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return digest;
    }


    public static String combinePlainText(TreeMap<String, String> treeMap) {
        String plainText = "";

        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                plainText += key + "=" + value + "&";
            }
        }


        plainText = plainText.substring(0, plainText.length() - 1);
        return plainText;

    }
}

