package com.sysupgrade.valet.library.config.helper;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.naming.Binding;

public class ErrorHelper {

    public final static String UNAUTHORIZED = "#1000403";

    public final static String DATA_NOT_FOUND = "#1000404";

    public final static String NOT_ENOUGH_ETEH = "#1000303";

    public final static String NOT_ENOUGH_ATA = "#1000302";

    public final static String ERROR = "#0000001";

    public final static String NOT_AN_ADMIN = "#0000002";

    public final static String NOT_PURCHASABLE_CURRENCY = "#2000401";

    public final static String CONVERT_RATE_DOES_NOT_EXIST_CURRENCY = "#2000402";

    public final static String NOT_ENOUGH_AMOUNT_CURRENCY = "#2000403";

    public final static String THIS_CURRENCY_IS_NOT_PURCHASABLE_CURRENCY = "#2000404";

    public final static String NOT_ENOUGH_CREDIT_CURRENCY = "#2000405";

    public final static String NOT_ENOUGH_CREDIT_IN_SYSTEM_CURRENCY = "#2000406";

    public final static String NOT_A_COIN_OWNER_CURRENCY = "#2000407";

    public final static String DATA_HAS_CHANGED_CURRENCY = "#2000408";

    public final static String NOT_FOUND_CURRENCY_CURRENCY = "#2000409";

    public final static String WALLETADDRESS_COINCODE_NOT_FOUND = "#2000410" ;

    public final static String RESET_PASSWORD_TOKEN_IS_NOT_CORRECT_USER = "#3000401";

    public final static String VALIDATION_TOKEN_IS_NOT_CORRECT_USER = "#3000402";

    public final static String PHONE_NUMBER_HAS_BEEN_REGISTERED_USER = "#3000403";

    public final static String PHONE_NUMBER_NOT_REGISTERED_USER = "#3000404";

    public final static String OLD_PASSWORD_WRONG_USER = "#3000405";

    public final static String NOT_ENOUGH_CREDIT_WALLET = "#4000402";

    public final static String WALLET_HAS_EXISTED_WALLET = "#4000403";

    public final static String GAME_IS_LIVE_OR_FINISH = "#5000001";

    public final static String ALREADY_HAVE_STORE = "#6000001";

    public final static String NOT_ENOUGH_STOCK = "#6000002";

    public final static String NOT_A_MERCHANT = "#6000003";

    public final static String NOT_A_BUYER = "#6000004";

    public final static String EMAIL_HAS_BEEN_REGISTERED_USER = "#7000001";

    public static String getErrorMessage(BindingResult bindingResult) {
        ObjectError objectError = bindingResult.getAllErrors().get(0);
        if(objectError instanceof FieldError) {
            return  ((FieldError) objectError).getField() + " " + objectError.getDefaultMessage();
        }
        return  objectError.getDefaultMessage();
    }
}






