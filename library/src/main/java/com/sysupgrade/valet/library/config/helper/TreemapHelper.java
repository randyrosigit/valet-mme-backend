package com.sysupgrade.valet.library.config.helper;

import java.util.Map;
import java.util.TreeMap;

public class TreemapHelper {

    public static String encodeUrl(TreeMap<String, String> map) {
        String plainText = "";

        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                plainText += key + "=" + value + "&";
            }
        }


        plainText = plainText.substring(0, plainText.length() - 1);
        return plainText;
    }
}
