package com.sysupgrade.valet.library.config.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsAdminValidator implements ConstraintValidator<IsAdminConstraint, String> {

    @Override
    public boolean isValid(String token,
                           ConstraintValidatorContext cxt) {
        return true;
    }
}
