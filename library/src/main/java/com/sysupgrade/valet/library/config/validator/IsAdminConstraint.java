package com.sysupgrade.valet.library.config.validator;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IsAdminValidator.class)
public @interface IsAdminConstraint {
    String message() default ErrorHelper.NOT_AN_ADMIN;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}