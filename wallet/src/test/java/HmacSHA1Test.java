import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;


public class HmacSHA1Test {

    @org.junit.Test
    public void main() {
        String secret_key = "sk_a11k0d47t4sjg22vfa5s19fhs6";

        String orderId = "5348335646426";
        String appId = "8610000074667";
        String currency = "CNY";
        Integer price = 8000;
        String productionName = "Test Order";
        String returnUrl = "http://www.example.com/return"; // get
        String notifyUrl = "http://www.example.com/notify";  // post
        String cancelUrl = "http://www.example.com/cancel";  // get


        //should a-z
        String plainText = "appId=8610000074667&cancelUrl=http://www.example.com/cancel&currency=CNY&notifyUrl=http://www.example.com/notify&orderId=5348335646426&price=1000&productionName=Test Order&returnUrl=http://www.example.com/return";


        System.out.println(hmacDigest1(plainText, secret_key, "HmacSHA1"));
        hmacDigest2(plainText, secret_key);

    }


    public static String hmacDigest1(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }


    public static void hmacDigest2(String message, String keyString) {

        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secret = new SecretKeySpec(keyString.getBytes(), "HmacSHA1");
            mac.init(secret);
            byte[] digest = mac.doFinal(message.getBytes());
            //String enc = new String(digest);
            //System.out.println(enc);

            for (byte b : digest) {
                System.out.format("%02x", b);
            }

            System.out.println();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}