import org.junit.Test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.TreeMap;

public class KingPayResultTest {


    @Test
    public void test() {
        String transactionId = "30";
        String status = "PAID";
        String appId = "8610000074667";
        String orderId = "5348335646426";
        String productionName = "Test Order";
        Integer price = 8000;
        String currency = "CNY";
        String transactionToken = "084DE204-BB42-DE8B-FEB9-E7B75F1B4D2E";

        String signature = "33ddabd7ceb1531557c9f0323d08b9e2f816965b";


        String secret_key = "sk_a11k0d47t4sjg22vfa5s19fhs6";
        TreeMap<String, String> treeMap = new TreeMap();
        treeMap.put("transactionId", transactionId);
        treeMap.put("status", status);
        treeMap.put("appId", appId);
        treeMap.put("orderId", orderId);
        treeMap.put("productionName", productionName);
        treeMap.put("price", price.toString());
        treeMap.put("currency", currency);
        treeMap.put("transactionToken", transactionToken);
        System.out.println("treeMap.keySet() => " + treeMap.keySet());


        String plainText = combinePlainText(treeMap);
        System.out.println("plainText => "+plainText);
        String cipherText = hmacSHA1Digest(plainText, secret_key, "HmacSHA1");
        System.out.println("cipherText => "+cipherText);

    }

    public String combinePlainText(TreeMap<String, String> treeMap) {
        String plainText = "";

        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                plainText += key + "=" + value + "&";
            }
        }


        plainText = plainText.substring(0, plainText.length() - 1);
        return plainText;

    }

    public String hmacSHA1Digest(String msg, String keyString, String algorithm) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algorithm);
            Mac mac = Mac.getInstance(algorithm);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (byte eachByte : bytes) {
                String hex = Integer.toHexString(0xFF & eachByte);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return digest;
    }
}
