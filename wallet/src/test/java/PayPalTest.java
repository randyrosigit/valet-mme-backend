import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PayPalTest {

    @Test
    public void test_1(){

        String clientId = "AbtPCCCU8qf08UBwsfS5wxwLT54b001bGaUZkkbUFLhIXyNeVj4_-T2pOHDCzqZiysJSCxauBcZNzmGV";
        String clientSecret = "EKusbGKIXG-qZ2UN1ktvtlxXUPFkzRUxrUVsVQs929PwnlbdCD8yKuj0vvU2E8asfD-cO8QeZudMovyP";

        Item item = new Item();
        item.setName("HC topup").setQuantity("1").setCurrency("USD").setPrice("10000");


        Amount amount = new Amount();
        amount.setCurrency("USD");
        //amount.setCurrency("EUR");

        amount.setTotal("1000.00");

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);


        ItemList itemList = new ItemList();
        List<Item> items = new ArrayList<Item>();
        items.add(item);
        itemList.setItems(items);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("https://example.com/cancel");
        redirectUrls.setReturnUrl("https://example.com/return");
        payment.setRedirectUrls(redirectUrls);


        try {
            APIContext apiContext = new APIContext(clientId, clientSecret, "sandbox");
            Payment createdPayment = payment.create(apiContext);


            System.out.println("*******************************************");
            //System.out.println(createdPayment.getLinks().get(0).toString());
            System.out.println(createdPayment.toString());

        } catch (PayPalRESTException e) {
            // Handle errors
        } catch (Exception ex) {
            // Handle errors
        }
    }



    @Test
    public void test_2() throws Exception {
        String clientId = "AbtPCCCU8qf08UBwsfS5wxwLT54b001bGaUZkkbUFLhIXyNeVj4_-T2pOHDCzqZiysJSCxauBcZNzmGV";
        String clientSecret = "EKusbGKIXG-qZ2UN1ktvtlxXUPFkzRUxrUVsVQs929PwnlbdCD8yKuj0vvU2E8asfD-cO8QeZudMovyP";

        Details details = new Details();

        details.setShipping("1");

        details.setSubtotal("5");

        details.setTax("1");

        Amount amount = new Amount();

        amount.setCurrency("USD");

        amount.setTotal("7");

        amount.setDetails(details);


//        ShippingAddress address = new ShippingAddress();
//        address.setCity("Singapore");
//        address.setCountryCode("SG");
//        address.setPostalCode("01119");
//        address.setLine1("8 burn road");


        Transaction transaction = new Transaction();

        transaction.setAmount(amount);

        transaction.setDescription("This is the payment transaction description");

        Item item = new Item();

        item.setName("Ground Coffee 40oz").setQuantity("1").setCurrency("USD").setPrice("5");

        ItemList itemList = new ItemList();

        List<Item> items = new ArrayList<Item>();

        items.add(item);

        itemList.setItems(items);


        //itemList.setShippingAddress(address);

        transaction.setItemList(itemList);


        List<Transaction> transactions = new ArrayList<Transaction>();

        transactions.add(transaction);


        Payer payer = new Payer();

        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();

        payment.setIntent("sale");

        payment.setPayer(payer);

        payment.setTransactions(transactions);


        RedirectUrls redirectUrls = new RedirectUrls();

        redirectUrls.setCancelUrl("https://example.com/cancel");

        redirectUrls.setReturnUrl("https://example.com/return");

        payment.setRedirectUrls(redirectUrls);


        APIContext apiContext = new APIContext(clientId, clientSecret, "sandbox");

        Payment createdPayment = payment.create(apiContext);

        System.out.println("***************************************************************************");
        System.out.println("createdPayment => " + createdPayment.toString());
        System.out.println("***************************************************************************");


        Iterator<Links> links = createdPayment.getLinks().iterator();


        while (links.hasNext()) {

            Links link = links.next();

            if (link.getRel().equalsIgnoreCase("approval_url")) {

                String paypalRedirectLink = link.getHref();
                //System.out.println("paypalRedirectLink => " + paypalRedirectLink);

            }

        }
    }
}
