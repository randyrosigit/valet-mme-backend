package com.sysupgrade.valet.wallet.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(name = "wallet_credit_system")
@Data
public class WalletCreditSystem {

    public final static int STATUS_DELETE = 0;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_PENDING = 11;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "total_credit")
    private BigDecimal totalCredit;

    @NotNull
    @Column(name = "currency_id")
    private String currencyId;

    @NotNull
    @Column(name = "activity_type")
    private String activityType;

    @NotNull
    @Column(name = "adjustment_value")
    private BigDecimal adjustmentValue;

    @NotNull
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;

}
