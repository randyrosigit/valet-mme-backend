package com.sysupgrade.valet.wallet.dao;

import com.sysupgrade.valet.wallet.orm.WalletPaypal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface WalletPaypalDao extends PagingAndSortingRepository<WalletPaypal, String> {

    @Query(nativeQuery = true, value = " " +
            " SELECT * " +
            " from wallet_paypal where user_id = :user_id and status =:status" +
            " ORDER BY id desc LIMIT 1")
    Optional<WalletPaypal> getLastPaypal(@Param("user_id") String userId, @Param("status") int status);


    Optional<WalletPaypal> findByUniqueCode(String uniqueCode);

}
