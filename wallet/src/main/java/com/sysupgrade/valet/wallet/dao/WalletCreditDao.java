package com.sysupgrade.valet.wallet.dao;

import com.sysupgrade.valet.wallet.orm.WalletCredit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Collection;
import java.util.Optional;

public interface WalletCreditDao extends PagingAndSortingRepository<WalletCredit, String> {

    @Modifying
    @Query(nativeQuery = true,
            value = "INSERT INTO wallet_credit(user_id, total_credit, adjustment_value, currency_id ,status,\n" +
                    "                    created_at,updated_at,activity_type ,note)\n" +
                    "        SELECT user_id, total_credit + :adjustment_value, :adjustment_value, :currency_id, :status, \n" +
                    "                :created_at, :updated_at, :activity_type, :note\n" +
                    "        from wallet_credit\n" +
                    "        where user_id = :user_id and currency_id = :currency_id\n" +
                    "        ORDER BY id desc\n" +
                    "        LIMIT 1 ")
    int updateCredit(
            @Param("user_id") String userId,
            @Param("currency_id") String currencyId,
            @Param("adjustment_value") BigDecimal adjustmentValue,
            @Param("note") String note,
            @Param("created_at") Date createdAt,
            @Param("updated_at") Date updatedAt,
            @Param("activity_type") String activityType,
            @Param("status") int status);


    @Query(nativeQuery = true, value = " " +
            " SELECT * " +
            " from wallet_credit where user_id = :user_id and currency_id = :currency_id " +
            " ORDER BY id desc LIMIT 1")
    Optional<WalletCredit> getUserCreditByCurrencyId(@Param("user_id") String userId, @Param("currency_id") String currencyId);


//    @Query(nativeQuery = true, value = "SELECT * " +
//            "from wallet_credit " +
//            "where wallet_credit.user_id = :user_id and currency_id IN :currency_ids " +
//            "order by id desc")
//    Page<WalletCredit> findAll(@Param("user_id") String userId,
//                                @Param("currency_ids") Collection<String> currencyIds,
//                                Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT * " +
                    " FROM wallet_credit wc " +
                    " WHERE user_id=:user_id " +
                    " AND wc.currency_id IN :currency_ids " +
                    " ORDER  BY wc.id DESC  ")
    Page<WalletCredit> findAll(@Param("user_id") String userId,
                               @Param("currency_ids") Collection<String> currencyIds,
                               Pageable pageable);

    /**
     * @param currencyId
     * @param pageable
     * @return
     */
    Page<WalletCredit> findAllByCurrencyId(String currencyId, Pageable pageable);

    @Query(nativeQuery = true, value = "" +
            "SELECT * " +
            "from wallet_credit " +
            "where id IN ( " +
            "    SELECT max(id) " +
            "    FROM wallet_credit" +
            "    where user_id IN (:user_ids) and currency_id = :currency_id " +
            "    group by user_id " +
            ")")
    Page<WalletCredit> listCreditByUsers(@Param("currency_id") String currencyId,
                                         @Param("user_ids") Collection<String> userIds,
                                         Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT * FROM wallet_credit WHERE user_id=:user_id AND updated_at BETWEEN SUBDATE(CURDATE(), 1) AND CURDATE() limit 1")
    Optional<WalletCredit> findByUserId(@Param("user_id") String userId);

    Page<WalletCredit> findByUserId(String userId, Pageable pageable);

    @Query("SELECT c FROM WalletCredit c WHERE activity_type = :type AND user_id = :userId ORDER BY created_at DESC")
    Page<WalletCredit> findAllByType(@Param("type") String type, Pageable pageable, @Param("userId") String userId);
}
