package com.sysupgrade.valet.wallet.model.paymentmethod;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class PaymentMethodCreation {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String currencyId;

    @NotNull
    private String baseCurrencyId;

    @NotNull
    private String paymentId;

    @NotNull
    private int priority;

    @NotNull
    private int platformSupport;


    @NotNull
    private int randomNumber;

    private String desc;

    @NotNull
    private String name;

    @NotNull
    private BigDecimal rate;

    @NotNull
    private String iconUrl;

}
