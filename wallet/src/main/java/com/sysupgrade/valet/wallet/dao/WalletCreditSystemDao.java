package com.sysupgrade.valet.wallet.dao;

import com.sysupgrade.valet.wallet.orm.WalletCreditSystem;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletCreditSystemDao extends PagingAndSortingRepository<WalletCreditSystem, String> {
}
