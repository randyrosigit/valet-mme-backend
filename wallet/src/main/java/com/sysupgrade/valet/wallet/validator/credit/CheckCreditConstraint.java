package com.sysupgrade.valet.wallet.validator.credit;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckCreditValidator.class)
public @interface CheckCreditConstraint {
    String message() default ErrorHelper.NOT_ENOUGH_CREDIT_WALLET;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
