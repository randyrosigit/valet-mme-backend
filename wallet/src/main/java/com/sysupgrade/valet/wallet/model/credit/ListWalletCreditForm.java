package com.sysupgrade.valet.wallet.model.credit;

import com.sysupgrade.valet.wallet.orm.WalletCredit;
import lombok.Data;
import org.springframework.data.domain.Page;


@Data
public class ListWalletCreditForm {

    private String search;

    private String error;

    private Page<WalletCredit> walletCredits;
}
