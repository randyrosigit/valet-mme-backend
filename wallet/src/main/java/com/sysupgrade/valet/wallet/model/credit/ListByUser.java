package com.sysupgrade.valet.wallet.model.credit;

import com.sysupgrade.valet.wallet.orm.WalletCredit;
import lombok.Data;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

@Data
public class ListByUser {

    @NotNull
    private String userId;

    private String error;

    private Page<WalletCredit> walletCredits;
}
