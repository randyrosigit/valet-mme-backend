package com.sysupgrade.valet.wallet.model.credit;

import com.sysupgrade.valet.wallet.orm.WalletCredit;
import lombok.Data;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class
ListWalletCreditByCurrencies {

    @NotNull
    private String[] currencies;

    private String activity;

    private Date startDate;

    private Date endDate;

    private boolean detail;

    private String error;

    private Page<WalletCredit> walletCredits;
}
