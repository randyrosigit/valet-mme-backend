package com.sysupgrade.valet.wallet.model.credit;

import lombok.Data;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class AdjustmentForm {

    @NotNull
    private String userId;

    @NotNull
    private String currencyId;

    @NotNull
    private BigDecimal amount;

    private String error;
}
