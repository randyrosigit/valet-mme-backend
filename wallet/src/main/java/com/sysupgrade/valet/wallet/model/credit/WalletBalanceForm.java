package com.sysupgrade.valet.wallet.model.credit;

import lombok.Data;

@Data
public class WalletBalanceForm {

    private String balance;

    private String userId;

    private String error;

    private String address;
}
