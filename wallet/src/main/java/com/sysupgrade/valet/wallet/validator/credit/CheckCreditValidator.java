package com.sysupgrade.valet.wallet.validator.credit;

import com.sysupgrade.valet.wallet.dao.WalletCreditDao;
import com.sysupgrade.valet.wallet.model.credit.WalletTransferForm;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class CheckCreditValidator implements ConstraintValidator<CheckCreditConstraint, WalletTransferForm> {

    @Autowired
    private WalletCreditDao walletCreditDao;

    @Override
    public void initialize(CheckCreditConstraint contactNumber) {
    }

    @Override
    public boolean isValid(WalletTransferForm walletTransferForm,
                           ConstraintValidatorContext cxt) {
        Optional<WalletCredit> currentWalletCreditOptional = this.walletCreditDao.getUserCreditByCurrencyId(walletTransferForm.getCurrentUserId(),
                walletTransferForm.getCurrencyId());
        if(!currentWalletCreditOptional.isPresent()) {
            return false;
        }

        WalletCredit currentWalletCredit = currentWalletCreditOptional.get();
        return currentWalletCredit.getTotalCredit().compareTo(walletTransferForm.getAmount()) > 0 ||
                currentWalletCredit.getTotalCredit().compareTo(walletTransferForm.getAmount()) == 0;
    }
}
