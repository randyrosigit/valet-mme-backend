package com.sysupgrade.valet.wallet.service;

import com.sysupgrade.valet.wallet.dao.WalletCreditDao;
import com.sysupgrade.valet.wallet.model.credit.AdjustmentForm;
import com.sysupgrade.valet.wallet.model.credit.ListWalletCreditByTypeForm;
import com.sysupgrade.valet.wallet.model.credit.WalletTransferForm;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

@Service
public class WalletCreditService {

    @Autowired
    private WalletCreditDao walletCreditDao;

    private Logger logger = LoggerFactory.getLogger(WalletCreditService.class);

    public Page<WalletCredit> list(Pageable pageable, String userId) {

        if (userId != null) {
            return walletCreditDao.findByUserId(userId, pageable);
        } else {
            return this.walletCreditDao.findAll(
                    PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                            Sort.Direction.DESC, "id"));
        }
    }

    public Page<WalletCredit> listCreditByUsers(ArrayList<String> userIds, String currencyId) {
        return walletCreditDao.listCreditByUsers(currencyId, userIds, PageRequest.of(0, userIds.size()));
    }

    public Page<WalletCredit> listByUser(String userId, Pageable pageable) {
        return walletCreditDao.findByUserId(userId, pageable);
    }

    public Page<WalletCredit> listByCurrency(String currencyId, Pageable pageable) {
        return this.walletCreditDao.findAllByCurrencyId(currencyId, pageable);
    }

    public Page<WalletCredit> listByCurrencies(Pageable pageable, String userId, String[] currencyIds) {
        return this.walletCreditDao.findAll(userId, Arrays.asList(currencyIds), pageable);
    }

    public Page<WalletCredit> listByType(ListWalletCreditByTypeForm form, Pageable pageable, String userId) {
        return this.walletCreditDao.findAllByType(form.getType(), pageable, userId);
    }

    @Transactional
    public void transfer(WalletTransferForm walletTransferForm) {
        Date currentTime = new Date(System.currentTimeMillis());

        this.walletCreditDao.updateCredit(
                walletTransferForm.getCurrentUserId(),
                walletTransferForm.getCurrencyId(),
                walletTransferForm.getAmount().negate(),
                null,
                currentTime,
                currentTime,
                WalletCredit.TRANSFER_CREDIT_ACTIVITY_TYPE,
                WalletCredit.STATUS_ACTIVE
        );

        int numberRowAffected = this.walletCreditDao.updateCredit(
                walletTransferForm.getTargetUserId(),
                walletTransferForm.getCurrencyId(),
                walletTransferForm.getAmount() ,
                null,
                currentTime,
                currentTime,
                WalletCredit.TRANSFERRED_CREDIT_ACTIVITY_TYPE,
                WalletCredit.STATUS_ACTIVE
        );

        if(numberRowAffected == 0) {
            WalletCredit walletCredit = new WalletCredit();
            walletCredit.setCurrencyId(walletTransferForm.getCurrencyId());
            walletCredit.setUserId(walletTransferForm.getTargetUserId());
            walletCredit.setActivityType(WalletCredit.ADJUSTMENT_CREDIT_ACTIVITY_TYPE);
            walletCredit.setAdjustmentValue(walletTransferForm.getAmount());
            walletCredit.setTotalCredit(walletTransferForm.getAmount());
            walletCredit.setNote(null);
            walletCredit.setStatus(WalletCredit.STATUS_ACTIVE);
            walletCreditDao.save(walletCredit);
        }

    }

    @Transactional
    public void adjustment(String userId, String currencyId, BigDecimal amount, String activityType, String note) {
        Date currentTime = new Date(System.currentTimeMillis());
        int numberRowAffected = this.walletCreditDao.updateCredit(
                userId,
                currencyId,
                amount,
                note,
                currentTime,
                currentTime,
                activityType,
                WalletCredit.STATUS_ACTIVE
        );
        if(numberRowAffected == 0) {
            WalletCredit walletCredit = new WalletCredit();
            walletCredit.setCurrencyId(currencyId);
            walletCredit.setUserId(userId);
            walletCredit.setActivityType(activityType);
            walletCredit.setAdjustmentValue(amount);
            walletCredit.setTotalCredit(amount);
            walletCredit.setNote(note);
            walletCredit.setStatus(WalletCredit.STATUS_ACTIVE);
            walletCreditDao.save(walletCredit);
        }
    }

    @Transactional
    public void adjustment(String userId, String currencyId, BigDecimal amount, String activityType) {
        adjustment(userId, currencyId, amount, activityType, null);
    }

    @Transactional
    public void adjustment(AdjustmentForm adjustmentForm, String activityType) {
        this.adjustment(adjustmentForm.getUserId(), adjustmentForm.getCurrencyId(), adjustmentForm.getAmount(),
                activityType, null);
    }

    @Transactional
    public void adjustment(AdjustmentForm adjustmentForm) {
        this.adjustment(adjustmentForm.getUserId(), adjustmentForm.getCurrencyId(),
                adjustmentForm.getAmount(), WalletCredit.ADJUSTMENT_CREDIT_ACTIVITY_TYPE, null);
    }

    @Transactional
    public Optional<WalletCredit> getUserCreditByCurrencyId(String userId, String currencyId) {
        return this.walletCreditDao.getUserCreditByCurrencyId(userId, currencyId);
    }

    @Transactional
    public boolean checkCredit(String userId, String currencyId, BigDecimal amount) {
        Optional<WalletCredit> walletCreditOptional = walletCreditDao.getUserCreditByCurrencyId(userId, currencyId);

        if (!walletCreditOptional.isPresent()) {
            return false;
        }

        WalletCredit walletCredit = walletCreditOptional.get();
        // 0 = equal | 1 = wallet > amount | -1 = wallet < amount
        logger.info(amount.toString() + " " + walletCredit.getTotalCredit().toString() + " " +
                (walletCredit.getTotalCredit().compareTo(amount) != -1));
        if (walletCredit.getTotalCredit().compareTo(amount) != -1) {
            return true;
        }

        return false;
    }
    
    public BigDecimal getCreditByCurrencyId(String userId, String currencyId) {
        Optional<WalletCredit> walletCreditOptional = this.getUserCreditByCurrencyId(userId, currencyId);
        if(walletCreditOptional.isPresent()) {
            return  walletCreditOptional.get().getTotalCredit();
        }
        return BigDecimal.ZERO;
    }
}
