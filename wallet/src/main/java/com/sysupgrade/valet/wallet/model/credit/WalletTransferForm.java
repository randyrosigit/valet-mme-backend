package com.sysupgrade.valet.wallet.model.credit;

import com.sysupgrade.valet.wallet.validator.credit.CheckCreditConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@CheckCreditConstraint
public class WalletTransferForm {

    @NotNull
    private String targetUserId;

    @NotNull
    private String currentUserId;

    @NotNull
    private String currencyId;

    @NotNull
    private BigDecimal amount;

    private String error;
}
