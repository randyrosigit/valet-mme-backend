package com.sysupgrade.valet.wallet.controller;

import com.sysupgrade.valet.wallet.dao.WalletCreditSystemDao;
import com.sysupgrade.valet.wallet.orm.WalletCreditSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api/wallet/credit-system")
public class WalletCreditSystemController {

    private final WalletCreditSystemDao dao;

    @Autowired
    public WalletCreditSystemController(WalletCreditSystemDao dao) {
        this.dao = dao;
    }

    @GetMapping("/")
    public Page<WalletCreditSystem> list(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @PutMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public WalletCreditSystem save(@RequestBody @Valid WalletCreditSystem walletCreditSystem) {
        walletCreditSystem.setStatus(WalletCreditSystem.STATUS_ACTIVE);
        dao.save(walletCreditSystem);
        return walletCreditSystem;
    }

    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public WalletCreditSystem update(@RequestBody @Valid WalletCreditSystem walletCreditSystem) {
        dao.save(walletCreditSystem);
        return walletCreditSystem;
    }

    @PostMapping("/delete")
    @ResponseStatus(HttpStatus.CREATED)
    public WalletCreditSystem delete(@RequestBody WalletCreditSystem walletCreditSystem) {
        walletCreditSystem.setStatus(WalletCreditSystem.STATUS_DELETE);
        dao.save(walletCreditSystem);
        return walletCreditSystem;
    }

    @GetMapping("/{id}")
    public Optional<WalletCreditSystem> get(@PathVariable String id) {
        return dao.findById(id);
    }
}
