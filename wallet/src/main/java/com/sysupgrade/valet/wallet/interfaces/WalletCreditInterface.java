package com.sysupgrade.valet.wallet.interfaces;

import com.sysupgrade.valet.wallet.dao.WalletCreditDao;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class WalletCreditInterface {

    @Autowired
    WalletCreditDao walletCreditDao;

    public boolean creditConvertPeriod(String sourceCurrencyId,
                                    String targetCurrencyId,
                                    String userId,
                                    String activity,
                                    BigDecimal credit,
                                    String note) {

        Optional<WalletCredit> walletCreditSourceOptional = walletCreditDao.getUserCreditByCurrencyId(userId, sourceCurrencyId);
        Optional<WalletCredit> walletCreditTargetOptional = walletCreditDao.getUserCreditByCurrencyId(userId, targetCurrencyId);

        if (!walletCreditSourceOptional.isPresent() ||
            walletCreditSourceOptional.get().getTotalCredit().compareTo(credit) < 0) {
            return false;
        }

        WalletCredit walletCredit = new WalletCredit();
        walletCredit.setUserId(userId);
        walletCredit.setCurrencyId(sourceCurrencyId);
        walletCredit.setActivityType(activity);
        walletCredit.setStatus(WalletCredit.STATUS_ACTIVE);
        walletCredit.setAdjustmentValue(credit);
        walletCredit.setTotalCredit(walletCreditSourceOptional.get().getTotalCredit().add(credit.negate()));
        walletCredit.setNote(note);
        walletCreditDao.save(walletCredit);

        walletCredit = new WalletCredit();
        walletCredit.setUserId(userId);
        walletCredit.setCurrencyId(targetCurrencyId);
        walletCredit.setActivityType(activity);
        walletCredit.setStatus(WalletCredit.STATUS_ACTIVE);
        walletCredit.setAdjustmentValue(credit);
        if(walletCreditTargetOptional.isPresent()) {
            walletCredit.setTotalCredit(walletCreditTargetOptional.get().getTotalCredit().add(credit));
        }
        else {
            walletCredit.setTotalCredit(credit);
        }
        walletCredit.setNote(note);
        walletCreditDao.save(walletCredit);

        return  true;

    }
}