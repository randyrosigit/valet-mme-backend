package com.sysupgrade.valet.wallet.model.paymentmethod;

import com.sysupgrade.valet.wallet.orm.WalletPaymentMethod;
import lombok.Data;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

@Data
public class ListPaymentByCurrency {

    @NotNull
    private String currencyId;

    private String error;

    private Page<WalletPaymentMethod> paymentMethods;
}
