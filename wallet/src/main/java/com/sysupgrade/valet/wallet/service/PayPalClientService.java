package com.sysupgrade.valet.wallet.service;


import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.sysupgrade.valet.notification.model.NotificationForm;
import com.sysupgrade.valet.notification.service.NotificationService;

import com.sysupgrade.valet.wallet.dao.WalletPaypalDao;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import com.sysupgrade.valet.wallet.orm.WalletPaypal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
public class PayPalClientService {

    @Autowired
    WalletPaypalDao paypalDao;

    @Autowired
    private Environment environment;

    @Autowired
    private WalletCreditService creditService;

    @Autowired
    private NotificationService notificationService;

    @Transactional
    public Map<String, Object> createPayment(String userId, String payTotal) throws Exception {

        String redirectURL = environment.getProperty("paypal.redirect-url");

        String uniqueCode = UUID.randomUUID().toString();

        Map<String, Object> response = new HashMap<String, Object>();
        Amount amount = new Amount();
        amount.setCurrency("USD");
        amount.setTotal(getTotalPayment(Double.valueOf(payTotal)));
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl(redirectURL + "/api/wallet/paypal/topup/cancel");
        redirectUrls.setReturnUrl(redirectURL + "/api/wallet/paypal/topup/complete/" + uniqueCode + "/payment");
        payment.setRedirectUrls(redirectUrls);
        Payment createdPayment;
        try {
            String redirectUrl = "";
            APIContext context = new APIContext(environment.getProperty("paypal.client-id"), environment.getProperty("paypal.client-secret"), environment.getProperty("paypal.mode"));
            createdPayment = payment.create(context);
            if (createdPayment != null) {
                List<Links> links = createdPayment.getLinks();
                for (Links link : links) {
                    if (link.getRel().equals("approval_url")) {
                        redirectUrl = link.getHref();

                        this.createPaypal(userId, payTotal, uniqueCode);

                        break;
                    }
                }
                response.put("status", "success");
                response.put("redirect_url", redirectUrl);
            }
        } catch (PayPalRESTException e) {
            System.out.println("Error happened during payment creation!");
        }
        return response;
    }

    @Transactional
    public String completePayment(String paymentId, String PayerId, String uniqueCode) throws Exception {
        String redirectURL = "";

        Map<String, Object> response = new HashMap();
        Payment payment = new Payment();
        payment.setId(paymentId);

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(PayerId);
        try {
            APIContext context = new APIContext(environment.getProperty("paypal.client-id"), environment.getProperty("paypal.client-secret"), environment.getProperty("paypal.mode"));
            Payment createdPayment = payment.execute(context, paymentExecution);
            if (createdPayment != null) {
                response.put("status", "success");
                response.put("payment", createdPayment);

                String userId = this.completePaypal(paymentId, PayerId, uniqueCode);
                redirectURL = "success";
                this.createNotification(userId, "success top up with paypal", "成功充值宝贝");
            }
        } catch (PayPalRESTException e) {
            System.err.println(e.getDetails());
            redirectURL = e.getMessage();
            this.createNotification(getUserIdByUniqueCode(uniqueCode), "failed top Up with paypal", "失败顶部用贝宝");
        }
        return redirectURL;
    }

    private String completePaypal(String paymentId, String PayerId, String uniqueCode) {

        Optional<WalletPaypal> paypalOptional = paypalDao.findByUniqueCode(uniqueCode);
        if (!paypalOptional.isPresent()) {
            return null;
        }

        paypalOptional.get().setStatus(WalletPaypal.STATUS_ACTIVE);
        paypalDao.save(paypalOptional.get());

//        this.creditService.adjustment(
//                paypalOptional.get().getUserId(),
//                WalletCredit.CURRENCY_CASH,
//                paypalOptional.get().getAdjustmentValue(),
//                WalletCredit.PAYPAL_ACTIVITY_TYPE,
//                saveNote(PayerId, paymentId, uniqueCode),
//                WalletCredit.STATUS_ACTIVE,
//                new Date(),
//                null);

        creditService.adjustment(paypalOptional.get().getUserId(), "SGD-221133", paypalOptional.get().getAdjustmentValue(), WalletCredit.PAYPAL_ACTIVITY_TYPE);
        return paypalOptional.get().getUserId();
    }

    private void createPaypal(String userId, String amount, String uniqueCode) {
        WalletPaypal paypal = new WalletPaypal();
        paypal.setActivityType(WalletPaypal.TOPUP_ACTIFITY);
        paypal.setAdjustmentValue(new BigDecimal(amount));
        paypal.setStatus(WalletPaypal.STATUS_PENDING);
        paypal.setUserId(userId);
        paypal.setUniqueCode(uniqueCode);
        paypalDao.save(paypal);
    }

    private String saveNote(String PayerId, String paymentId, String uniqueCode) {
        return
                "unique code : " + uniqueCode +
                        ", paymentId : " + paymentId +
                        ", PayerId : " + PayerId;

    }

    private void createNotification(String userId, String description, String chineseDescription) throws Exception {
        NotificationForm form = new NotificationForm();
        form.setUserId(userId);
        form.setName("paypal address");
        form.setChineseName("贝宝通知");
        form.setDescription(description);
        form.setChineseDescription(chineseDescription);
//        notificationService.createNotification(form);
    }


    private String getUserIdByUniqueCode(String uniqueCode) {
        Optional<WalletPaypal> paypalOptional = paypalDao.findByUniqueCode(uniqueCode);
        return paypalOptional.map(WalletPaypal::getUserId).orElse(null);
    }


    private String getTotalPayment(double payment) {
        double admin = (payment * 4.5) / 100;
        double total = payment + admin;
        return String.valueOf(total);
    }

}


