package com.sysupgrade.valet.wallet.model.credit;

import com.sysupgrade.valet.wallet.orm.WalletCredit;
import lombok.Data;

@Data
public class GetCreditForm {

    private String currencyId;

    private String error;

    private WalletCredit credit;
}
