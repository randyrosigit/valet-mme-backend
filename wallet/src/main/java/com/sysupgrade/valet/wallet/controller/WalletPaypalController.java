package com.sysupgrade.valet.wallet.controller;

import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import com.sysupgrade.valet.wallet.service.PayPalClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Map;


@RestController
@RequestMapping("/api/wallet/paypal")
public class WalletPaypalController {

    @Autowired
    private IAuthenticationFacade authenticationFacade;


    @Autowired
    private Environment environment;

    @Autowired
    private PayPalClientService payPalClientService;


    @CrossOrigin("*")
    @PreAuthorize("hasAnyAuthority('USER', 'DRIVER')")
    @GetMapping(value = "topup/make/payment")
    public Map<String, Object> makePayment(@RequestParam("amount") String amount) throws Exception {
        String userId = authenticationFacade.getMyFace().getId();
        return payPalClientService.createPayment(userId, amount);
    }


    @CrossOrigin("*")
    @GetMapping(value = "topup/complete/{uniqueCode}/payment")
    public RedirectView completePayment(@RequestParam String paymentId, @RequestParam String PayerID, @PathVariable String uniqueCode) throws Exception {

        String redirectUrl = payPalClientService.completePayment(paymentId, PayerID, uniqueCode);
        RedirectView redirectView = new RedirectView();
        if (redirectUrl.equals("success")) {
            redirectView.setUrl(environment.getProperty("frontend.successUrl"));
        } else {
            redirectView.setUrl(environment.getProperty("frontend.failureUrl"));
        }
        return redirectView;
    }
}
