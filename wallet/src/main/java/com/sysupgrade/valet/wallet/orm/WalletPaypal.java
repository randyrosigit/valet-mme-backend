package com.sysupgrade.valet.wallet.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "wallet_paypal")
@Data
public class WalletPaypal {


    public final static int STATUS_DELETE = 0;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_PENDING = 11;


    public final static String TOPUP_ACTIFITY = "top up";


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "activity_type")
    private String activityType;

    @NotNull
    @Column(name = "adjustment_value")
    private BigDecimal adjustmentValue;

    @NotNull
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;

    @NotNull
    private String uniqueCode;

}
