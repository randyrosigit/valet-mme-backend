package com.sysupgrade.valet.wallet.config;

public class PaymentConfig {

    public static final String KING_O_PAY_ID = "kingopayid";

    public static final String PAYPAL_ID = "paypalid";

    public static final String MIDTRANS_ID = "midtrans";

    public static final String AlIPAY_ID = "alipay";

    public static final String CASH = "cash";

    public static final String CREDIT_CARD = "creditcard";

    public static final String WALLET_CREDIT = "walletcredit";
}
