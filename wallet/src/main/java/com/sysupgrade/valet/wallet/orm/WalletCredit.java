package com.sysupgrade.valet.wallet.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "wallet_credit")
@Data
public class WalletCredit {

    public final static int STATUS_DELETE = 0;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_PENDING = 11;

    public final static String TRANSFER_CREDIT_ACTIVITY_TYPE = "transfer";

    public final static String TRANSFERRED_CREDIT_ACTIVITY_TYPE = "transferred";

    public final static String ADJUSTMENT_CREDIT_ACTIVITY_TYPE = "adjustment";

    public final static String USED_TO_PURCHASE_CREDIT_ACTIVITY_TYPE = "used_to_purchase";

    public final static String PURCHASED_CREDIT_ACTIVITY_TYPE = "purchased";

    public final static String COST_PURCHASE_ACTIVITY_TYPE = "cost_purchase";

    public final static String REWARD_ACTIVITY_TYPE = "reward";

    public final static String TRANSFER_OUT_ACTIVITY_TYPE = "transfer_out";

    public final static String TRANSFER_OUT_COST_ACTIVITY_TYPE = "transfer_out_cost";

    public final static String VC_CONVERSION_ACTIVITY_TYPE = "vc_conversion";

    public final static String CONVERSION_FEE = "conversion_fee";

    public final static String TRANSFER_ETH = "transfer_eth";

    public final static String TRANSFER_OUT_ETH = "transfer_out_eth";

    public final static String TRANSFER_OUT_ETH_COST = "transfer_out_eth_cost";

    public final static String GUESS_GAME = "guess";

    public final static String PURCHASHED_ECOMMERCE_ITEM = "purchase_ecommerce_item";

    public final static String USED_TO_PURCHASE_ECOMMERCE = "used_to_purchase_ecommerce";

    public final static String BUYER_RETURN_FOR_ECOMMERCE = "buyer_return_for_ecommerce";

    public final static String RETURN_USER_CREDIT_DUE_TO_REJECT_ORDER = "return_user_credit_due_to_reject_order";

    public final static String DEDUCT_MERCHANT_CREDIT_DUE_TO_REJECT_ORDER = "deduct_merchant_credit_due_to_reject_order";

    public final static String RETURN_CONSUMED_ECOMMERCE_DUE_TO_REJECT_ORDER = "return_consumed_ecommerce_due_to_reject_order";

    public final static String DEDUCT_USED_CURRENCY_DUE_TO_REJECT_ORDER = "deduct_used_currency_due_to_reject_order";

    public final static String CONSUMED_ECOMMERCE = "consumed_ecommerce";

    public final static String REFUND_ECOMMERCE = "refund_ecommerce";

    public final static String ITEM_SELLING = "item_selling";

    public final static String KINGPAY_ACTIVITY_TYPE = "KINGPAY-topup";

    public final static String PAYPAL_ACTIVITY_TYPE = "PAYPAL-topup";

    public final static String JOB_ORDER = "Order";

    public final static String CHARGE = "CHARGE";
    public final static String CURRENCY_CASH = "cash";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "total_credit")
    private BigDecimal totalCredit;

    @NotNull
    @Column(name = "currency_id")
    private String currencyId;

    @NotNull
    @Column(name = "activity_type")
    private String activityType;

    @NotNull
    @Column(name = "adjustment_value")
    private BigDecimal adjustmentValue;

    @Column(name = "note")
    private String note;

    @NotNull
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;

}
