package com.sysupgrade.valet.wallet.controller;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import com.sysupgrade.valet.user.model.user.UserAuthenticationForm;
import com.sysupgrade.valet.wallet.model.credit.*;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import com.sysupgrade.valet.wallet.service.WalletCreditService;
import com.sysupgrade.valet.wallet.model.credit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.net.*;
import java.io.*;

@RestController
@RequestMapping("/api/wallet/credit")
public class WalletCreditController {

    @Autowired
    private WalletCreditService walletCreditService;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/admin/list-by-user")
    public ResponseEntity<ListByUser> listByUser(@Valid ListByUser form,
                                                 Pageable pageable,
                                                 BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            Page<WalletCredit> walletCredits = this.walletCreditService.listByUser(form.getUserId(), pageable);
            form.setWalletCredits(walletCredits);
        }
        catch (RuntimeException e) {
            form.setError(e.getMessage());
            return ResponseEntity.badRequest().body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/admin/list-by-currency")
    public ResponseEntity<ListWalletCreditByCurrency>
        listByCurrency(@Valid ListWalletCreditByCurrency form,
                         BindingResult bindingResult,
                         Pageable pageable) {
        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            Page<WalletCredit> walletCredits = this.walletCreditService.listByCurrency(form.getCurrencyId(), pageable);
            form.setWalletCredits(walletCredits);
        }
        catch (RuntimeException e) {
            form.setError(e.getMessage());
            return ResponseEntity.badRequest().body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'DRIVER')")
    @GetMapping("/get-by-currency")
    public ResponseEntity<GetCreditForm> getCredit(@Valid GetCreditForm form, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        String userId = authenticationFacade.getMyFace().getId();
        form.setCurrencyId("SGD-221133");

        try {
            Optional<WalletCredit> credit = this.walletCreditService.getUserCreditByCurrencyId(userId, form.getCurrencyId());
            form.setCredit(credit.isPresent() ? credit.get() : null);
        }
        catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.badRequest().body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/adjust")
    public ResponseEntity<AdjustmentForm> adjustment(@Valid @RequestBody AdjustmentForm adjustmentForm, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            adjustmentForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(adjustmentForm);
        }

        try {
            this.walletCreditService.adjustment(adjustmentForm);
        }
        catch (RuntimeException e) {
            adjustmentForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(adjustmentForm);
        }

        return ResponseEntity.ok(adjustmentForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("/list-by-currencies")
    public ResponseEntity<ListWalletCreditByCurrencies>
        listByCurrencies(@Valid @RequestBody  ListWalletCreditByCurrencies form,
                         BindingResult bindingResult,
                         Pageable pageable) {

        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }
        String userId = authenticationFacade.getMyFace().getId();
        try {
            Page<WalletCredit> walletCredits =
                    this.walletCreditService.listByCurrencies(pageable, userId,
                        form.getCurrencies());
            form.setWalletCredits(walletCredits);
        }
        catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'DRIVER')")
    @GetMapping("/list")
    public ResponseEntity<ListWalletCreditForm> list( ListWalletCreditForm form, Pageable pageable, String userId) {

        UserAuthenticationForm authenticationForm = authenticationFacade.getUserAuthenticationForm();
        if (!authenticationForm.isAdmin()){
            userId = authenticationForm.getUser().getId();
        }

        try {
            Page<WalletCredit> walletCredits = this.walletCreditService.list(pageable, userId);
            form.setWalletCredits(walletCredits);
        }
        catch (RuntimeException e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'DRIVER')")
    @GetMapping("/list-by-type")
    public ResponseEntity<ListWalletCreditByTypeForm> listByType(ListWalletCreditByTypeForm form, Pageable pageable) {

        String userId = authenticationFacade.getMyFace().getId();
        try {
            Page<WalletCredit> walletCredits = this.walletCreditService.listByType(form, pageable, userId);
            form.setWalletCredits(walletCredits);
        }
        catch (RuntimeException e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }




}
