package com.sysupgrade.valet.wallet.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "wallet_payment_method")
@Data
public class WalletPaymentMethod {

    public final static int WEB_PLATFORM_SUPPORT = 1;

    public final static int MOBILE_PLATFORM_SUPPORT = 2;

    public final static int ALL_PLATFORM_SUPPORT = 3;

    public final static int STATUS_ACTIVE = 10;

    public final static int STATUS_NOT_ACTIVE = 11;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "payment_id")
    private String paymentId;

    @NotNull
    @Column(name = "currency_id")
    private String currencyId;

    @NotNull
    @Column(name = "base_currency_id")
    private String baseCurrencyId;

    @NotNull
    @Column(name = "icon_url")
    private String iconUrl;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "status")
    private int status;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "priority")
    private int priority;

    @NotNull
    @Column(name = "random_number")
    private int randomNumber;

    @Column(name = "extra_data")
    private String extraData;

    @NotNull
    @Column(name = "rate")
    private BigDecimal rate;

    @NotNull
    @Column(name = "platform_support")
    private int platformSupport;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;

}
