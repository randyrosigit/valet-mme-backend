CREATE TABLE referral_code(
  id VARCHAR(36) PRIMARY KEY ,
  user_id VARCHAR (36) NOT NULL UNIQUE ,
  code VARCHAR(36) NOT NULL UNIQUE ,
  status    SMALLINT       NOT NULL,
  created_at TIMESTAMP    NOT NULL,
  updated_at TIMESTAMP    NOT NULL
);


-- //rewod
CREATE TABLE referral_marketing(
  id VARCHAR(36) PRIMARY KEY ,
  currency_id VARCHAR (36) NOT NULL ,
  total_credit DECIMAL(36,18)  NOT NULL ,
  total_currency_credit DECIMAL(36,18)  NOT NULL ,
  status    SMALLINT       NOT NULL,
  created_at TIMESTAMP    NOT NULL,
  updated_at TIMESTAMP    NOT NULL
);
-- //register user
CREATE TABLE referral(
  id VARCHAR(36) PRIMARY KEY ,
  source_user_id VARCHAR (36),
  referred_user_id VARCHAR (36) NOT NULL,
  used_code VARCHAR (36),
  status    SMALLINT       NOT NULL,
  created_at TIMESTAMP    NOT NULL,
  updated_at TIMESTAMP    NOT NULL
);

CREATE TABLE referral_transaction(
  id VARCHAR(36) PRIMARY KEY ,
  user_id VARCHAR (36) NOT NULL ,
  total_credit DECIMAL(36,18)  NOT NULL ,
  source_user_id VARCHAR (36) NOT NULL ,
  activity_type VARCHAR (36) NOT NULL ,
  status    SMALLINT       NOT NULL,
  created_at TIMESTAMP    NOT NULL,
  updated_at TIMESTAMP    NOT NULL
);

CREATE TABLE referral_level(
  id INT PRIMARY KEY AUTO_INCREMENT,
  level INT NOT NULL ,
  currency_id VARCHAR (36) NOT NULL ,
  return_amount DECIMAL(36,18)  NOT NULL,
  status    SMALLINT       NOT NULL,
  created_at TIMESTAMP    NOT NULL,
  updated_at TIMESTAMP    NOT NULL
);

CREATE TABLE referral_vc_schedule (
	user_id varchar(36) not null primary key,
	last_schedule_at timestamp not null,
	total_past_scheduled int not null default 0
);