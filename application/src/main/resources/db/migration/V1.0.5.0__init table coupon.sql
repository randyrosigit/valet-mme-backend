CREATE TABLE coupon
(
	id varchar(36) PRIMARY KEY,
	name varchar(255) NOT NULL,
	code varchar(255) NOT NULL,
	amount decimal(36,18) NOT NULL,
	percentage int not null default 0,
	status integer(11) NOT NULL,
	expired_date timestamp NOT NULL,
	deleted_status int  NOT NULL default 10,
	user_id varchar(255) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

CREATE TABLE coupon_users_coupons
(
  id varchar(36) PRIMARY KEY,
	coupon_id varchar(36) NOT NULL,
	user_id varchar(255) NOT NULL,
	status integer(11) NOT NULL,
	deleted_status integer(11) NOT NULL default 10,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

CREATE TABLE coupon_stamp_card
(
	id varchar(36) PRIMARY KEY,
	user_id varchar(36) NOT NULL,
	stamp_collected integer(11) NOT NULL,
	can_use integer(11) NOT NULL default 0,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);