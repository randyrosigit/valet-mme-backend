CREATE TABLE wallet_paypal
(
  id               VARCHAR(36) PRIMARY KEY,
  user_id          VARCHAR(36)     NOT NULL,
  adjustment_value DECIMAL(36, 18) NOT NULL,
  activity_type    VARCHAR(50)     NOT NULL,
  status           SMALLINT        NOT NULL,
  created_at       TIMESTAMP       NOT NULL,
  updated_at       TIMESTAMP       NOT NULL,
  unique_code      VARCHAR(36)     NOT NULL UNIQUE
)