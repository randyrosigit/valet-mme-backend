create table user_account
(
    id VARCHAR(36) PRIMARY KEY ,
    status smallint not null,
    created_at timestamp not null,
    updated_at timestamp not null,
    auth_key varchar(36) collate utf8_unicode_ci  not null,
    first_name varchar(255) collate utf8_unicode_ci not null,
    last_name varchar(255) collate utf8_unicode_ci null,
    username varchar(255) not null UNIQUE,
    enabled boolean not null default 0,
    password varchar(100) not null,
    front_ic_file_id varchar(36) null,
    back_ic_file_id varchar(36) NULL,
    photo_file_id varchar(36) NULL,
    address text null,
    phone varchar(255) null,
    postal_code varchar(10) null,
    country varchar(100) null,
    ic_number varchar(255) null,
    driving_experience integer(11) null
);

create table user_email_authentication
(
    id varchar(36) not null primary key,
    user_id varchar(36) not null ,
    password_reset_token varchar(255) null,
    primary_email tinyint(1) default '1' not null,
    email varchar(100) not null unique ,
    valid tinyint(1) default '0' not null,
    validation_token varchar(32) null,
    reset_token varchar(32) null,
    created_at timestamp not null,
    updated_at timestamp not null,
    status smallint(6) not null,
    foreign key (user_id) references user_account (id)
);

create table user_email_validation (
    id int not null primary key auto_increment,
    email varchar(100) not null,
    validation_token varchar(6) not null,
    status smallint(6) not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

create table user_role (
  name        varchar(10) not null primary key ,
  description text         null,
  created_at  timestamp    not null,
  updated_at  timestamp    not null
);

create table user_users_roles
(
  user_id varchar(36) not null,
  role_id varchar(10) not null,
  driver_priority varchar(2) null default null,
  foreign key (user_id) references user_account (id),
  foreign key (role_id) references user_role (name),
  primary key (role_id, user_id)
);

create table user_phone_validation (
    id int not null primary key auto_increment,
    phone_code varchar(10),
    phone varchar(20),
    validation_token varchar(6) not null,
    status smallint(6) not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

create table user_phone_authentication (
    id int not null primary key auto_increment,
    phone varchar(20),
    phone_code varchar(10),
    user_id varchar(36) not null,
    password_reset_token varchar(255) null,
    status smallint(6) not null,
    created_at timestamp not null,
    updated_at timestamp not null
);
