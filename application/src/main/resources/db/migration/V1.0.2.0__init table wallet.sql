
CREATE TABLE wallet_credit
(
  id               int             not null PRIMARY KEY AUTO_INCREMENT,
  user_id          VARCHAR(36)     NOT NULL,
  total_credit     DECIMAL(36, 18) NOT NULL,
  currency_id      VARCHAR(36)     NOT NULL,
  activity_type    VARCHAR(50)     NOT NULL,
  adjustment_value DECIMAL(36, 18),
  note             TEXT,
  status           SMALLINT        NOT NULL,
  created_at       TIMESTAMP       NOT NULL,
  updated_at       TIMESTAMP       NOT NULL,
  transaction_date TIMESTAMP       NOT NULL,
  proof_image_id   varchar(36)
);

CREATE TABLE wallet_credit_system
(
  id               INT             NOT NULL PRIMARY KEY AUTO_INCREMENT,
  total_credit     DECIMAL(36, 18) NOT NULL,
  currency_id      VARCHAR(36)     NOT NULL,
  activity_type    VARCHAR(50)     NOT NULL,
  note             TEXT,
  adjustment_value DECIMAL(36, 18),
  status           SMALLINT        NOT NULL,
  created_at       TIMESTAMP       NOT NULL,
  updated_at       TIMESTAMP       NOT NULL
);

CREATE TABLE wallet_payment_method (
    id varchar(36) primary key,
    payment_id varchar(36) not null,
    currency_id varchar(36) not null,
    base_currency_id varchar(36) not null default " ",
    icon_url varchar(500) not null,
    name varchar(300) not null,
    status smallint(6) not null,
    description text null,
    priority int not null default 0,
    random_number smallint(6) not null COMMENT '1:yes, 2:no',
    extra_data text null,
    platform_support int not null,
    rate decimal(36,18) not null default 0,
    created_at Date not null,
    updated_at Date not null
);