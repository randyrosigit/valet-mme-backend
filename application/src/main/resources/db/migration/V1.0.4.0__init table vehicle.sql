CREATE TABLE vehicle
(
	id varchar(36) PRIMARY KEY,
	user_id varchar(36) NOT NULL,
	plate_number varchar(255) NOT NULL,
	brand varchar(255) NOT NULL,
	color varchar(255) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);