
CREATE TABLE job_extra
(
	id varchar(36) PRIMARY KEY,
	job_id varchar(36) NOT NULL,
	total_location integer(2) NOT NULL,
	waiting_time integer(5) NOT NULL,
	price decimal(36,18) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

