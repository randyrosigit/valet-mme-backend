ALTER TABLE job_pick_up DROP pickup_time;
ALTER TABLE job_pick_up ADD pickup_time datetime NOT NULL;
ALTER TABLE job_pick_up ADD friend_phone_number VARCHAR(36) NULL;
