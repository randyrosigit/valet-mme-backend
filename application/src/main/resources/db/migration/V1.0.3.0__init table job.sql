CREATE TABLE job
(
	id varchar(36) PRIMARY KEY,
	user_id varchar(36) NOT NULL,
	driver_id varchar(36) NULL,
	coupon_id varchar(36) NULL,
	payment_method_id varchar(36) NOT NULL,
	price decimal(36,18) NOT NULL,
	distance decimal(36,18) NOT NULL,
	status integer(11) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

CREATE TABLE job_pick_up
(
	id varchar(36) PRIMARY KEY,
	job_id varchar(36) NOT NULL,
	vehicle_id varchar(36) NULL,
	latitude varchar(255) NOT NULL,
	longitude varchar(255) NOT NULL,
	note text NULL,
	address text NOT NULL,
	pickup_time timestamp NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

CREATE TABLE job_destination
(
	id varchar(36) PRIMARY KEY,
	job_id varchar(36) NOT NULL,
	latitude varchar(255) NOT NULL,
	longitude varchar(255) NOT NULL,
	address text NOT NULL,
	sequence integer(11) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

CREATE TABLE job_recall
(
	id varchar(36) PRIMARY KEY,
	job_id varchar(36) NOT NULL,
	latitude varchar(255) NOT NULL,
	longitude varchar(255) NOT NULL,
	address text NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
);

CREATE TABLE job_payment_method
(
    id varchar(36) PRIMARY KEY,
	currency_id varchar(36) NOT NULL,
	name varchar(255) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL
)