
CREATE TABLE phone_code
(
	phone_code varchar(4) PRIMARY KEY,
	country varchar(11) NULL
);