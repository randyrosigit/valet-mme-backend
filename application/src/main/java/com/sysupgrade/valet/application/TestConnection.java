package com.sysupgrade.valet.application;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/api/public")
public class TestConnection {
    @GetMapping("/ok")
    public ModelAndView test() {
        ModelAndView mdoelAndView = new ModelAndView();
        mdoelAndView.setViewName("ok");

        return mdoelAndView;
    }
}
