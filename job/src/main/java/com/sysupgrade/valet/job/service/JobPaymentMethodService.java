package com.sysupgrade.valet.job.service;

import com.sysupgrade.valet.job.dao.JobPaymentMethodDao;
import com.sysupgrade.valet.job.model.jobpaymentmethod.CreateJobPaymentMethodForm;
import com.sysupgrade.valet.job.orm.Job;
import com.sysupgrade.valet.job.orm.JobPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class JobPaymentMethodService {
    @Autowired
    JobPaymentMethodDao jobPaymentMethodDao;

    public Page<JobPaymentMethod> listJobPaymentMethod(Pageable pageable) {
        return jobPaymentMethodDao.findAll(pageable);
    }


//    public Optional<JobPaymentMethod> listJobPaymentMethod2() {
//        return jobPaymentMethodDao.findAll2();
//    }

    public Optional<JobPaymentMethod> findByPaymentIdAndCurrencyId(String id, String currencyId) {
        return this.jobPaymentMethodDao.findByIdAndCurrencyId(id, currencyId);
    }

    public Optional<JobPaymentMethod> findByNameAndCurrencyId(String name, String currencyId) {
        return this.jobPaymentMethodDao.findByNameAndCurrencyId(name, currencyId);

    }

    public Optional<JobPaymentMethod> findById(String id) {
        return this.jobPaymentMethodDao.findById(id);
    }

    @Transactional
    public JobPaymentMethod create(CreateJobPaymentMethodForm createJobPaymentMethodForm) {

        JobPaymentMethod jobPaymentMethod = new JobPaymentMethod();
        jobPaymentMethod.setCurrencyId(createJobPaymentMethodForm.getCurrencyId());
        jobPaymentMethod.setName(createJobPaymentMethodForm.getName());
        this.jobPaymentMethodDao.save(jobPaymentMethod);

        return jobPaymentMethod;
    }

}
