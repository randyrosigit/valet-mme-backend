package com.sysupgrade.valet.job.model.jobpickup;

import com.sysupgrade.valet.job.orm.JobPickup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class CreatePickupForm {
    @NotNull
    private String jobId;

    private String vehicleId;

    private String friendPhoneNumber;

    private String latitude;

    private String longitude;

    private String note;

    @NotNull
    private String address;

    private Date pickupTime;

    private String error;

    private JobPickup jobPickup;
}
