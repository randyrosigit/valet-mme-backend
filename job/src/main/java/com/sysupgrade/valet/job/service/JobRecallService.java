package com.sysupgrade.valet.job.service;

import com.sysupgrade.valet.job.dao.JobRecallDao;
import com.sysupgrade.valet.job.model.jobrecall.CreateJobRecallForm;
import com.sysupgrade.valet.job.orm.JobRecall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class JobRecallService {
    @Autowired
    private JobRecallDao jobRecallDao;

    public Page<JobRecall> listJobRecall(Pageable pageable) {
        return jobRecallDao.findAll(pageable);
    }

    @Transactional
    public JobRecall create(CreateJobRecallForm createJobRecallForm) {
        JobRecall jobRecall = new JobRecall();
        jobRecall.setJobId(createJobRecallForm.getJobId());
        jobRecall.setLongitude(createJobRecallForm.getLongitude());
        jobRecall.setLatitude(createJobRecallForm.getLatitude());
        jobRecall.setAddress(createJobRecallForm.getAddress());
        this.jobRecallDao.save(jobRecall);

        return jobRecall;
    }
}
