package com.sysupgrade.valet.job.controller;


import com.sysupgrade.valet.job.model.jobrecall.CreateJobRecallForm;
import com.sysupgrade.valet.job.model.jobrecall.ListJobRecallForm;
import com.sysupgrade.valet.job.orm.JobRecall;
import com.sysupgrade.valet.job.service.JobRecallService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/job/recall/")

public class JobRecallController {
    @Autowired
    private JobRecallService jobRecallService;

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ListJobRecallForm> listJobRecalls(ListJobRecallForm listJobRecallForm, Pageable pageable) {
        try {
            Page<JobRecall> jobRecall = jobRecallService.listJobRecall(pageable);

            listJobRecallForm.setJobRecalls(jobRecall);
        } catch (Exception e) {
            listJobRecallForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobRecallForm);
        }
        return ResponseEntity.ok(listJobRecallForm);
    }

    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @PostMapping("create")
    public ResponseEntity<CreateJobRecallForm> create(@RequestBody @Valid CreateJobRecallForm createJobRecallForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createJobRecallForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createJobRecallForm);
        }

        try {
            JobRecall jobRecall = jobRecallService.create(createJobRecallForm);

            createJobRecallForm.setJobRecall(jobRecall);
        } catch (Exception e) {
            createJobRecallForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createJobRecallForm);
        }
        return ResponseEntity.ok(createJobRecallForm);
    }
}
