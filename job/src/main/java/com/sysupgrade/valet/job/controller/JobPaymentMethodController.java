package com.sysupgrade.valet.job.controller;

import com.sysupgrade.valet.job.model.jobpaymentmethod.ListJobPaymentMethodForm;
import com.sysupgrade.valet.job.orm.JobPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sysupgrade.valet.job.service.JobPaymentMethodService;

import java.util.Optional;


@RestController
@RequestMapping("/api/job-payment-method/")
public class JobPaymentMethodController {

    @Autowired
    private JobPaymentMethodService jobPaymentMethodService;



    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("")
    public ResponseEntity<ListJobPaymentMethodForm> listJobPaymentMethod(ListJobPaymentMethodForm listJobPaymentMethodForm, Pageable pageable) {
        try {
            Page<JobPaymentMethod> payment = jobPaymentMethodService.listJobPaymentMethod(pageable);

            listJobPaymentMethodForm.setJobPaymentMethods(payment);
        } catch (Exception e) {
            listJobPaymentMethodForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobPaymentMethodForm);
        }
        return ResponseEntity.ok(listJobPaymentMethodForm);
    }

//
//    @PreAuthorize("hasAnyAuthority('ADMIN')")
//    @GetMapping("list")
//    public ResponseEntity<ListJobPaymentMethodForm> listJobPaymentMethod(ListJobPaymentMethodForm listJobPaymentMethodForm) {
//        try {
//            Optional<JobPaymentMethod> payment = jobPaymentMethodService.listJobPaymentMethod2();
//
//            listJobPaymentMethodForm.setJobPaymentMethods2(payment);
//        } catch (Exception e) {
//            listJobPaymentMethodForm.setError(e.getMessage());
//            return ResponseEntity.status(500).body(listJobPaymentMethodForm);
//        }
//        return ResponseEntity.ok(listJobPaymentMethodForm);
//    }



}
