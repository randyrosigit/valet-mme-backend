package com.sysupgrade.valet.job.controller;

import com.sysupgrade.valet.job.model.jobpickup.CreatePickupForm;
import com.sysupgrade.valet.job.model.jobpickup.GetPickupForm;
import com.sysupgrade.valet.job.model.jobpickup.UpdatePickupForm;
import com.sysupgrade.valet.job.orm.JobPickup;
import com.sysupgrade.valet.job.service.JobPickupService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/job/pickup/")

public class JobPickupController {
    @Autowired
    private JobPickupService jobPickupService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;

    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @GetMapping("{id}")
    public ResponseEntity<GetPickupForm> getJobPickup(@PathVariable String id, GetPickupForm getPickupForm) {
        try {
            Optional<JobPickup> jobPickup = jobPickupService.getJobPickup(id);
            if (!jobPickup.isPresent()) {
                throw new Exception(ErrorHelper.DATA_NOT_FOUND);
            }

            getPickupForm.setJobPickup(jobPickup.get());
        } catch (Exception e) {
            getPickupForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(getPickupForm);
        }
        return ResponseEntity.ok(getPickupForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("create")
    public ResponseEntity<CreatePickupForm> create(CreatePickupForm createPickupForm) {
        try {
            JobPickup jobPickup = jobPickupService.create(createPickupForm.getJobId(), createPickupForm);

            createPickupForm.setJobPickup(jobPickup);
        } catch (Exception e) {
            createPickupForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createPickupForm);
        }
        return ResponseEntity.ok(createPickupForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("update")
    public ResponseEntity<UpdatePickupForm> update(UpdatePickupForm updatePickupForm) {
        try {
            JobPickup jobPickup = jobPickupService.update(updatePickupForm);

            updatePickupForm.setJobPickup(jobPickup);
        } catch (Exception e) {
            updatePickupForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updatePickupForm);
        }
        return ResponseEntity.ok(updatePickupForm);
    }
}
