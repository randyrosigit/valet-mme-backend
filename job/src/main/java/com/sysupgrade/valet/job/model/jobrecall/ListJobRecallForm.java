package com.sysupgrade.valet.job.model.jobrecall;

import com.sysupgrade.valet.job.orm.JobRecall;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ListJobRecallForm {

    private String error;

    private Page<JobRecall> jobRecalls;
}
