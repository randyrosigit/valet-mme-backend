package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.orm.Job;
import com.sysupgrade.valet.job.orm.JobExtra;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ExtraJobForm {
    @NotNull
    private String jobId;

    private Integer totalLocation;
    private Integer waitingTime;
    private BigDecimal price;

    private String error;

    private JobExtra job;
}
