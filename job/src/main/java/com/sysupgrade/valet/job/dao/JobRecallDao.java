package com.sysupgrade.valet.job.dao;

import com.sysupgrade.valet.job.orm.JobRecall;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JobRecallDao extends PagingAndSortingRepository<JobRecall, String> {
}
