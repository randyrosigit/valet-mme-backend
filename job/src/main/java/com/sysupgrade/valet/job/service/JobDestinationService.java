package com.sysupgrade.valet.job.service;

import com.sysupgrade.valet.job.dao.JobDestinationDao;
import com.sysupgrade.valet.job.model.jobdestination.CreateDestinationForm;
import com.sysupgrade.valet.job.model.jobdestination.GetDestinationForm;
import com.sysupgrade.valet.job.orm.JobDestination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class JobDestinationService {
    @Autowired
    private JobDestinationDao jobDestinationDao;

    public Page<JobDestination> getJobDestination(String id, Pageable pageable) {
        return this.jobDestinationDao.findByJobId(id, pageable);
    }

    @Transactional
    public JobDestination create(String id, CreateDestinationForm createDestinationForm) {
        JobDestination jobDestination = new JobDestination();
        jobDestination.setJobId(id);
        jobDestination.setLongitude(createDestinationForm.getLongitude());
        jobDestination.setLatitude(createDestinationForm.getLatitude());
        jobDestination.setAddress(createDestinationForm.getAddress());
        jobDestination.setSequence(createDestinationForm.getSequence());
        this.jobDestinationDao.save(jobDestination);

        return jobDestination;
    }
}
