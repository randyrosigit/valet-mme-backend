package com.sysupgrade.valet.job.dao;

import com.sysupgrade.valet.job.orm.JobDestination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface JobDestinationDao extends PagingAndSortingRepository<JobDestination, String> {
    @Query("SELECT c from JobDestination c WHERE c.jobId = :jobId ORDER BY c.sequence ASC")
    List<JobDestination> findByJobId(@Param("jobId") String jobId);

    @Query("SELECT c from JobDestination c WHERE c.jobId = :jobId ORDER BY c.sequence ASC")
    Page<JobDestination> findByJobId(@Param("jobId") String jobId, Pageable pageable);
}
