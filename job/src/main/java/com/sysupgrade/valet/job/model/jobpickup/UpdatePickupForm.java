package com.sysupgrade.valet.job.model.jobpickup;

import com.sysupgrade.valet.job.orm.JobPickup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class UpdatePickupForm {
    @NotNull
    private String id;

    private String jobId;

    private String vehicleId;

    private String latitude;

    private String longitude;

    private String note;

    private String address;

    private Date pickupTime;

    private String friendPhoneNumber;

    private String error;

    private JobPickup jobPickup;
}
