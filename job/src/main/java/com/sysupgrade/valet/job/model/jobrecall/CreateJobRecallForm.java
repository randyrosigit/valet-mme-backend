package com.sysupgrade.valet.job.model.jobrecall;

import com.sysupgrade.valet.job.orm.JobRecall;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateJobRecallForm {
    @NotNull
    private String jobId;

    @NotNull
    private String latitude;

    @NotNull
    private String longitude;

    @NotNull
    private String address;

    private String error;

    private JobRecall jobRecall;
}
