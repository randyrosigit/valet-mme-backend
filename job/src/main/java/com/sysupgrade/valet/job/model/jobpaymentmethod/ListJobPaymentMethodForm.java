package com.sysupgrade.valet.job.model.jobpaymentmethod;

import com.sysupgrade.valet.job.orm.JobPaymentMethod;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.Optional;

@Data
public class ListJobPaymentMethodForm {

    private String error;

    private Page<JobPaymentMethod> jobPaymentMethods;
    private Optional<JobPaymentMethod> jobPaymentMethods2;
}
