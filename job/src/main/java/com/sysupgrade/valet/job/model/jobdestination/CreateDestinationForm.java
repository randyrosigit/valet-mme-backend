package com.sysupgrade.valet.job.model.jobdestination;

import com.sysupgrade.valet.job.orm.JobDestination;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateDestinationForm {
    @NotNull
    private String jobId;


    private String latitude;


    private String longitude;

    @NotNull
    private String address;

    private int sequence;

    private String error;

    private JobDestination jobDestination;
}
