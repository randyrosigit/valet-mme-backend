package com.sysupgrade.valet.job.controller;

import com.sysupgrade.valet.job.model.jobdestination.CreateDestinationForm;
import com.sysupgrade.valet.job.model.jobdestination.GetDestinationForm;
import com.sysupgrade.valet.job.orm.JobDestination;
import com.sysupgrade.valet.job.service.JobDestinationService;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/job/destination/")

public class JobDestinationController {
    @Autowired
    private JobDestinationService jobDestinationService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("{id}")
    public ResponseEntity<GetDestinationForm> getJobDestination(@PathVariable String id, GetDestinationForm getDestinationForm, Pageable pageable) {
        try {
            Page<JobDestination> jobDestinations = jobDestinationService.getJobDestination(id, pageable);

            getDestinationForm.setJobDestinations(jobDestinations);
        } catch (Exception e) {
            getDestinationForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(getDestinationForm);
        }
        return ResponseEntity.ok(getDestinationForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("create")
    public ResponseEntity<CreateDestinationForm> create(CreateDestinationForm createDestinationForm) {
        try {
            JobDestination jobDestination = jobDestinationService.create(createDestinationForm.getJobId(), createDestinationForm);

            createDestinationForm.setJobDestination(jobDestination);
        } catch (Exception e) {
            createDestinationForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createDestinationForm);
        }
        return ResponseEntity.ok(createDestinationForm);
    }
}
