package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.orm.Job;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FinishJobForm {
    @NotNull
    private String id;

    private String error;

    private Job job;
}
