package com.sysupgrade.valet.job.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "job_pick_up")
@Data
public class JobPickup {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "job_id")
    private String jobId;

    @Column(name = "vehicle_id")
    private String vehicleId;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "note")
    private String note;

    @Column(name = "address")
    private String address;

    @Column(name = "pickup_time")
    private Date pickupTime;

    @Column(name = "friend_phone_number")
    private String friendPhoneNumber;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;
}
