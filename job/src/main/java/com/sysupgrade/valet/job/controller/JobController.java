package com.sysupgrade.valet.job.controller;

import com.sysupgrade.valet.coupon.orm.Coupon;
import com.sysupgrade.valet.coupon.service.CouponService;
import com.sysupgrade.valet.job.model.job.*;
import com.sysupgrade.valet.job.model.jobpickup.GetPickupForm;
import com.sysupgrade.valet.job.orm.Job;
import com.sysupgrade.valet.job.orm.JobDestination;
import com.sysupgrade.valet.job.orm.JobExtra;
import com.sysupgrade.valet.job.orm.JobPickup;
import com.sysupgrade.valet.job.service.JobDestinationService;
import com.sysupgrade.valet.job.service.JobPickupService;
import com.sysupgrade.valet.job.service.JobService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(exposedHeaders = "Access-Control-Allow-Origin")
@RestController
@RequestMapping("/api/job/")

public class JobController {
    @Autowired
    private JobService jobService;

    @Autowired
    private JobPickupService jobPickupService;

    @Autowired
    private JobDestinationService jobDestinationService;

    @Autowired
    private CouponService couponService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ListJobForm> listJobs(ListJobForm listJobForm, Pageable pageable) {
        try {
            Page<Job> jobs = jobService.listJob(pageable, listJobForm);

            listJobForm.setJobs(jobs);
        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("list-by-user/{userId}")
    public ResponseEntity<ListJobForm> listJobs(ListJobForm listJobForm, @PathVariable String userId, Pageable pageable) {
        try {
            Page<Job> jobs = jobService.listJobByUser(listJobForm, userId, pageable);

            listJobForm.setJobs(jobs);
        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }


    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("list-my-booking")
    public ResponseEntity<ListJobForm> listJobs2(ListJobForm listJobForm, Pageable pageable) {
        try {
            Page<Job> jobs = jobService.listJobByUser2(iAuthenticationFacade.getMyFace().getId(), pageable);
            for (Job job : jobs) {
                Optional<JobPickup> pickup = jobService.getPickupByJobId(job.getId());
                List<JobDestination> destinations = jobService.getDestinationByJobId(job.getId());
                job.setJobPickup(pickup);
                job.setJobDestinations(destinations);
            }

            listJobForm.setJobs(jobs);
        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }


    //    jobs availabel
    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @GetMapping("job-available")
    public ResponseEntity<ListJobForm> jobAvailable(ListJobForm listJobForm, Pageable pageable) {
        try {
            Page<Job> jobs = jobService.jobAvailable(pageable);
            for (Job job : jobs) {
                Optional<JobPickup> pickup = jobService.getPickupByJobId(job.getId());
                List<JobDestination> destinations = jobService.getDestinationByJobId(job.getId());
                job.setJobPickup(pickup);
                job.setJobDestinations(destinations);
            }

            listJobForm.setJobs(jobs);
        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }

    //    for histories driver  by ID
    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @GetMapping("my-history-jobs")
    public ResponseEntity<ListJobForm> myHistoryJobs(ListJobForm listJobForm, Pageable pageable) {
        try {
            String driverId = iAuthenticationFacade.getMyFace().getId();
            Page<Job> jobs = jobService.findHistoryMyJobs(driverId, pageable);

            for (Job job : jobs) {
                Optional<JobPickup> pickup = jobService.getPickupByJobId(job.getId());
                List<JobDestination> destinations = jobService.getDestinationByJobId(job.getId());
                job.setJobPickup(pickup);
                job.setJobDestinations(destinations);
            }

            listJobForm.setJobs(jobs);
        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }

    //   get only one job is assign to me (still active)
    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @GetMapping("get-job-assign-to-me")
    public ResponseEntity<ListJobForm> jobAssignToMe(ListJobForm listJobForm, Pageable pageable) {
        try {
            String driverId = iAuthenticationFacade.getMyFace().getId();
            Page<Job> jobs = jobService.getJobAssignToMe(driverId, pageable);

            for (Job job : jobs) {
                Optional<JobPickup> pickup = jobService.getPickupByJobId(job.getId());
                List<JobDestination> destinations = jobService.getDestinationByJobId(job.getId());
                job.setJobPickup(pickup);
                job.setJobDestinations(destinations);
            }

            listJobForm.setJobs(jobs);

        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }


    //   job where i accept (STATUS_DRIVER_START_THE_JOB = 14)
    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @PostMapping("job-accepted/{jobId}")
    public ResponseEntity<ListJobForm> jobAccepted(@PathVariable String jobId, ListJobForm listJobForm, Pageable pageable) {
        try {
            String driverId = iAuthenticationFacade.getMyFace().getId();
            int jobs = jobService.getJobAccepted(jobId, driverId);

//            listJobForm.setJobs(jobs);
        } catch (Exception e) {
            listJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listJobForm);
        }
        return ResponseEntity.ok(listJobForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("get-detail-pickup/{id}")
    public ResponseEntity<GetPickupForm> getJobPickup(@PathVariable String id, GetPickupForm getPickupForm) {
        try {
            Optional<JobPickup> jobPickup = jobPickupService.getJobPickup(id);
            if (!jobPickup.isPresent()) {
                throw new Exception(ErrorHelper.DATA_NOT_FOUND);
            }

            getPickupForm.setJobPickup(jobPickup.get());
        } catch (Exception e) {
            getPickupForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(getPickupForm);
        }
        return ResponseEntity.ok(getPickupForm);
    }

    // driver finished the job
    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @PostMapping("finish")
    public ResponseEntity<FinishJobForm> finishJob(@RequestBody @Valid FinishJobForm finishJobForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            finishJobForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(finishJobForm);
        }

        try {
            Job job = jobService.finishJob(finishJobForm);
            finishJobForm.setJob(job);
        } catch (Exception e) {
            finishJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(finishJobForm);
        }
        return ResponseEntity.ok(finishJobForm);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'DRIVER')")
    @GetMapping("{id}")
    public ResponseEntity<ViewJobForm> viewJob(ViewJobForm viewJobForm, @PathVariable String id) {
        try {
            Optional<Job> job = jobService.viewJob(id);
            if (!job.isPresent()) {
                throw new Exception(ErrorHelper.DATA_NOT_FOUND);
            }

            Optional<JobPickup> pickup = jobService.getPickupByJobId(job.get().getId());
            List<JobDestination> destinations = jobService.getDestinationByJobId(job.get().getId());
            job.get().setJobPickup(pickup);
            job.get().setJobDestinations(destinations);
            viewJobForm.setJob(job);
        } catch (Exception e) {
            viewJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(viewJobForm);
        }
        return ResponseEntity.ok(viewJobForm);
    }

    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @GetMapping("job-available/{id}")
    public ResponseEntity<ViewJobForm> viewJobAvailable(ViewJobForm viewJobForm, @PathVariable String id) {
        try {
            Optional<Job> job = jobService.viewJob(id);
            if (!job.isPresent()) {
                throw new Exception(ErrorHelper.DATA_NOT_FOUND);
            }

            Optional<JobPickup> pickup = jobService.getPickupByJobId(job.get().getId());
            List<JobDestination> destinations = jobService.getDestinationByJobId(job.get().getId());
            job.get().setJobPickup(pickup);
            job.get().setJobDestinations(destinations);
            viewJobForm.setJob(job);
        } catch (Exception e) {
            viewJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(viewJobForm);
        }
        return ResponseEntity.ok(viewJobForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("report")
    public ResponseEntity<JobReportForm> generateJobReport(JobReportForm jobReportForm) {
        try {
            List<Job> jobs = jobService.jobReport(jobReportForm);

            jobReportForm.setJobs(jobs);
        } catch (Exception e) {
            jobReportForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(jobReportForm);
        }
        return ResponseEntity.ok(jobReportForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("create")
    public ResponseEntity<CreateJobForm> create(@RequestBody @Valid CreateJobForm createJobForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createJobForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createJobForm);
        }

        try {
            createJobForm.setUserId(iAuthenticationFacade.getMyFace().getId());
            Job job = jobService.create(createJobForm);

            createJobForm.setJob(job);
        } catch (Exception e) {
            createJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createJobForm);
        }
        return ResponseEntity.ok(createJobForm);
    }


    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("apply_coupon")
    public ResponseEntity<CreateJobForm> applyCoupon(@RequestBody CreateJobForm createJobForm) throws ParseException {
        Coupon coupon = couponService.findById(createJobForm.getCouponId()).orElse(null);
        if (null == coupon) {
            createJobForm.setError("Coupon doesn't exist");
            return ResponseEntity.ok(createJobForm);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
        Date couponExpiryDate = sdf.parse(coupon.getExpiredDate());
        Date dateNow = sdf.parse(sdf.format(new Date()));

        if (couponExpiryDate.compareTo(dateNow) < 0) {
            createJobForm.setError("Coupon already Expire");
            return ResponseEntity.ok(createJobForm);
        }
        createJobForm.setDiscount(coupon.getAmount());
        Job job = createJobForm.getJob();
        if (createJobForm.getJob().getPrice().compareTo(coupon.getAmount()) <= 0) {
            job.setPrice(BigDecimal.ZERO);
        } else {
            job.setPrice(createJobForm.getJob().getPrice().subtract(coupon.getAmount()));
        }
        return ResponseEntity.ok(createJobForm);
    }

    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @PostMapping("job-extra")
    public ResponseEntity<ExtraJobForm> create(@RequestBody @Valid ExtraJobForm extraJobForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            extraJobForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(extraJobForm);
        }

        try {
//            extraJobForm.setUserId(iAuthenticationFacade.getMyFace().getId());
            JobExtra jobExtra = jobService.createExtraJob(extraJobForm);

            extraJobForm.setJob(jobExtra);
        } catch (Exception e) {
            extraJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(extraJobForm);
        }
        return ResponseEntity.ok(extraJobForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("update")
    public ResponseEntity<UpdateJobForm> updateJob(@RequestBody @Valid UpdateJobForm updateJobForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            updateJobForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(updateJobForm);
        }

        try {
            Job job = jobService.updateJob(updateJobForm);
            updateJobForm.setJob(job);
        } catch (Exception e) {
            updateJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updateJobForm);
        }
        return ResponseEntity.ok(updateJobForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("assign-driver")
    public ResponseEntity<AssignDriverForm> assignDriver(@RequestBody @Valid AssignDriverForm assignDriverForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            assignDriverForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(assignDriverForm);
        }

        try {
            Job job = jobService.assignDriver(assignDriverForm);
            assignDriverForm.setJob(job);
        } catch (Exception e) {
            assignDriverForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(assignDriverForm);
        }
        return ResponseEntity.ok(assignDriverForm);
    }

    //accept jobs from available job
    @PreAuthorize("hasAnyAuthority('DRIVER')")
    @PostMapping("accept")
    public ResponseEntity<AssignDriverForm> acceptJobFromAvailableJob(@RequestBody @Valid AssignDriverForm assignDriverForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            assignDriverForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(assignDriverForm);
        }

        try {
            assignDriverForm.setDriverId(iAuthenticationFacade.getMyFace().getId());
            Job job = jobService.assignDriver(assignDriverForm);
            assignDriverForm.setJob(job);
        } catch (Exception e) {
            assignDriverForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(assignDriverForm);
        }
        return ResponseEntity.ok(assignDriverForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("delete")
    public ResponseEntity<DeleteJobForm> deleteJob(@RequestBody @Valid DeleteJobForm deleteJobForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            deleteJobForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(deleteJobForm);
        }

        try {
            Job job = jobService.deleteJob(deleteJobForm);
            deleteJobForm.setJob(job);
        } catch (Exception e) {
            deleteJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(deleteJobForm);
        }
        return ResponseEntity.ok(deleteJobForm);
    }


    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("cancel")
    public ResponseEntity<CancelJobForm> cancelJob(@RequestBody @Valid CancelJobForm cancelJobForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            cancelJobForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(cancelJobForm);
        }

        try {
            Job job = jobService.cancelJob(cancelJobForm);
            cancelJobForm.setJob(job);
        } catch (Exception e) {
            cancelJobForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(cancelJobForm);
        }
        return ResponseEntity.ok(cancelJobForm);
    }


}
