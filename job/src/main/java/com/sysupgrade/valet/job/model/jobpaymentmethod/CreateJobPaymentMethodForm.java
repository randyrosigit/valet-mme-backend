package com.sysupgrade.valet.job.model.jobpaymentmethod;

import com.sysupgrade.valet.job.orm.JobPaymentMethod;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateJobPaymentMethodForm {
    @NotNull
    private String currencyId;

    @NotNull
    private String name;

    private JobPaymentMethod jobPaymentMethod;
}
