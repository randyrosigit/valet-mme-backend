package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.orm.Job;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class JobReportForm {
    private Date start;

    private Date finish;

    private String error;

    List<Job> jobs;
}
