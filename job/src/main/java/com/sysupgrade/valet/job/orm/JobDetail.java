package com.sysupgrade.valet.job.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "job")
@Data
public class JobDetail {
    public static final int STATUS_DELETED = 0;

    public static final int STATUS_ACTIVE = 10;

    public static final int STATUS_ON_PROGRESS = 11;

    public static final int STATUS_FINISHED = 12;

    public static final int STATUS_CANCELED = 13;

    public static final int STATUS_DRIVER_START_THE_JOB = 14;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "driver_id")
    private String driverId;

    @Column(name = "payment_method_id")
    private String paymentMethodId;

    @Column(name = "coupon_id")
    private String couponId;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "distance")
    private BigDecimal distance;

    @Column(name = "status")
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = true)
    private Timestamp updatedAt;



    // pickup
    @Column(name = "vehicle_id")
    private String vehicleId;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "note")
    private String note;

    @Column(name = "address")
    private String address;

    @Column(name = "pickup_time")
    private Date pickupTime;

    @Column(name = "friend_phone_number")
    private String friendPhoneNumber;



}
