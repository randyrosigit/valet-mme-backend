package com.sysupgrade.valet.job.dao;

import com.sysupgrade.valet.job.orm.Job;
import com.sysupgrade.valet.job.orm.JobDetail;
import com.sysupgrade.valet.job.orm.JobPickup;
import com.sysupgrade.valet.job.orm.JobDestination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface JobDao extends PagingAndSortingRepository<Job, String> {

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM job j JOIN user_account ua ON j.user_id = ua.id " +
            "WHERE  (ua.first_name like %:search% OR ua.last_name like %:search%) AND j.status = :status")
    Page<Job> findAll(Pageable pageable, @Param("search") String search, @Param("status") int status);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM job j JOIN user_account ua ON j.user_id = ua.id " +
            "WHERE  (ua.first_name like %:search% OR ua.last_name like %:search%) AND j.status != 0")
    Page<Job> findAll(Pageable pageable, @Param("search") String search);



    @Query("SELECT j from Job j WHERE j.createdAt BETWEEN :start AND :finish AND j.status >= 12")
    List<Job> jobReport(@Param("start") Date start, @Param("finish") Date finish);




    @Query("SELECT j from Job j WHERE j.userId = :userId AND j.status = :status")
    Page<Job> findAllByUserId(@Param("status") int status, @Param("userId") String userId, Pageable pageable);


    @Query("SELECT j from Job j WHERE j.userId = :userId")
    Page<Job> findAllByUserId2(@Param("userId") String userId, Pageable pageable);

    @Query("SELECT j from Job j WHERE j.driverId = :driverId")
    Page<Job> findAllByDriverId(@Param("driverId") String driverId, Pageable pageable);

    // DRIVER

    // get all status jobs active = 10
    @Query("SELECT j from Job j WHERE j.status=10")
    Page<Job> jobAvailable(Pageable pageable);

    // get all status jobs by driverID
    @Query("SELECT j from Job j WHERE j.driverId = :driverId")
    Page<Job> findHistoryMyJobs(@Param("driverId") String driverId, Pageable pageable);

    // status = 11 is on progress mean assign to driver
    @Query("SELECT j from Job j WHERE j.driverId = :driverId AND (j.status = 11 OR j.status = 14) ")
//    @Query(nativeQuery = true, value ="SELECT job.* from job j WHERE job.driver_id = :driverId AND (job.status=11 OR job.status=14)")
    Page<Job> getJobAssignToMe(@Param("driverId") String driverId, Pageable pageable);

    // status = 14 driver start the job
//    @Query("SELECT j from Job j WHERE j.driverId = :driverId AND j.status=14")
    @Transactional
    @Modifying
    @Query("UPDATE Job j SET j.status=14  WHERE j.driverId = :driverId AND j.id=:jobId")
    int getJobAccepted(@Param("jobId") String jobId, @Param("driverId") String driverId);


    // status = 14 driver start the job
    @Query(nativeQuery = true, value ="SELECT * FROM job_pick_up WHERE job_id=:jobId")
    Page<JobPickup> getPickupByJobId(@Param("jobId") String jobId, Pageable pageable);



    @Query(nativeQuery = true, value ="SELECT * FROM job_destination WHERE job_id=:jobId")
    Page<JobDestination> getDestinationByJobId(@Param("jobId") String jobId, Pageable pageable);

}
