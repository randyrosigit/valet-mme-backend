package com.sysupgrade.valet.job;

import com.sysupgrade.valet.job.model.jobpaymentmethod.CreateJobPaymentMethodForm;
import com.sysupgrade.valet.job.orm.JobPaymentMethod;
import com.sysupgrade.valet.job.service.JobPaymentMethodService;
import com.sysupgrade.valet.library.config.config.CurrencyConfig;
import com.sysupgrade.valet.wallet.config.PaymentConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InitJobPaymentOnStartup implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(InitJobPaymentOnStartup.class);

    @Autowired
    private JobPaymentMethodService jobPaymentMethodService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        this.initCash();
        this.initCreditCard();
        this.initWalletCredit();
    }

    private void initCash() {
        Optional<JobPaymentMethod> jobPaymentMethodOptional = jobPaymentMethodService.findByNameAndCurrencyId(PaymentConfig.CASH, CurrencyConfig.SGD_CURRENCY_ID);

        if (jobPaymentMethodOptional.isPresent()) {
            return;
        }

        CreateJobPaymentMethodForm createJobPaymentMethodForm = new CreateJobPaymentMethodForm();
        createJobPaymentMethodForm.setCurrencyId(CurrencyConfig.SGD_CURRENCY_ID);
        createJobPaymentMethodForm.setName(PaymentConfig.CASH);

        jobPaymentMethodService.create(createJobPaymentMethodForm);
    }

    private void initCreditCard() {
        Optional<JobPaymentMethod> jobPaymentMethodOptional = jobPaymentMethodService.findByNameAndCurrencyId(PaymentConfig.CREDIT_CARD, CurrencyConfig.SGD_CURRENCY_ID);

        if (jobPaymentMethodOptional.isPresent()) {
            return;
        }

        CreateJobPaymentMethodForm createJobPaymentMethodForm = new CreateJobPaymentMethodForm();
        createJobPaymentMethodForm.setCurrencyId(CurrencyConfig.SGD_CURRENCY_ID);
        createJobPaymentMethodForm.setName(PaymentConfig.CREDIT_CARD);

        jobPaymentMethodService.create(createJobPaymentMethodForm);
    }

    private void initWalletCredit() {
        Optional<JobPaymentMethod> jobPaymentMethodOptional = jobPaymentMethodService.findByNameAndCurrencyId(PaymentConfig.WALLET_CREDIT, CurrencyConfig.SGD_CURRENCY_ID);

        if (jobPaymentMethodOptional.isPresent()) {
            return;
        }

        CreateJobPaymentMethodForm createJobPaymentMethodForm = new CreateJobPaymentMethodForm();
        createJobPaymentMethodForm.setCurrencyId(CurrencyConfig.SGD_CURRENCY_ID);
        createJobPaymentMethodForm.setName(PaymentConfig.WALLET_CREDIT);

        jobPaymentMethodService.create(createJobPaymentMethodForm);
    }
}
