package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.model.jobdestination.CreateDestinationForm;
import com.sysupgrade.valet.job.model.jobpickup.CreatePickupForm;
import com.sysupgrade.valet.job.orm.Job;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CreateJobForm {

    private String userId;

    private String couponId;

    @NotNull
    private String paymentMethodId;

    @NotNull
    private BigDecimal distance;

    private String error;

    private BigDecimal discount;

    private Job job;

    private CreatePickupForm jobPickup;

    private CreateDestinationForm[] jobDestinations;

}
