package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.orm.Job;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class UpdateJobForm {
    @NotNull
    private String id;

    private String driverId;

    private BigDecimal price;

    private String error;

    private Job job;
}
