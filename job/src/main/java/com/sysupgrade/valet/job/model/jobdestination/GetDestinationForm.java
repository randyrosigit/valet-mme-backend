package com.sysupgrade.valet.job.model.jobdestination;

import com.sysupgrade.valet.job.orm.JobDestination;
import lombok.Data;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

@Data
public class GetDestinationForm {
    @NotNull
    private String jobId;

    private String error;

    private Page<JobDestination> jobDestinations;
}