package com.sysupgrade.valet.job.model.jobpickup;

import com.sysupgrade.valet.job.orm.JobPickup;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GetPickupForm {
    @NotNull
    private String jobId;

    private String error;

    private JobPickup jobPickup;
}
