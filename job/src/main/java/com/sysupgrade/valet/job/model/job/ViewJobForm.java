package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.orm.Job;
import lombok.Data;

import java.util.Optional;

@Data
public class ViewJobForm {
    private String id;

    private String error;

    private Optional<Job> job;
}
