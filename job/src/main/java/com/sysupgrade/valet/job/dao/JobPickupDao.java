package com.sysupgrade.valet.job.dao;

import com.sysupgrade.valet.job.orm.Job;
import com.sysupgrade.valet.job.orm.JobPickup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface JobPickupDao extends PagingAndSortingRepository<JobPickup, String> {
    @Query("SELECT jp from JobPickup jp WHERE jp.jobId = :jobId")
    Optional<JobPickup> findByJobId(@Param("jobId") String jobId);
}
