package com.sysupgrade.valet.job.service;

import com.sysupgrade.valet.job.dao.JobPickupDao;
import com.sysupgrade.valet.job.model.job.CreateJobForm;
import com.sysupgrade.valet.job.model.jobpickup.CreatePickupForm;
import com.sysupgrade.valet.job.model.jobpickup.GetPickupForm;
import com.sysupgrade.valet.job.model.jobpickup.UpdatePickupForm;
import com.sysupgrade.valet.job.orm.JobPickup;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class JobPickupService {
    @Autowired
    private JobPickupDao jobPickupDao;

    public Optional<JobPickup> getJobPickup(String id) {
        return jobPickupDao.findByJobId(id);
    }

    @Transactional
    public JobPickup create(String id, CreatePickupForm createPickupForm) {
        JobPickup jobPickup = new JobPickup();
        jobPickup.setJobId(id);
        jobPickup.setVehicleId(createPickupForm.getVehicleId());
        jobPickup.setFriendPhoneNumber(createPickupForm.getFriendPhoneNumber());
        jobPickup.setLongitude(createPickupForm.getLongitude());
        jobPickup.setLatitude(createPickupForm.getLatitude());
        jobPickup.setAddress(createPickupForm.getAddress());
        jobPickup.setNote(createPickupForm.getNote());
        jobPickup.setPickupTime(createPickupForm.getPickupTime());
        this.jobPickupDao.save(jobPickup);

        return jobPickup;
    }

    @Transactional
    public JobPickup update(UpdatePickupForm updatePickupForm) throws Exception {
        Optional<JobPickup> jobPickupOptional = jobPickupDao.findById(updatePickupForm.getId());
        if (!jobPickupOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        JobPickup jobPickup = jobPickupOptional.get();
        jobPickup.setVehicleId(updatePickupForm.getVehicleId());
        jobPickup.setFriendPhoneNumber(updatePickupForm.getFriendPhoneNumber());
        jobPickup.setLongitude(updatePickupForm.getLongitude());
        jobPickup.setLatitude(updatePickupForm.getLatitude());
        jobPickup.setAddress(updatePickupForm.getAddress());
        jobPickup.setNote(updatePickupForm.getNote());
        jobPickup.setPickupTime(updatePickupForm.getPickupTime());
        this.jobPickupDao.save(jobPickup);

        return jobPickup;
    }
}
