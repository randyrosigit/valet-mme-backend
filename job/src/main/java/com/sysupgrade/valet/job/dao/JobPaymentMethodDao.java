package com.sysupgrade.valet.job.dao;

import com.sysupgrade.valet.job.orm.JobPaymentMethod;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface JobPaymentMethodDao extends PagingAndSortingRepository<JobPaymentMethod, String> {
    Optional<JobPaymentMethod> findByIdAndCurrencyId(String paymentId, String currencyId);

    Optional<JobPaymentMethod> findByNameAndCurrencyId(String name, String currencyId);

//    @Query("SELECT * from job_payment_method")
//    Optional<JobPaymentMethod> findAll2();
}
