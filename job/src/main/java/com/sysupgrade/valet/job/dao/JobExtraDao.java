package com.sysupgrade.valet.job.dao;

import com.sysupgrade.valet.job.orm.JobExtra;
import com.sysupgrade.valet.job.orm.JobPickup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface JobExtraDao extends PagingAndSortingRepository<JobExtra, String> {

}
