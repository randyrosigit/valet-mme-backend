package com.sysupgrade.valet.job.model.job;

import com.sysupgrade.valet.job.orm.Job;
import com.sysupgrade.valet.job.orm.JobDestination;
import com.sysupgrade.valet.job.orm.JobDetail;
import com.sysupgrade.valet.job.orm.JobPickup;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.Optional;

@Data
public class ListJobForm {

    private String search;

    private String status;

    private String error;

    private Page<Job> jobs;
}
