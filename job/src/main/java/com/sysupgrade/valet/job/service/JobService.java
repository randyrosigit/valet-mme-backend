package com.sysupgrade.valet.job.service;

import com.sysupgrade.valet.job.dao.JobDao;
import com.sysupgrade.valet.job.dao.JobDestinationDao;
import com.sysupgrade.valet.job.dao.JobExtraDao;
import com.sysupgrade.valet.job.dao.JobPickupDao;
import com.sysupgrade.valet.job.model.job.*;
import com.sysupgrade.valet.job.model.jobdestination.CreateDestinationForm;
import com.sysupgrade.valet.job.orm.*;
import com.sysupgrade.valet.library.config.config.CurrencyConfig;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.wallet.orm.WalletCredit;
import com.sysupgrade.valet.wallet.service.WalletCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class JobService {
    @Autowired
    private JobDao jobDao;

    @Autowired
    private JobPickupDao jobPickupDao;

    @Autowired
    private JobDestinationDao jobDestinationDao;

    @Autowired
    private JobExtraDao jobExtraDao;

    @Autowired
    private JobPickupService jobPickupService;

    @Autowired
    private JobDestinationService jobDestinationService;

    @Autowired
    private JobPaymentMethodService jobPaymentMethodService;

    @Autowired
    private WalletCreditService walletCreditService;


    public Page<Job> listJob(Pageable pageable, ListJobForm listJobForm) {
        if (listJobForm.getSearch() == null)
            listJobForm.setSearch("");

        if (listJobForm.getStatus() == null)
            return jobDao.findAll(pageable, listJobForm.getSearch());

        return jobDao.findAll(pageable, listJobForm.getSearch(), Integer.parseInt(listJobForm.getStatus()));
    }

    public Page<Job> listJobByUser(ListJobForm listJobForm, String userId, Pageable pageable) {
        return jobDao.findAllByUserId(Integer.parseInt(listJobForm.getStatus()), userId, pageable);
    }

    public Page<Job> listJobByUser2(String userId, Pageable pageable) {
        return jobDao.findAllByUserId2(userId, pageable);
    }

    public Page<Job> listJobByDriver(String driverId, Pageable pageable) {
        return jobDao.findAllByDriverId(driverId, pageable);
    }


    // driver

    public Page<Job> findHistoryMyJobs(String driverId, Pageable pageable) {
        return jobDao.findHistoryMyJobs(driverId, pageable);
    }
    public Page<Job> jobAvailable(Pageable pageable) {
        return jobDao.jobAvailable(pageable);
    }
    public Page<Job> getJobAssignToMe(String driverId, Pageable pageable) {
        return jobDao.getJobAssignToMe(driverId, pageable);
    }

    public int getJobAccepted(String jobId, String driverId) {
        return jobDao.getJobAccepted(jobId, driverId);
    }

    public Optional<JobPickup> getPickupByJobId(String jobId) {
        return jobPickupDao.findByJobId(jobId);
    }

    public List<JobDestination> getDestinationByJobId(String jobId) {
        return jobDestinationDao.findByJobId(jobId);
    }

    public Optional<Job> viewJob(String id) {
        return jobDao.findById(id);
    }

    public List<Job> jobReport(JobReportForm jobReportForm) {
        return jobDao.jobReport(jobReportForm.getStart(), jobReportForm.getFinish());
    }

    @Transactional
    public JobExtra createExtraJob(ExtraJobForm extraJobForm)throws Exception {

        JobExtra job = new JobExtra();
        job.setJobId(extraJobForm.getJobId());
        job.setWaitingTime(extraJobForm.getWaitingTime());
        job.setTotalLocation(extraJobForm.getTotalLocation());
        job.setPrice(extraJobForm.getPrice());
        jobExtraDao.save(job);
        return job;
    }


    @Transactional
    public Job create(CreateJobForm createJobForm)throws Exception {
        Optional<JobPaymentMethod> jobPaymentMethodOptional = jobPaymentMethodService.findById(createJobForm.getPaymentMethodId());
        if (!jobPaymentMethodOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

//        CheckCouponForm checkCouponForm = new CheckCouponForm();
//        checkCouponForm.setCode(createJobForm.getCouponId());
//        Coupon coupon = couponService.checkCoupon(checkCouponForm);
//        if(coupon.isPresent() ){
//            throw new Exception("Your coupon is not recognize");
//        }

        if(jobPaymentMethodOptional.get().getName().equals("walletcredit") && !walletCreditService.checkCredit(createJobForm.getUserId(), CurrencyConfig.SGD_CURRENCY_ID, countPrice(createJobForm)))
        {
            throw new Exception("Your balance is not enough");
        }

        Job job = new Job();
        job.setUserId(createJobForm.getUserId());
        job.setCouponId(createJobForm.getCouponId());
        job.setPaymentMethodId(createJobForm.getPaymentMethodId());
        job.setPrice(countPrice(createJobForm));

        job.setDistance(createJobForm.getDistance());
        job.setStatus(Job.STATUS_ACTIVE);
        this.jobDao.save(job);


        // pickup destination only 1
        this.jobPickupService.create(job.getId(), createJobForm.getJobPickup());

        // destination can more than 1
        for (CreateDestinationForm createDestinationForm : createJobForm.getJobDestinations()) {
            this.jobDestinationService.create(job.getId(), createDestinationForm);
        }

        return job;
    }

    private BigDecimal countPrice(CreateJobForm createJobForm) {
        int countDest = createJobForm.getJobDestinations().length - 1;
        BigDecimal distancePrice = createJobForm.getDistance().multiply(new BigDecimal(1));
        BigDecimal destinationPrice = new BigDecimal(countDest).multiply(new BigDecimal(10));
        return distancePrice.add(destinationPrice);
    }

    @Transactional
    public Job updateJob(UpdateJobForm updateJobForm) throws Exception {
        Optional<Job> jobOptional = jobDao.findById(updateJobForm.getId());
        if (!jobOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Job job = jobOptional.get();
        job.setPrice(updateJobForm.getPrice());
        job.setDriverId(updateJobForm.getDriverId());
        this.jobDao.save(job);

        return job;
    }

    @Transactional
    public Job deleteJob(DeleteJobForm deleteJobForm) throws Exception {
        Optional<Job> jobOptional = jobDao.findById(deleteJobForm.getId());
        if (!jobOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Job job = jobOptional.get();
        job.setStatus(Job.STATUS_DELETED);
        this.jobDao.save(job);

        return job;
    }

    @Transactional
    public Job assignDriver(AssignDriverForm assignDriverForm) throws Exception {
        Optional<Job> jobOptional = jobDao.findById(assignDriverForm.getId());
        if (!jobOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        if(!walletCreditService.checkCredit(assignDriverForm.getDriverId(), CurrencyConfig.SGD_CURRENCY_ID, new BigDecimal((50)))) {
            throw new Exception("Driver balance is under SGD 50");
        }
        Job job = jobOptional.get();
        job.setDriverId(assignDriverForm.getDriverId());
        job.setStatus(Job.STATUS_ON_PROGRESS);
        this.jobDao.save(job);

        return job;
    }

    @Transactional
    public Job cancelJob(CancelJobForm cancelJobForm) throws Exception {
        Optional<Job> jobOptional = jobDao.findById(cancelJobForm.getId());
        if (!jobOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }
        if(jobOptional.get().getStatus() != Job.STATUS_ACTIVE || jobOptional.get().getStatus() != Job.STATUS_ON_PROGRESS){
            throw new Exception("Job is not Active or On Progress");
        }

        Job job = jobOptional.get();
        job.setStatus(Job.STATUS_CANCELED);
        this.jobDao.save(job);

        return job;
    }


    @Transactional
    public Job finishJob(FinishJobForm finishJobForm) throws Exception {
        Optional<Job> jobOptional = jobDao.findById(finishJobForm.getId());
        if (!jobOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }
        if(jobOptional.get().getStatus() != Job.STATUS_DRIVER_START_THE_JOB || jobOptional.get().getStatus() != Job.STATUS_ON_PROGRESS)
        {
            throw new Exception("Job is not yet Started or On Progress");
        }

        Optional<JobPaymentMethod> jobPaymentMethodOptional = jobPaymentMethodService.findById(jobOptional.get().getPaymentMethodId());
        if (!jobPaymentMethodOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Job job = jobOptional.get();
        job.setStatus(Job.STATUS_FINISHED);
        this.jobDao.save(job);


        //if customer pay using wallet credit = deduct saldo from customer wallet and the move to driver saldo
        if(jobPaymentMethodOptional.get().getName().equals("walletcredit")) {
            walletCreditService.adjustment(jobOptional.get().getUserId(), jobPaymentMethodOptional.get().getCurrencyId(), jobOptional.get().getPrice().negate(), WalletCredit.JOB_ORDER);
            walletCreditService.adjustment(jobOptional.get().getDriverId(), jobPaymentMethodOptional.get().getCurrencyId(), jobOptional.get().getPrice(), WalletCredit.JOB_ORDER);
        }

        // Deduct Wallet Credit for company
        //if customer pay using cash then deduct saldo from driver wallet
        if(jobPaymentMethodOptional.get().getName().equals("cash")) {
            walletCreditService.adjustment(jobOptional.get().getDriverId(), jobPaymentMethodOptional.get().getCurrencyId(), jobOptional.get().getPrice().negate(), WalletCredit.CHARGE);
        }
        return job;

    }
}
