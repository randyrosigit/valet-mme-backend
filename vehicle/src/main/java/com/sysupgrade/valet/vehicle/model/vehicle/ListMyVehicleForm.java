package com.sysupgrade.valet.vehicle.model.vehicle;

import com.sysupgrade.valet.vehicle.orm.Vehicle;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ListMyVehicleForm {


    private String userId;
    private String error;

    private Page<Vehicle> vehicles;
}
