package com.sysupgrade.valet.vehicle.model.vehicle;

import lombok.Data;

import javax.validation.constraints.NotNull;
import com.sysupgrade.valet.vehicle.orm.Vehicle;


@Data
public class CreateVehicleForm {

    @NotNull
    private String plateNumber;

    @NotNull
    private String brand;

    @NotNull
    private String color;

    private Vehicle vehicle;

    private String error;

    private String plateImageURL;
}
