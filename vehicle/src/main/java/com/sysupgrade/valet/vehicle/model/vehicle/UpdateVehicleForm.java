package com.sysupgrade.valet.vehicle.model.vehicle;

import com.sysupgrade.valet.vehicle.orm.Vehicle;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class UpdateVehicleForm {

    private String id;


    @NotNull
    private String userId;


    @NotNull
    private String plateNumber;

    @NotNull
    private String brand;

    @NotNull
    private String color;



    private Vehicle vehicle;

    private String error;
}
