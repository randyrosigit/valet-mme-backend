package com.sysupgrade.valet.vehicle.controller;



import com.sysupgrade.valet.vehicle.model.vehicle.*;
import com.sysupgrade.valet.vehicle.orm.Vehicle;
import com.sysupgrade.valet.vehicle.service.VehicleService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/vehicle/")
public class VehicleController {


    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("get-all")
    public ResponseEntity<ListVehicleForm> listVehicles(ListVehicleForm listVehicleForm, Pageable pageable) {
        try {
            Page<Vehicle> vehicle = vehicleService.listVehicle(pageable);

            listVehicleForm.setVehicles(vehicle);
        } catch (Exception e) {
            listVehicleForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listVehicleForm);
        }
        return ResponseEntity.ok(listVehicleForm);
    }


    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("my-vehicle")
    public ResponseEntity<ListMyVehicleForm> listVehicles(ListMyVehicleForm listMyVehicleForm, Pageable pageable) {
        try {
            String userId = iAuthenticationFacade.getMyFace().getId();
            Page<Vehicle> vehicle = vehicleService.listMyVehicle(userId, pageable);

            listMyVehicleForm.setVehicles(vehicle);
        } catch (Exception e) {
            listMyVehicleForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(listMyVehicleForm);
        }
        return ResponseEntity.ok(listMyVehicleForm);
    }



    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("create")
    public ResponseEntity<CreateVehicleForm> createVehicle(@RequestBody @Valid CreateVehicleForm createVehicleForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            createVehicleForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(createVehicleForm);
        }

        try {
            String userId = iAuthenticationFacade.getMyFace().getId();
            Vehicle vehicle = vehicleService.create(userId, createVehicleForm);

            createVehicleForm.setVehicle(vehicle);
        } catch (Exception e) {
            createVehicleForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(createVehicleForm);
        }
        return ResponseEntity.ok(createVehicleForm);
    }



    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("update")
    public ResponseEntity<UpdateVehicleForm> updateVehicle(@RequestBody @Valid UpdateVehicleForm updateVehicleForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            updateVehicleForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(updateVehicleForm);
        }

        try {
            Vehicle vehicle = vehicleService.updateVehicle(updateVehicleForm);
            updateVehicleForm.setVehicle(vehicle);
        } catch (Exception e) {
            updateVehicleForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(updateVehicleForm);
        }
        return ResponseEntity.ok(updateVehicleForm);
    }



    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("delete")
    public ResponseEntity<DeleteVehicleForm> deleteVehicle(@RequestBody @Valid DeleteVehicleForm deleteVehicleForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            deleteVehicleForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(deleteVehicleForm);
        }

        try {
            Vehicle vehicle = vehicleService.deleteVehicle(deleteVehicleForm);
            deleteVehicleForm.setVehicle(vehicle);
        } catch (Exception e) {
            deleteVehicleForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(deleteVehicleForm);
        }
        return ResponseEntity.ok(deleteVehicleForm);
    }




}
