package com.sysupgrade.valet.vehicle.model.vehicle;

import com.sysupgrade.valet.vehicle.orm.Vehicle;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class DeleteVehicleForm {

    @NotNull
    private String id;

    private Vehicle vehicle;

    private String error;
}
