package com.sysupgrade.valet.vehicle.dao;

import com.sysupgrade.valet.vehicle.orm.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface VehicleDao extends PagingAndSortingRepository<Vehicle, String> {


    @Query("SELECT v FROM Vehicle v where userId=:user_id")
    Page<Vehicle> listMyVehicle(@Param("user_id")String userId, Pageable pageable);
}
