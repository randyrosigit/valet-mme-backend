package com.sysupgrade.valet.vehicle.model.vehicle;

import lombok.Data;
import org.springframework.data.domain.Page;
import com.sysupgrade.valet.vehicle.orm.Vehicle;

@Data
public class ListVehicleForm {


    private String error;

    private Page<Vehicle> vehicles;
}
