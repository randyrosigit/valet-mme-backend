package com.sysupgrade.valet.vehicle.service;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.vehicle.model.vehicle.CreateVehicleForm;
import com.sysupgrade.valet.vehicle.model.vehicle.UpdateVehicleForm;
import com.sysupgrade.valet.vehicle.model.vehicle.DeleteVehicleForm;
import com.sysupgrade.valet.vehicle.orm.Vehicle;
import com.sysupgrade.valet.vehicle.dao.VehicleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;

@Service
public class VehicleService {

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;

    @Autowired
    private VehicleDao vehicleDao;


    public Page<Vehicle> listVehicle(Pageable pageable){
        return vehicleDao.findAll(pageable);
    }

    public Page<Vehicle> listMyVehicle(String userId, Pageable pageable) {

        return vehicleDao.listMyVehicle(userId, pageable);
    }


    @Transactional
    public Vehicle create(String userId, CreateVehicleForm createVehicleForm){
        Vehicle vehicle = new Vehicle();

        vehicle.setUserId(userId);
        vehicle.setBrand(createVehicleForm.getBrand());
        vehicle.setColor(createVehicleForm.getColor());
        vehicle.setPlateNumber(createVehicleForm.getPlateNumber());
        this.vehicleDao.save(vehicle);
        return vehicle;
    }

    @Transactional
    public Vehicle updateVehicle(UpdateVehicleForm updateVehicleForm) throws Exception {
        Optional<Vehicle> vehicleOptional = vehicleDao.findById(updateVehicleForm.getId());

        if (!vehicleOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Vehicle vehicle = vehicleOptional.get();

        vehicle.setBrand(updateVehicleForm.getBrand());
        vehicle.setColor(updateVehicleForm.getColor());
        vehicle.setPlateNumber(updateVehicleForm.getPlateNumber());

        this.vehicleDao.save(vehicle);

        return vehicle;
    }

    @Transactional
    public Vehicle deleteVehicle(DeleteVehicleForm deleteVehicleForm) throws Exception {
        Optional<Vehicle> vehicleOptional = vehicleDao.findById(deleteVehicleForm.getId());
        if (!vehicleOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        Vehicle vehicle = vehicleOptional.get();
        //vehicle.setStatus(Vehicle.STATUS_DELETED);
        this.vehicleDao.deleteById(deleteVehicleForm.getId());

        return vehicle;
    }



}
