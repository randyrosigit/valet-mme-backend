package com.sysupgrade.valet.chat.dao;

import com.sysupgrade.valet.chat.orm.ChatMessage;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ChatMessageDao extends PagingAndSortingRepository<ChatMessage, String> {
}
