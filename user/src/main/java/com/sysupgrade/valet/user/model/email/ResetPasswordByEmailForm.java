package com.sysupgrade.valet.user.model.email;

import lombok.Data;

@Data
public class ResetPasswordByEmailForm {

    private String email;

    private String error;
}
