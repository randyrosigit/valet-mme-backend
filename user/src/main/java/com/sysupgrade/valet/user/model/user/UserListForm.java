package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class UserListForm {

    private String search;

    private String status;

    private String error;

    private Page<User> users;
}
