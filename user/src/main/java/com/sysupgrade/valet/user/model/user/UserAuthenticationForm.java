package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

@Data
public class UserAuthenticationForm {

    private User user;

    private boolean admin;
}
