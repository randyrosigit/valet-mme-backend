package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.UserEmailAuthentication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserEmailDao extends PagingAndSortingRepository<UserEmailAuthentication, String> {

    Optional<UserEmailAuthentication> findByEmail(String email);

    List<UserEmailAuthentication> findByUserId(String userId);


    @Query(nativeQuery = true, value = "SELECT * FROM user_email_authentication where user_id=:user_id limit 1")
    Optional<UserEmailAuthentication> findByUserIdLimit(@Param("user_id") String userId);
}
