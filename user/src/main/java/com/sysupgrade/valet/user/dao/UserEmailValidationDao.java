package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.UserEmailValidation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserEmailValidationDao extends PagingAndSortingRepository<UserEmailValidation, String> {

    @Query(nativeQuery = true,
            value = "SELECT u.* from user_email_validation u " +
                    "where u.email = :email " +
                    "ORDER BY id DESC limit 1")
    Optional<UserEmailValidation> findByEmail(@Param("email") String email);

}
