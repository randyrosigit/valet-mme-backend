package com.sysupgrade.valet.user.controller;

import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import com.sysupgrade.valet.user.model.email.*;
import com.sysupgrade.valet.user.service.EmailService;
import com.sysupgrade.valet.user.service.UserService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.orm.UserEmailAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user/email/")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @CrossOrigin("*")
    @PostMapping("public/login")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<LoginByEmailForm> loginByEmail(@RequestBody @Valid LoginByEmailForm form,
                                                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = emailService.login(form);
            form.setUser(user);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        form.setPassword(null);
        form.setEmail(null);
        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("public/reset-password")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ResetPasswordByEmailForm> resetPassword(@RequestBody @Valid ResetPasswordByEmailForm form,
                                                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            emailService.forgotPassword(form.getEmail());
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
        return ResponseEntity.ok(form);
    }


    @CrossOrigin("*")
    @PostMapping("public/change-password-by-code")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ChangePasswordByCode> changePassword(@RequestBody @Valid ChangePasswordByCode form,
                                                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            String userId = emailService.getUserIdFromCode(form.getEmail(), form.getPasswordResetToken());
            userService.changePassword(userId, form.getNewPassword());
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
        return ResponseEntity.ok(form);
    }

    @GetMapping("get/{email}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GetEmailForm> get(@PathVariable String email) {

        GetEmailForm form = new GetEmailForm();
        try {
            form.setEmail(email);
            UserEmailAuthentication emailAuthentication = emailService.getEmail(form);
            form.setUserEmailAuthentication(emailAuthentication);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("public/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<RegisterByEmailForm> registerByEmail(@RequestBody @Valid RegisterByEmailForm form,
                                                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = emailService.register(form);
            form.setUser(user);
        } catch (Exception e) {
            form.setPassword(null);
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        form.setPassword(null);
        form.setEmail(null);
        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("public/driver")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<RegisterByEmailForm> driverRegisterByEmail(@RequestBody @Valid RegisterByEmailForm form,
                                                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = emailService.driverRegister(form);
            form.setUser(user);
        } catch (Exception e) {
            form.setPassword(null);
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        form.setPassword(null);
        form.setEmail(null);
        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @GetMapping("public/validate/{email}/{token}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ValidateEmailForm> validate(@PathVariable("email") String email,
                                                      @PathVariable("token") String token,
                                                      ValidateEmailForm form,
                                                      BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            emailService.validateToken(email, token);
            form.setMessage("Please go back to the app and refresh the app!");
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
        return ResponseEntity.ok(form);
    }

    @GetMapping("get")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GetEmailForm> listEmailByAdmin(@RequestParam("userId") String userId) {

        GetEmailForm form = new GetEmailForm();

        try {
            form.setUserId(userId);
            List<UserEmailAuthentication> emailAuthentications = emailService.listEmailByUserId(form.getUserId());
            form.setUserEmailAuthentications(emailAuthentications);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @GetMapping("get-email")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GetEmailForm> getEmailByUserId(@RequestParam("userId") String userId) {

        GetEmailForm form = new GetEmailForm();

        try {
            form.setUserId(userId);
            form.setEmail(emailService.getEmailByUserId(userId));
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @GetMapping("get-my-email")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GetEmailForm> getMyEmail() {
        GetEmailForm form = new GetEmailForm();
        try {

            String userId = authenticationFacade.getMyFace().getId();
            form.setUserId(userId);
            form.setEmail(emailService.getEmailByUserId(userId));
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }
}
