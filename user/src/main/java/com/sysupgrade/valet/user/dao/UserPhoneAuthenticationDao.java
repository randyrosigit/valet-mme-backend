package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.UserPhoneAuthentication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserPhoneAuthenticationDao extends PagingAndSortingRepository<UserPhoneAuthentication, String> {

    Optional<UserPhoneAuthentication> findByPhoneAndPhoneCode(String phone, String phoneCode);


    Page<UserPhoneAuthentication> findAllByUserId(String userId, Pageable pageable);
}
