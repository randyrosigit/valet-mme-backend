package com.sysupgrade.valet.user.config;

import com.sysupgrade.valet.user.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Optional;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private UserDao userDao;

    @Autowired
    public TokenAuthenticationFilter(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain filterChain) throws ServletException, IOException {

        final String accessToken = request.getHeader("Authorization");

        if (accessToken != null) {

            String myHeader[] = accessToken.split(" ");

            if (!myHeader[0].equals("Bearer")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            if (myHeader.length != 2) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }


            Optional<com.sysupgrade.valet.user.orm.User> userOptional = userDao.findByAuthKey(myHeader[1]);

            if (!userOptional.isPresent()) {
                response.sendError(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
                return;
            }

            if (!userOptional.get().isEnabled()) {
                response.sendError(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
                return;
            }

//            if (userOptional.get().getStatus() != User.STATUS_ACTIVE) {
//                response.sendError(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
//                return;
//            }

            final User user = new User(
                    userOptional.get().getUsername(),
                    userOptional.get().getPassword(),
                    userOptional.get().isEnabled(),
                    true,
                    true,
                    true,
                    userOptional.get().getAuthorities());
            final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);

            filterChain.doFilter(request, response);
            return;

        }
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }


}