package com.sysupgrade.valet.user.service;

import com.sysupgrade.valet.user.model.user.EditMyInfoForm;
import com.sysupgrade.valet.user.model.user.PublicUser;
import com.sysupgrade.valet.user.model.user.UserAuthenticationForm;
import com.sysupgrade.valet.user.model.user.UserListForm;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.dao.UserDao;
import com.sysupgrade.valet.user.dao.UserRoleDao;
import com.sysupgrade.valet.user.model.user.*;
import com.sysupgrade.valet.user.orm.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Transactional
    public Optional<User> getById(String userId) {
        return userDao.findById(userId);
    }

    @Transactional
    public Page<PublicUser> findAllByUserIds(ArrayList<String> userIds) {

        Page<User> userPage = userDao.findAllByUserIds(userIds, PageRequest.of(0, userIds.size()));

        return userPage.map((user) -> PublicUser.convert(user));
    }

    @Transactional
    public Optional<PublicUser> getByIdForPublic(String userId) {
        Optional<User> userOptional = getById(userId);
        return userOptional.isPresent() ?
                Optional.of(PublicUser.convert(userOptional.get())) :
                Optional.empty();
    }

    @Transactional
    public User changePassword(String userId, String newPassword) throws Exception {
        Optional<User> optionalUser = userDao.findById(userId);
        if(!optionalUser.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        User user = optionalUser.get();
        user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
        userDao.save(user);
        return user;
    }

    public User edit(EditMyInfoForm form) throws Exception {
        Optional<User> optionalUser = userDao.findById(form.getUserId());
        if(!optionalUser.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        User user = optionalUser.get();
        user.setFirstName(form.getFirstName());
        user.setAddress(form.getAddress());
        user.setCountry(form.getCountry());
        user.setPhone(form.getPhone());
        user.setPostalCode(form.getPostalCode());
//        user.setLastName(form.getLastName());
        user.setFrontICFileId(form.getFrontICFileId());
        user.setBackICFileId(form.getBackICFileId());
        user.setPhotoFileId(form.getPhotoFileId());
        userDao.save(user);

        return user;
    }

    public Page<User> list(UserListForm userListForm, Pageable pageable) {
        if (userListForm.getSearch() == null)
            userListForm.setSearch("");

        if (userListForm.getStatus() == null)
            return userDao.findAll(pageable, userListForm.getSearch());

        return userDao.findAll(pageable, userListForm.getSearch(), Integer.parseInt(userListForm.getStatus()));
    }

    public Page<User> listDriver(UserListForm userListForm, Pageable pageable) {
        return userDao.findDriver(pageable, userListForm.getSearch());
    }
    public Page<User> listCustomer(UserListForm userListForm, Pageable pageable) {
        if (userListForm.getSearch() == null)
            userListForm.setSearch("");

        if (userListForm.getStatus() == null)
            return userDao.findCustomer(pageable, userListForm.getSearch());

        return userDao.findCustomer(pageable, userListForm.getSearch(), Integer.parseInt(userListForm.getStatus()));
    }

    @Transactional
    public User approve(String userId) throws Exception {
        Optional<User> optionalUser = userDao.findById(userId);
        if(!optionalUser.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        User user = optionalUser.get();
        user.setStatus(User.STATUS_VERIFIED);
        userDao.save(user);

        return user;
    }
    @Transactional
    public User delete(String userId) throws Exception {
        Optional<User> optionalUser = userDao.findById(userId);
        if(!optionalUser.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        User user = optionalUser.get();
        user.setStatus(User.STATUS_DELETED);
        userDao.save(user);

        return user;
    }

    public User getUserByIdAuthentication(String userId, UserAuthenticationForm form) throws Exception{
        Optional<User> optionalUser = userDao.findById(userId);
        if (!optionalUser.isPresent()){
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        if (!form.isAdmin()){
            User user = new User();
            user.setFirstName(optionalUser.get().getFirstName());
            user.setLastName(optionalUser.get().getLastName());
            user.setPhotoFileId(optionalUser.get().getPhotoFileId());
            return user;
        }
        return optionalUser.get();
    }

    public Page<User> listAllUsers(Pageable pageable) {
        return userDao.findAll(pageable);
    }

    public String generateUsername(String firstName, String lastName) {
        String baseUsername = firstName.toLowerCase();
        if(lastName != null) {
            baseUsername += lastName.toLowerCase();
        }
        baseUsername = baseUsername.replaceAll("\\s+", ".");
        Optional<User> optionalUser = userDao.findByUsername(baseUsername);
        if(!optionalUser.isPresent()) {
            return baseUsername;
        }

        return  baseUsername + "." + System.currentTimeMillis();

    }

    public String getUserName(String userId){
        Optional<User> userOptional =  userDao.findById(userId);
        return userOptional.map(User::getUsername).orElse(null);
    }
}