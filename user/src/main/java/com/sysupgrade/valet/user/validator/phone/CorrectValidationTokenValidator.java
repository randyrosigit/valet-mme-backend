package com.sysupgrade.valet.user.validator.phone;

import com.sysupgrade.valet.user.model.phone.RegisterByPhoneForm;
import com.sysupgrade.valet.user.service.PhoneService;
import com.sysupgrade.valet.user.orm.UserPhoneValidation;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class CorrectValidationTokenValidator
        implements ConstraintValidator<CorrectValidationTokenConstraint, RegisterByPhoneForm> {

    @Autowired
    private PhoneService phoneService;

    @Override
    public boolean isValid(RegisterByPhoneForm form, ConstraintValidatorContext context) {
        Optional<UserPhoneValidation> userPhoneValidationOptional =
                phoneService.getToken(form.getPhone(), form.getPhoneCode());

        if(!userPhoneValidationOptional.isPresent()) {
            return false;
        }
        return  userPhoneValidationOptional.get().getValidationToken().equals(form.getValidationToken());
    }
}
