package com.sysupgrade.valet.user.interfaces;


import com.sysupgrade.valet.user.dao.UserDao;
import com.sysupgrade.valet.user.model.user.UserAuthenticationForm;
import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.orm.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class AuthenticationFacade implements IAuthenticationFacade {

    private final UserDao userDao;

    @Autowired
    public AuthenticationFacade(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public User getUserByToken(String token) {
        Optional<User> userOptional = userDao.findByAuthKey(token);
        return userOptional.orElse(null);
    }

    @Override
    public User getMyFace() {
        Optional<User> userOptional = userDao.findByUsername(getAuthentication().getName());
        return userOptional.orElse(null);
    }

    @Override
    public UserAuthenticationForm getUserAuthenticationForm() {
        Optional<User> userOptional = userDao.findByUsername(getAuthentication().getName());
        if (!userOptional.isPresent()){
            return null;
        }

        boolean admin = false;

        User user = userOptional.get();

        for (UserRole role : user.getRoleList()){
            if (role.getName().equals("ADMIN")){
                admin = true;
            }
        }

        UserAuthenticationForm form = new UserAuthenticationForm();
        form.setAdmin(admin);
        form.setUser(user);
        return form;
    }
}