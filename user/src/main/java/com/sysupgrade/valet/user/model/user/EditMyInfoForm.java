package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EditMyInfoForm {

    @NotNull
    private String userId;

    @NotNull
    private String firstName;

    private String frontICFileId;

    private String backICFileId;

    private String photoFileId;

    private String address;

    private String postalCode;

    private String phone;

    private String country;

    private String error;

    private User user;

}
