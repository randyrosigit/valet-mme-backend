package com.sysupgrade.valet.user.service;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.dao.*;
import com.sysupgrade.valet.user.model.email.GetEmailForm;
import com.sysupgrade.valet.user.model.email.LoginByEmailForm;
import com.sysupgrade.valet.user.model.email.RegisterByEmailForm;
import com.sysupgrade.valet.user.orm.*;
import net.bytebuddy.utility.RandomString;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EmailService {

    @Autowired
    private UserEmailDao emailDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserPhoneAuthenticationDao userPhoneAuthenticationDao;

    @Autowired
    private UserEmailValidationDao emailValidationDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private UserUsersRoleDao userUsersRoleDao;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserService userService;

    @Autowired
    private Environment environment;

    @Transactional
    public User login(LoginByEmailForm form) throws Exception {
        Optional<UserEmailAuthentication> optionalUserEmailAuthentication = emailDao.findByEmail(form.getEmail());
        if(!optionalUserEmailAuthentication.isPresent()) {
            throw new Exception(ErrorHelper.OLD_PASSWORD_WRONG_USER);
        }

        Optional<User> userOptional = userDao.findById(optionalUserEmailAuthentication.get().getUserId());
        if(!userOptional.isPresent()) {
            throw new Exception(ErrorHelper.OLD_PASSWORD_WRONG_USER);
        }

        User user = userOptional.get();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean passwordEquals = encoder.matches(form.getPassword(), user.getPassword());
        if (!passwordEquals) {
            throw new Exception(ErrorHelper.OLD_PASSWORD_WRONG_USER);
        }

        return user;
    }

    public UserEmailAuthentication getEmail(GetEmailForm form) throws Exception {
        Optional<UserEmailAuthentication> optionalUserEmailAuthentication = emailDao.findByEmail(form.getEmail());
        if(!optionalUserEmailAuthentication.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        return optionalUserEmailAuthentication.get();
    }

    public Optional<UserEmailAuthentication> findByEmail(String email){
        return emailDao.findByEmail(email);
    }


    @Transactional
    public User register(RegisterByEmailForm form) throws Exception {
        // save user
        String username = userService.generateUsername(form.getFirstName(), form.getLastName());
        User user = new User();
        user.setFirstName(form.getFirstName());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setAuthKey(String.valueOf(UUID.randomUUID()));
        user.setEnabled(true);
        user.setUsername(username);
        user.setStatus(User.STATUS_ACTIVE);
        user.getRoleList().add(userRoleDao.findByName(UserRole.ROLE_USER));
        userDao.save(user);

        String code = RandomString.make(6).toLowerCase();
        userDao.generateReferralCode(user.getId(), code, String.valueOf(UUID.randomUUID()));

        // save email
        UserEmailAuthentication emailAuthentication = new UserEmailAuthentication();
        emailAuthentication.setEmail(form.getEmail());
        emailAuthentication.setUserId(user.getId());
        emailAuthentication.setStatus(UserEmailAuthentication.STATUS_PENDING);
        emailDao.save(emailAuthentication);

        // save phone
        if(form.getPhone() != null) {
            UserPhoneAuthentication userPhoneAuthentication = new UserPhoneAuthentication();
            userPhoneAuthentication.setPhone(form.getPhone());
            userPhoneAuthentication.setPhoneCode(form.getPhoneCode());
            userPhoneAuthentication.setUserId(user.getId());
            userPhoneAuthentication.setStatus(UserPhoneAuthentication.STATUS_ACTIVE);
            userPhoneAuthenticationDao.save(userPhoneAuthentication);
        }

        UserEmailValidation emailValidation = createEmailValidationData(emailAuthentication);
        sendValidationEmail(emailAuthentication.getEmail(), emailValidation.getValidationToken(), user.getFirstName());
        return user;
    }

    @Transactional
    public User driverRegister(RegisterByEmailForm form) throws Exception {
        // save user
        String username = userService.generateUsername(form.getFirstName(), form.getLastName());
        User user = new User();
        user.setFirstName(form.getFirstName());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setAuthKey(String.valueOf(UUID.randomUUID()));
        user.setEnabled(true);
        user.setUsername(username);
        user.setStatus(User.STATUS_ACTIVE);
        user.getRoleList().add(userRoleDao.findByName(UserRole.ROLE_DRIVER));
        userDao.save(user);

//        String code = RandomString.make(6).toLowerCase();
//        userDao.generateReferralCode(user.getId(), code, String.valueOf(UUID.randomUUID()));

        // save email
        UserEmailAuthentication emailAuthentication = new UserEmailAuthentication();
        emailAuthentication.setEmail(form.getEmail());
        emailAuthentication.setUserId(user.getId());
        emailAuthentication.setStatus(UserEmailAuthentication.STATUS_PENDING);
        emailDao.save(emailAuthentication);

        // save phone
        if(form.getPhone() != null) {
            UserPhoneAuthentication userPhoneAuthentication = new UserPhoneAuthentication();
            userPhoneAuthentication.setPhone(form.getPhone());
            userPhoneAuthentication.setPhoneCode(form.getPhoneCode());
            userPhoneAuthentication.setUserId(user.getId());
            userPhoneAuthentication.setStatus(UserPhoneAuthentication.STATUS_ACTIVE);
            userPhoneAuthenticationDao.save(userPhoneAuthentication);
        }

        UserEmailValidation emailValidation = createEmailValidationData(emailAuthentication);
        sendValidationEmail(emailAuthentication.getEmail(), emailValidation.getValidationToken(), user.getFirstName());
        return user;
    }

    @Transactional
    public User createUser(RegisterByEmailForm form) throws Exception {
        // save user
        String username = userService.generateUsername(form.getFirstName(), form.getLastName());
        User user = new User();
        user.setFirstName(form.getFirstName());
//        user.setDriverPriority(form.getDriverPriority());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setAuthKey(String.valueOf(UUID.randomUUID()));
        user.setEnabled(true);
        user.setUsername(username);
        user.setStatus(User.STATUS_ACTIVE);
        user.getRoleList().add(userRoleDao.findByName(form.getUserRole()));
        userDao.save(user);

        String code = RandomString.make(6).toLowerCase();
        userDao.generateReferralCode(user.getId(), code, String.valueOf(UUID.randomUUID()));

//        UserUsersRole userUsersRole = new UserUsersRole();
//        userUsersRole.setUserId(user.getId());
//        userUsersRole.setRoleId(form.getUserRole());
//        userUsersRole.setDriverPriority(form.getDriverPriority());
         userUsersRoleDao.updateDriverPriority(form.getDriverPriority(), user.getId(), form.getUserRole());

        // save email
        UserEmailAuthentication emailAuthentication = new UserEmailAuthentication();
        emailAuthentication.setEmail(form.getEmail());
        emailAuthentication.setUserId(user.getId());
        emailAuthentication.setStatus(UserEmailAuthentication.STATUS_PENDING);
        emailDao.save(emailAuthentication);

        UserEmailValidation emailValidation = createEmailValidationData(emailAuthentication);
        sendValidationEmail(emailAuthentication.getEmail(), emailValidation.getValidationToken(), user.getFirstName());
        return user;
    }

    public List<UserEmailAuthentication> listEmailByUserId(String userId) throws Exception{
        List<UserEmailAuthentication> list=  emailDao.findByUserId(userId);
        if (list.size() <= 0){
            throw new Exception("user not valid email");
        }
        return list;
    }

    public void validateToken(String email, String validationToken) throws Exception {
        Optional<UserEmailValidation> emailValidationOptional = emailValidationDao.findByEmail(email);
        if(!emailValidationOptional.isPresent()) {
            throw new Exception(ErrorHelper.VALIDATION_TOKEN_IS_NOT_CORRECT_USER);
        }
        UserEmailValidation emailValidation = emailValidationOptional.get();
        if(!emailValidation.getValidationToken().equals(validationToken)) {
            throw new Exception(ErrorHelper.VALIDATION_TOKEN_IS_NOT_CORRECT_USER);
        }
        LoggerFactory.getLogger(EmailService.class).debug(email + " " + validationToken);
        Optional<UserEmailAuthentication> emailAuthenticationOptional =
                emailDao.findByEmail(email);
        if(!emailAuthenticationOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }

        UserEmailAuthentication emailAuthentication = emailAuthenticationOptional.get();
        emailAuthentication.setStatus(UserEmailAuthentication.STATUS_ACTIVE);
        emailDao.save(emailAuthentication);
    }

    @Transactional
    public String getUserIdFromCode(String email, String code) throws Exception {
        Optional<UserEmailAuthentication> emailAuthenticationOptional =
                emailDao.findByEmail(email);
        if(!emailAuthenticationOptional.isPresent()) {
            throw new Exception(ErrorHelper.VALIDATION_TOKEN_IS_NOT_CORRECT_USER);
        }

        UserEmailAuthentication emailObject = emailAuthenticationOptional.get();
        if(!emailObject.getPasswordResetToken().equals(code)) {
            throw new Exception(ErrorHelper.VALIDATION_TOKEN_IS_NOT_CORRECT_USER);
        }

        return emailObject.getUserId();
    }

    @Transactional
    public void forgotPassword(String email) throws Exception {
        Optional<UserEmailAuthentication> emailAuthenticationOptional =
                emailDao.findByEmail(email);
        if(!emailAuthenticationOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }
        String passwordResetToken = generateToken();
        UserEmailAuthentication emailAuthentication =
                emailAuthenticationOptional.get();

        Optional<User> userOptional =
                userDao.findById(emailAuthentication.getUserId());
        if(!userOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }
        emailAuthentication.setPasswordResetToken(passwordResetToken);
        emailDao.save(emailAuthentication);
        sendForgotPasswordEmail(email, passwordResetToken, userOptional.get().getFirstName());
    }

    public String getEmailByUserId(String userId) {
        Optional<UserEmailAuthentication> optional = emailDao.findByUserIdLimit(userId);
        if (!optional.isPresent()) return "empty";
        return optional.get().getEmail();
    }

    public void sendForgotPasswordEmail(String email, String forgotPasswordCode, String userName) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();

        mail.setTo(email);
        mail.setFrom(environment.getProperty("admin.noreplyemail"));
        mail.setSubject("Forgot Password Token");
        mail.setText("Hi " + userName + "\n" +
                "\n" +
                "Your Forgot Password Code: " + forgotPasswordCode);
        javaMailSender.send(mail);

    }

    public void sendValidationEmail(String email, String validationToken, String userName) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();

        mail.setTo(email);
        mail.setFrom(environment.getProperty("admin.noreplyemail"));
        mail.setSubject("Your email needs to be validated");
        mail.setText("Hi " + userName + "\n" +
            "\n" +
            "Please validate your email by clicking this link \n" +
            this.getValidationLink(email, validationToken) +
            "");
        javaMailSender.send(mail);
    }

    private String getValidationLink(String email, String validationToken) {
        String currencyUrl = environment.getProperty("api.currentUrl");
        return currencyUrl + "/" + email + "/" + validationToken;
    }

    private String generateToken() {
        return ((int)((Math.random() * 899999)+100000)) + "";
    }

    private UserEmailValidation createEmailValidationData(UserEmailAuthentication userEmail) {
        String validationToken = generateToken();

        UserEmailValidation emailValidation = new UserEmailValidation();
        emailValidation.setEmail(userEmail.getEmail());
        emailValidation.setStatus(UserPhoneValidation.STATUS_ACTIVE);
        emailValidation.setValidationToken(validationToken);
        emailValidationDao.save(emailValidation);
        return emailValidation;
    }

}
