package com.sysupgrade.valet.user.interfaces;

import com.sysupgrade.valet.user.model.user.UserAuthenticationForm;
import com.sysupgrade.valet.user.orm.User;
import org.springframework.security.core.Authentication;

public interface IAuthenticationFacade {
    Authentication getAuthentication();
    User getUserByToken(String token);
    User getMyFace();
    UserAuthenticationForm getUserAuthenticationForm();
}
