package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.validator.user.OldPasswordCorrectConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@OldPasswordCorrectConstraint
public class ChangePasswordForm {

    @NotNull
    private String userId;

    @NotNull
    private String oldPassword;

    @NotNull
    private String newPassword;

    private String error;

    private User user;
}
