package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.validator.phone.CorrectValidationTokenConstraint;
import com.sysupgrade.valet.user.validator.phone.IPhone;
import com.sysupgrade.valet.user.validator.phone.PhoneNotRegisteredConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
//@PhoneNotRegisteredConstraint
//@CorrectValidationTokenConstraint
//public class RegisterByPhoneForm implements IPhone{
public class RegisterByPhoneForm{

    @NotNull
    String phoneCode;

    @NotNull
    String phone;

    @NotNull
    String firstName;

    String lastName;
    String email;

    @NotNull
    String password;

//    @NotNull
    String validationToken;

    User user;

    String error;
}
