package com.sysupgrade.valet.user.validator.phone;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PhoneNotRegisteredValidator.class)
public @interface PhoneNotRegisteredConstraint {
    String message() default ErrorHelper.PHONE_NUMBER_HAS_BEEN_REGISTERED_USER;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
