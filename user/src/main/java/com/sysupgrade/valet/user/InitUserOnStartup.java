package com.sysupgrade.valet.user;

import com.sysupgrade.valet.user.service.PhoneService;
import com.sysupgrade.valet.user.dao.UserDao;
import com.sysupgrade.valet.user.dao.UserPhoneAuthenticationDao;
import com.sysupgrade.valet.user.dao.UserRoleDao;
import com.sysupgrade.valet.user.orm.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InitUserOnStartup implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(InitUserOnStartup.class);

    private final UserDao userDao;

    @Autowired
    private UserPhoneAuthenticationDao userPhoneAuthenticationDao;

    @Autowired
    private PhoneService phoneService;

    private final UserRoleDao userRoleDao;

    @Autowired
    public InitUserOnStartup(UserDao userDao, UserRoleDao userRoleDao) {
        this.userDao = userDao;
        this.userRoleDao = userRoleDao;
    }

    private void createRole() {
        Optional<UserRole> userRoleOptionalAdmin = userRoleDao.findById(UserRole.ROLE_ADMIN);
        if (!userRoleOptionalAdmin.isPresent()) {
            UserRole role = new UserRole(UserRole.ROLE_ADMIN);
            userRoleDao.save(role);
        }

        Optional<UserRole> userRoleOptionalUser = userRoleDao.findById(UserRole.ROLE_USER);
        if (!userRoleOptionalUser.isPresent()) {
            UserRole role = new UserRole(UserRole.ROLE_USER);
            userRoleDao.save(role);
        }

        Optional<UserRole> userRoleOptionalDriver = userRoleDao.findById(UserRole.ROLE_DRIVER);
        if (!userRoleOptionalDriver.isPresent()) {
            UserRole role = new UserRole(UserRole.ROLE_DRIVER);
            userRoleDao.save(role);
        }
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        createRole();
        this.phoneService.registerAdmin();

    }

}