package com.sysupgrade.valet.user.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "user_phone_validation")
@Data
public class UserPhoneValidation {

    public static final int STATUS_ACTIVE = 10;

    public static final int STATUS_DELETED = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "phone")
    private String phone;

    @Column(name = "phone_code")
    private String phoneCode;

    @Column(name = "validation_token")
    private String validationToken;

    @Column(name = "status")
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;
}
