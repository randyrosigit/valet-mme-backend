package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.orm.UserPhoneAuthentication;
import lombok.Data;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

@Data
public class ListByUserForm {

    @NotNull
    private String userId;

    private String error;

    private Page<UserPhoneAuthentication> phoneAuthenticationPage;
}
