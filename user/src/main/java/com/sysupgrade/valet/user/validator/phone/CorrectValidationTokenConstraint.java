package com.sysupgrade.valet.user.validator.phone;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CorrectValidationTokenValidator.class)
public @interface CorrectValidationTokenConstraint {
    String message() default ErrorHelper.VALIDATION_TOKEN_IS_NOT_CORRECT_USER;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
