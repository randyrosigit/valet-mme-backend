package com.sysupgrade.valet.user.validator;

import com.sysupgrade.valet.user.dao.UserDao;
import com.sysupgrade.valet.user.orm.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;


public class NotUserValidator implements ConstraintValidator<NotUserConstraint, IUser> {

    @Autowired
    private UserDao userDao;

    @Override
    public boolean isValid(IUser iUser, ConstraintValidatorContext context) {

        String phoneNumber = iUser.getPhone();

        if (iUser.getPhone() != null && iUser.getPhone().matches("[0-9]+")
                && (iUser.getPhone().length() > 8)) {
            iUser.getPhone();
            return true;
        }

        Optional<User> optionalUser = userDao.findByUsername(phoneNumber);
        return !optionalUser.isPresent();
    }
}