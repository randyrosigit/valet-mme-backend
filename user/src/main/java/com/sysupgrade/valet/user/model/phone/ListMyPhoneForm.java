package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.orm.UserPhoneAuthentication;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ListMyPhoneForm {

    private String error;

    private Page<UserPhoneAuthentication> phoneAuthenticationList;
}
