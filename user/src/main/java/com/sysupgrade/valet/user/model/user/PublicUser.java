package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

@Data
public class PublicUser {

    private String id;

    private String firstName;

    private String photoFileId;

    private String lastName;

    private int status;

    public static PublicUser convert(User user) {
        PublicUser publicUser = new PublicUser();
        publicUser.setId(user.getId());
        publicUser.setFirstName(user.getFirstName());
        publicUser.setLastName(user.getLastName());
        publicUser.setPhotoFileId(user.getPhotoFileId());
        publicUser.setStatus(user.getStatus());
        return publicUser;
    }
}
