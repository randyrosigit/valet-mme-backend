package com.sysupgrade.valet.user.model.email;

import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.validator.email.EmailNotRegisteredConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@EmailNotRegisteredConstraint
public class RegisterByEmailForm {

    @NotNull
    @Pattern(regexp ="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$", message="Email should be valid")
    String email;

    @NotNull
    String firstName;

    String lastName;

    @NotNull
    String password;

    String userRole;
    String driverPriority;


    String phoneCode;

    String phone;

    User user;

    String error;
}
