package com.sysupgrade.valet.user.service;

import com.sysupgrade.valet.user.config.AdminConfig;
import com.sysupgrade.valet.user.dao.*;
import com.sysupgrade.valet.user.model.phone.GetPhoneCodeForm;
import com.sysupgrade.valet.user.model.phone.LoginByPhoneForm;
import com.sysupgrade.valet.user.model.phone.RegisterByPhoneForm;
import com.sysupgrade.valet.user.model.phone.SendResetPasswordToken;
import com.sysupgrade.valet.user.orm.*;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.dao.*;
import com.sysupgrade.valet.user.orm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class PhoneService {

    @Autowired
    private UserPhoneValidationDao userPhoneValidationDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserPhoneAuthenticationDao userPhoneAuthenticationDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired private UserEmailDao emailDao;

    @Autowired
    private Environment environment;

    @Autowired private UserService userService;

    @Transactional
    public void setResetPasswordTokenAndSend(SendResetPasswordToken form) throws Exception {
        Optional<UserPhoneAuthentication> userPhoneAuthenticationOptional =
                userPhoneAuthenticationDao.findByPhoneAndPhoneCode(form.getPhone(), form.getPhoneCode());
        if(!userPhoneAuthenticationOptional.isPresent()) {
            throw new Exception(ErrorHelper.DATA_NOT_FOUND);
        }
        String validationToken = generateValidationToken();

        UserPhoneAuthentication userPhoneAuthentication = userPhoneAuthenticationOptional.get();
        userPhoneAuthentication.setPasswordResetToken(validationToken);
        userPhoneAuthenticationDao.save(userPhoneAuthentication);

    }

    @Transactional
    public UserPhoneValidation createPhoneValidationData(UserPhoneAuthentication form) {
//    public UserPhoneValidation setTokenAndSend(GetPhoneCodeForm form) {
        String validationToken = generateValidationToken();

        UserPhoneValidation userPhoneValidation = new UserPhoneValidation();
        userPhoneValidation.setPhone(form.getPhone());
        userPhoneValidation.setPhoneCode(form.getPhoneCode());
        userPhoneValidation.setStatus(UserPhoneValidation.STATUS_ACTIVE);
        userPhoneValidation.setValidationToken(validationToken);
        userPhoneValidationDao.save(userPhoneValidation);

        return  userPhoneValidation;
    }

    @Transactional
    public void setTokenAndSend(GetPhoneCodeForm form) {
        String validationToken = generateValidationToken();

        UserPhoneValidation userPhoneValidation = new UserPhoneValidation();
        userPhoneValidation.setPhone(form.getPhone());
        userPhoneValidation.setPhoneCode(form.getPhoneCode());
        userPhoneValidation.setStatus(UserPhoneValidation.STATUS_ACTIVE);
        userPhoneValidation.setValidationToken(validationToken);
        userPhoneValidationDao.save(userPhoneValidation);
    }

    @Transactional
    public User register(RegisterByPhoneForm form) {
        // save user
        String username = userService.generateUsername(form.getFirstName(), form.getLastName());
        User user = new User();
        user.setFirstName(form.getFirstName());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setAuthKey(String.valueOf(UUID.randomUUID()));
        user.setEnabled(true);
        user.setUsername(username);
        user.setStatus(User.STATUS_ACTIVE);
        user.getRoleList().add(userRoleDao.findByName(UserRole.ROLE_USER));
        userDao.save(user);


        // save phone
        UserPhoneAuthentication userPhoneAuthentication = new UserPhoneAuthentication();
        userPhoneAuthentication.setPhone(form.getPhone());
        userPhoneAuthentication.setPhoneCode(form.getPhoneCode());
        userPhoneAuthentication.setUserId(user.getId());
        userPhoneAuthentication.setStatus(UserPhoneAuthentication.STATUS_ACTIVE);
        userPhoneAuthenticationDao.save(userPhoneAuthentication);

        // save email
        UserEmailAuthentication emailAuthentication = new UserEmailAuthentication();
        emailAuthentication.setEmail(form.getEmail());
        emailAuthentication.setUserId(user.getId());
        emailAuthentication.setStatus(UserEmailAuthentication.STATUS_PENDING);
//        emailDao.save(emailAuthentication);



        UserPhoneValidation phoneValidation = createPhoneValidationData(userPhoneAuthentication);
//        sendValidationEmail(emailAuthentication.getEmail(), emailValidation.getValidationToken(), user.getFirstName());

        return user;
    }

    @Transactional
    public Optional<UserPhoneValidation> getToken(String phone, String phoneCode) {
        return this.userPhoneValidationDao.findByPhoneAndCode(phone, phoneCode);
    }

    @Transactional
    public Optional<UserPhoneAuthentication> getByPhoneAndPhoneCode(String phone, String phoneCode) {
        return this.userPhoneAuthenticationDao.findByPhoneAndPhoneCode(phone, phoneCode);
    }

    @Transactional
    public Page<UserPhoneAuthentication> findAllByUserId(String userId, Pageable pageable) {
        return this.userPhoneAuthenticationDao.findAllByUserId(userId, pageable);
    }

    @Transactional
    public User login(LoginByPhoneForm form) throws Exception {
        Optional<UserPhoneAuthentication> userPhoneAuthenticationOptional =
                userPhoneAuthenticationDao.findByPhoneAndPhoneCode(form.getPhone(), form.getPhoneCode());
        if(!userPhoneAuthenticationOptional.isPresent()) {
            throw new Exception(ErrorHelper.OLD_PASSWORD_WRONG_USER);
        }

        Optional<User> userOptional = userDao.findById(userPhoneAuthenticationOptional.get().getUserId());
        if(!userOptional.isPresent()) {
            throw new Exception(ErrorHelper.OLD_PASSWORD_WRONG_USER);
        }

        User user = userOptional.get();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean passwordEquals = encoder.matches(form.getPassword(), user.getPassword());
        if (!passwordEquals) {
            throw new Exception(ErrorHelper.OLD_PASSWORD_WRONG_USER);
        }

        return user;
    }


    @Transactional
    public void registerAdmin() {

        String email = environment.getProperty("admin.email");
        String username = AdminConfig.USERNAME;
        String id = AdminConfig.ADMIN_ID;
        Date currentTime = new Date();
        if(userDao.findByUsername(username).isPresent()) {
            return;
        }
        String phone = environment.getProperty("admin.phone");
        String password = environment.getProperty("admin.password");
//        String phoneCode = environment.getProperty("admin.phone_code");

        // save user
        this.userDao.save(id,
                User.STATUS_ACTIVE,
                currentTime,
                currentTime,
                String.valueOf(UUID.randomUUID()),
                "Admin",
                null,
                username,
                true,
                new BCryptPasswordEncoder().encode(password));

        // save roles
        Optional<User> optionalUser = userDao.findById(id);
        if(optionalUser.isPresent()) {
            User userAdmin = optionalUser.get();
            userAdmin.getRoleList().add(userRoleDao.findByName(UserRole.ROLE_USER));
            userAdmin.getRoleList().add(userRoleDao.findByName(UserRole.ROLE_ADMIN));
            userAdmin.getRoleList().add(userRoleDao.findByName(UserRole.ROLE_DRIVER));
            userDao.save(userAdmin);
        }

        // save phone
        UserPhoneAuthentication userPhoneAuthentication = new UserPhoneAuthentication();
        userPhoneAuthentication.setPhone(phone);
//        userPhoneAuthentication.setPhoneCode(phoneCode);
        userPhoneAuthentication.setUserId(id);
        userPhoneAuthentication.setStatus(UserPhoneAuthentication.STATUS_ACTIVE);
        userPhoneAuthenticationDao.save(userPhoneAuthentication);

        //save email;
        UserEmailAuthentication emailAuthentication =  new UserEmailAuthentication();
        emailAuthentication.setEmail(email);
        emailAuthentication.setUserId(id);
        emailAuthentication.setStatus(UserEmailAuthentication.STATUS_ACTIVE);
        emailDao.save(emailAuthentication);
    }


    private String generateValidationToken() {
        return ((int)((Math.random() * 899999)+100000)) + "";
    }
}
