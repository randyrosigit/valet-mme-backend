package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.validator.phone.IPhone;
import com.sysupgrade.valet.user.validator.phone.PhoneNotRegisteredConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@PhoneNotRegisteredConstraint
public class GetPhoneCodeForm implements IPhone {

    @NotNull
    String phoneCode;

    @NotNull
    String phone;

    String error;
}
