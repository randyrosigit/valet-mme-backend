package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.validator.phone.CorrectResetPasswordTokenConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@CorrectResetPasswordTokenConstraint
public class ResetPasswordForm {

    @NotNull
    private String phone;

    @NotNull
    private String phoneCode;

    @NotNull
    private String passwordResetToken;

    @NotNull
    private String newPassword;

    private String error;

    private User user;


}
