package com.sysupgrade.valet.user.validator.phone;

import com.sysupgrade.valet.user.model.phone.ResetPasswordForm;
import com.sysupgrade.valet.user.service.PhoneService;
import com.sysupgrade.valet.user.orm.UserPhoneAuthentication;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class CorrectResetPasswordTokenValidator
        implements ConstraintValidator<CorrectResetPasswordTokenConstraint, ResetPasswordForm> {

    @Autowired
    private PhoneService phoneService;

    @Override
    public boolean isValid(ResetPasswordForm form, ConstraintValidatorContext context) {
        Optional<UserPhoneAuthentication> userPhoneAuthenticationOptional =
                phoneService.getByPhoneAndPhoneCode(form.getPhone(), form.getPhoneCode());

        return userPhoneAuthenticationOptional.map(userPhoneAuthentication -> userPhoneAuthentication
                .getPasswordResetToken().equals(form.getPasswordResetToken())).orElse(false);
    }
}
