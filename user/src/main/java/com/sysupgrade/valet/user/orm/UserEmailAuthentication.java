package com.sysupgrade.valet.user.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_email_authentication")
@Data
public class UserEmailAuthentication {

    public static final int STATUS_ACTIVE = 10;

    public static final int STATUS_PENDING = 11;

    public static final int STATUS_DELETED = 0;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "validation_token")
    private String validationToken;

    @Column(name = "reset_token")
    private String resetToken;

    @Column(name = "password_reset_token")
    private String passwordResetToken;

    @Column(name = "primary_email")
    private boolean primaryEmail;

    private String email;

    private boolean valid;

    @Column(name = "status")
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;
}
