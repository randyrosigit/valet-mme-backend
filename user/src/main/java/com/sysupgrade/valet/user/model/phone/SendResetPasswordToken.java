package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.validator.phone.IPhone;
import com.sysupgrade.valet.user.validator.phone.PhoneRegisteredConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@PhoneRegisteredConstraint
public class SendResetPasswordToken implements IPhone {

    @NotNull
    private String phoneCode;

    @NotNull
    private String phone;

    private String error;
}
