package com.sysupgrade.valet.user.validator.user;

import com.sysupgrade.valet.library.config.helper.ErrorHelper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = OldPasswordCorrectValidator.class)
public @interface OldPasswordCorrectConstraint {
    String message() default ErrorHelper.OLD_PASSWORD_WRONG_USER;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
