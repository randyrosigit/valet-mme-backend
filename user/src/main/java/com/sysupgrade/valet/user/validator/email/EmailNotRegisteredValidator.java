package com.sysupgrade.valet.user.validator.email;

import com.sysupgrade.valet.user.model.email.RegisterByEmailForm;
import com.sysupgrade.valet.user.service.EmailService;
import com.sysupgrade.valet.user.orm.UserEmailAuthentication;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class EmailNotRegisteredValidator implements ConstraintValidator<EmailNotRegisteredConstraint, RegisterByEmailForm> {

    @Autowired private EmailService emailService;

    @Override
    public boolean isValid(RegisterByEmailForm form, ConstraintValidatorContext context) {

        Optional<UserEmailAuthentication> emailAuthenticationOptional = emailService.findByEmail(form.getEmail());

        return !emailAuthenticationOptional.isPresent();

    }
}

