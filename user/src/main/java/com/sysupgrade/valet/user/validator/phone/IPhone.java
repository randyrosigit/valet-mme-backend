package com.sysupgrade.valet.user.validator.phone;

public interface IPhone {

    public String getPhoneCode();

    public String getPhone();
}
