package com.sysupgrade.valet.user.validator.email;


import com.sysupgrade.valet.library.config.helper.ErrorHelper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailNotRegisteredValidator.class)
public @interface EmailNotRegisteredConstraint {

    String message() default ErrorHelper.EMAIL_HAS_BEEN_REGISTERED_USER;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
