package com.sysupgrade.valet.user.orm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_role")
@Data
public class UserRole {

    public static final String ROLE_ADMIN = "ADMIN";

    public static final String ROLE_USER = "USER";

    public static final String ROLE_DRIVER = "DRIVER";

    public UserRole() {
    }

    public UserRole(String name) {
        this.name = name;
    }

    @Id
    private String name;

    private String description;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Timestamp updatedAt;

    @JsonIgnore
    @ManyToMany(mappedBy = "roleList")
    private List<User> userList = new ArrayList<>();
}