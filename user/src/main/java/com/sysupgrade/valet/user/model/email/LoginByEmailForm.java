package com.sysupgrade.valet.user.model.email;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginByEmailForm {

    @NotNull
    String email;

    @NotNull
    String password;

    private String error;

    private User user;
}
