package com.sysupgrade.valet.user.validator.phone;

import com.sysupgrade.valet.user.dao.UserPhoneAuthenticationDao;
import com.sysupgrade.valet.user.orm.UserPhoneAuthentication;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class PhoneRegisteredValidator implements ConstraintValidator<PhoneRegisteredConstraint, IPhone> {

    @Autowired
    private UserPhoneAuthenticationDao userPhoneAuthenticationDao;

    @Override
    public boolean isValid(IPhone iPhone, ConstraintValidatorContext context) {
        Optional<UserPhoneAuthentication> userPhoneAuthenticationOptional =
                userPhoneAuthenticationDao.findByPhoneAndPhoneCode(iPhone.getPhone(), iPhone.getPhoneCode());
        return userPhoneAuthenticationOptional.isPresent();
    }
}