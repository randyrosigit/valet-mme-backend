package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

public interface UserDao extends PagingAndSortingRepository<User, String> {

//    Optional<User> findByPhoneAndPassword(String phone, String password);

    Optional<User> findByUsername(String username);

    Optional<User> findByAuthKey(String authKey);

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO referral_code (id, user_id, code, status) VALUES (:id ,:userId, :code, 10)")
    int generateReferralCode(@Param("userId") String userId, @Param("code") String code, @Param("id") String id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM user_account ua JOIN user_users_roles uur ON ua.id = uur.user_id " +
            "WHERE  (ua.first_name like %:search% OR ua.last_name like %:search%) AND ua.status = :status")
    Page<User> findAll(Pageable pageable, @Param("search") String search, @Param("status") int status);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM user_account ua JOIN user_users_roles uur ON ua.id = uur.user_id " +
            "WHERE (ua.first_name like %:search% OR ua.last_name like %:search%) AND ua.status != 0 ")
    Page<User> findAll(Pageable pageable, @Param("search") String search);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM user_account ua JOIN user_users_roles uur ON ua.id = uur.user_id " +
            "WHERE role_id = 'driver' AND (ua.first_name like %:search% OR ua.last_name like %:search%) ")
    Page<User> findDriver(Pageable pageable, @Param("search") String search);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM user_account ua JOIN user_users_roles uur ON ua.id = uur.user_id " +
            "WHERE role_id = 'user' AND (ua.first_name like %:search% OR ua.last_name like %:search%) AND ua.status = :status ")
    Page<User> findCustomer(Pageable pageable, @Param("search") String search, @Param("status") int status);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM user_account ua JOIN user_users_roles uur ON ua.id = uur.user_id " +
            "WHERE role_id = 'user' AND (ua.first_name like %:search% OR ua.last_name like %:search%) ")
    Page<User> findCustomer(Pageable pageable, @Param("search") String search);

    @Modifying
    @Query( nativeQuery = true,
            value = "INSERT INTO user_account (id, status, created_at, updated_at, auth_key, first_name, last_name, username, enabled, password) " +
            "           VALUES( :id, :status, :createdAt, :updatedAt, :authKey, :firstName, :lastName, :username,          " +
            "                :enabled, :password)")
    int save(@Param("id") String id,
             @Param("status") int status,
             @Param("createdAt") Date createdAt,
             @Param("updatedAt") Date updatedAt,
             @Param("authKey") String authKey,
             @Param("firstName") String firstName,
             @Param("lastName") String lastName,
             @Param("username") String username,
             @Param("enabled") boolean enabled,
             @Param("password") String password);

    @Query( nativeQuery = true,
            value = "SELECT * from user_account where id IN :userIds")
    Page<User> findAllByUserIds(@Param("userIds") Collection<String> userIds,Pageable pageable);
}
