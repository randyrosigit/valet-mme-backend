package com.sysupgrade.valet.user.validator.user;

import com.sysupgrade.valet.user.model.user.ChangePasswordForm;
import com.sysupgrade.valet.user.service.UserService;
import com.sysupgrade.valet.user.orm.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class OldPasswordCorrectValidator implements ConstraintValidator<OldPasswordCorrectConstraint, ChangePasswordForm> {

    @Autowired
    private UserService userService;

    private Logger logger = LoggerFactory.getLogger(OldPasswordCorrectValidator.class);

    @Override
    public boolean isValid(ChangePasswordForm form, ConstraintValidatorContext context) {

        Optional<User> optionalUser = userService.getById(form.getUserId());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        logger.info(optionalUser.isPresent() + " " +
                form.getOldPassword() + " " +
                optionalUser.get().getPassword());
        return optionalUser.isPresent() &&
                encoder.matches(form.getOldPassword(), optionalUser.get().getPassword());
    }
}