package com.sysupgrade.valet.user.model.email;

import lombok.Data;

@Data
public class ChangePasswordByCode {

    private String email;

    private String passwordResetToken;

    private String newPassword;

    private String error;
}
