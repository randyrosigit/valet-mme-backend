package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.UserRole;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRoleDao extends PagingAndSortingRepository<UserRole, String> {
    UserRole findByName(String name);
}
