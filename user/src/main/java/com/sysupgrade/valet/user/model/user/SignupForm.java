package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.validator.IUser;
import com.sysupgrade.valet.user.validator.NotUserConstraint;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * DTO
 */
@Data
@NotUserConstraint
public class SignupForm implements IUser {

    @NotEmpty
    private String phone;

    @NotEmpty
    @Size.List ({
            @Size(min=2, message="First name must be at least {min} characters"),
            @Size(max=60, message="First name must be less than {max} characters")
    })
    private String firstName;

    private String lastName;

    @Pattern(regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$", message = "invalid password. use a combination of lowercase, number and not less than 6 digits")
    private String password;

    private String error;

    private User user;
}
