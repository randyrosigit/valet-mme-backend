package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.UserUsersRole;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface UserUsersRoleDao extends PagingAndSortingRepository<UserUsersRole, String> {

    @Modifying
    @Query(nativeQuery = true, value = "UPDATE user_users_roles SET driver_priority=:driver_priority WHERE user_id=:user_id AND role_id=:role_id")
    Integer updateDriverPriority(@Param("driver_priority") String driverPriority, @Param("user_id") String userId, @Param("role_id") String roleId);

}
