package com.sysupgrade.valet.user.model.email;

import lombok.Data;

@Data
public class ValidateEmailForm {

    private String message;

    private String error;

}
