package com.sysupgrade.valet.user.dao;

import com.sysupgrade.valet.user.orm.UserPhoneValidation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserPhoneValidationDao extends PagingAndSortingRepository<UserPhoneValidation, String> {

    @Query(nativeQuery = true,
           value = "SELECT u.* from user_phone_validation u " +
                    "where u.phone = :phone and u.phone_code = :phone_code " +
                    "ORDER BY id DESC limit 1")
    Optional<UserPhoneValidation> findByPhoneAndCode(@Param("phone") String phone,
                                                      @Param("phone_code") String phoneCode);

}
