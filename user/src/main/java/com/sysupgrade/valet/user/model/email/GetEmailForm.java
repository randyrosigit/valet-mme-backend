package com.sysupgrade.valet.user.model.email;

import com.sysupgrade.valet.user.orm.UserEmailAuthentication;
import lombok.Data;

import java.util.List;

@Data
public class GetEmailForm {

    private String email;

    private String error;

    private String userId;

    private UserEmailAuthentication userEmailAuthentication;

    private List<UserEmailAuthentication> userEmailAuthentications;

}
