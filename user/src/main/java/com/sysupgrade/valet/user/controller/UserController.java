package com.sysupgrade.valet.user.controller;

import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import com.sysupgrade.valet.user.model.email.RegisterByEmailForm;
import com.sysupgrade.valet.user.model.user.*;
import com.sysupgrade.valet.user.service.UserService;
import com.sysupgrade.valet.user.service.EmailService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.dao.UserDao;
import com.sysupgrade.valet.user.orm.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/user/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @Autowired
    private SmartValidator validator;




    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("create")
    public ResponseEntity<RegisterByEmailForm> registerByEmail(@RequestBody @Valid RegisterByEmailForm form,
                                                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = emailService.createUser(form);
            form.setUser(user);
        } catch (Exception e) {
            form.setPassword(null);
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        form.setPassword(null);
        form.setEmail(null);
        return ResponseEntity.ok(form);
    }


    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("verify")
    public ResponseEntity<VerifyUserForm> verify(@RequestBody @Valid VerifyUserForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = userService.approve(form.getUserId());
            form.setUser(user);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("delete")
    public ResponseEntity<VerifyUserForm> delete(@RequestBody @Valid VerifyUserForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = userService.delete(form.getUserId());
            form.setUser(user);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("edit-my-info")
    public ResponseEntity<EditMyInfoForm> editMyInfo(@RequestBody EditMyInfoForm form, BindingResult bindingResult) {

        form.setUserId(authenticationFacade.getMyFace().getId());
        validator.validate(form, bindingResult);

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = userService.edit(form);
            form.setUser(user);

        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping("change-password")
    public ResponseEntity<ChangePasswordForm> changePassword(@RequestBody ChangePasswordForm form,
                                                             BindingResult bindingResult) {
        form.setUserId(authenticationFacade.getMyFace().getId());
        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = userService.changePassword(form.getUserId(), form.getNewPassword());
            form.setUser(user);

        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("")
    public ResponseEntity<UserListForm> list(@Valid UserListForm userListForm,
                                             BindingResult bindingResult,
                                             Pageable pageable) {

        if (bindingResult.hasErrors()) {
            userListForm.setError(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return ResponseEntity.badRequest().body(userListForm);
        }

        try {
            Page<User> list = userService.list(userListForm, pageable);


            userListForm.setUsers(list);

        } catch (Exception e) {
            userListForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(userListForm);
        }

        return ResponseEntity.ok(userListForm);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("list-driver")
    public ResponseEntity<UserListForm> listDriver(@Valid UserListForm userListForm,
                                             BindingResult bindingResult,
                                             Pageable pageable) {

        if (bindingResult.hasErrors()) {
            userListForm.setError(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return ResponseEntity.badRequest().body(userListForm);
        }

        try {
            Page<User> list = userService.listDriver(userListForm, pageable);
            userListForm.setUsers(list);

        } catch (Exception e) {
            userListForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(userListForm);
        }

        return ResponseEntity.ok(userListForm);
    }
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("list-customer")
    public ResponseEntity<UserListForm> listCustomer(@Valid UserListForm userListForm,
                                             BindingResult bindingResult,
                                             Pageable pageable) {

        if (bindingResult.hasErrors()) {
            userListForm.setError(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return ResponseEntity.badRequest().body(userListForm);
        }

        try {
            Page<User> list = userService.listCustomer(userListForm, pageable);
            userListForm.setUsers(list);

        } catch (Exception e) {
            userListForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(userListForm);
        }

        return ResponseEntity.ok(userListForm);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/get")
    public User getMyInfo() {
        return authenticationFacade.getMyFace();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<User> getById(@PathVariable String id) {
        UserAuthenticationForm form = authenticationFacade.getUserAuthenticationForm();

        User user = new User();

        try {
            user = userService.getUserByIdAuthentication(id, form);
        } catch (Exception e){
            return ResponseEntity.status(404).body(user);
        }

        return ResponseEntity.ok(user);

    }
}