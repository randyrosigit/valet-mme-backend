package com.sysupgrade.valet.user.model.phone;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginByPhoneForm {

    @NotNull
    String phone;

    @NotNull
    String phoneCode;

    @NotNull
    String password;

    private String error;

    private User user;
}
