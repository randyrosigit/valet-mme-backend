package com.sysupgrade.valet.user.controller;

import com.sysupgrade.valet.user.interfaces.IAuthenticationFacade;
import com.sysupgrade.valet.user.model.phone.*;
import com.sysupgrade.valet.user.service.PhoneService;
import com.sysupgrade.valet.user.service.UserService;
import com.sysupgrade.valet.library.config.helper.ErrorHelper;
import com.sysupgrade.valet.user.orm.User;
import com.sysupgrade.valet.user.orm.UserPhoneAuthentication;
import com.sysupgrade.valet.user.model.phone.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/user/phone")
public class PhoneController {

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private UserService userService;

    @Autowired
    private IAuthenticationFacade iAuthenticationFacade;

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/list-mine")
    public ResponseEntity<ListMyPhoneForm> listMine(@RequestBody ListMyPhoneForm form,
                                                    Pageable pageable,
                                                    BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            String userId = iAuthenticationFacade.getMyFace().getId();
            Page<UserPhoneAuthentication> userPhoneAuthenticationPage =
                    this.phoneService.findAllByUserId(userId, pageable);
            form.setPhoneAuthenticationList(userPhoneAuthenticationPage);
        }
        catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
        return ResponseEntity.ok(form);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/list-by-user")
    public ResponseEntity<ListByUserForm> listByUser(@Valid ListByUserForm form,
                                                     Pageable pageable,
                                                     BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            String userId = iAuthenticationFacade.getMyFace().getId();
            Page<UserPhoneAuthentication> userPhoneAuthenticationPage =
                    this.phoneService.findAllByUserId(userId, pageable);
            form.setPhoneAuthenticationPage(userPhoneAuthenticationPage);
        }
        catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }
        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("/public/send-token")
    public ResponseEntity<GetPhoneCodeForm> sendToken(@RequestBody @Valid GetPhoneCodeForm getPhoneCodeForm,
                                                      BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            getPhoneCodeForm.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(getPhoneCodeForm);
        }

        try {
            this.phoneService.setTokenAndSend(getPhoneCodeForm);
        } catch (Exception e) {
            getPhoneCodeForm.setError(e.getMessage());
            return ResponseEntity.status(500).body(getPhoneCodeForm);
        }

        return ResponseEntity.ok(getPhoneCodeForm);
    }

    @CrossOrigin("*")
    @PostMapping("/public/send-reset-password-token")
    public ResponseEntity<SendResetPasswordToken> sendResetPasswordToken(@RequestBody @Valid SendResetPasswordToken form,
                                                                         BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            this.phoneService.setResetPasswordTokenAndSend(form);

        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("/public/reset-password")
    public ResponseEntity<ResetPasswordForm> resetPassword(@RequestBody @Valid ResetPasswordForm form,
                                                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            Optional<UserPhoneAuthentication> userPhoneAuthenticationOptional =
                    phoneService.getByPhoneAndPhoneCode(form.getPhone(), form.getPhoneCode());
            if(!userPhoneAuthenticationOptional.isPresent()) {
                throw new Exception(ErrorHelper.DATA_NOT_FOUND);
            }

            UserPhoneAuthentication userPhoneAuthentication = userPhoneAuthenticationOptional.get();
            User user =
                    userService.changePassword(userPhoneAuthentication.getUserId(), form.getNewPassword());
            form.setUser(user);

        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("/public/register")
    public ResponseEntity<RegisterByPhoneForm> registerByPhone(@RequestBody @Valid RegisterByPhoneForm form,
                                                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = this.phoneService.register(form);
            form.setUser(user);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        return ResponseEntity.ok(form);
    }

    @CrossOrigin("*")
    @PostMapping("/public/login")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<LoginByPhoneForm> loginByPhone(@RequestBody @Valid LoginByPhoneForm form,
                                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            form.setError(ErrorHelper.getErrorMessage(bindingResult));
            return ResponseEntity.badRequest().body(form);
        }

        try {
            User user = this.phoneService.login(form);
            user.setPassword(null);
            form.setUser(user);
        } catch (Exception e) {
            form.setError(e.getMessage());
            return ResponseEntity.status(500).body(form);
        }

        form.setPassword(null);
        form.setPhoneCode(null);
        form.setPhone(null);
        return ResponseEntity.ok(form);
    }
}
