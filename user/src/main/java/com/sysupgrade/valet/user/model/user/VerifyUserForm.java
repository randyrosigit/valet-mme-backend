package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class VerifyUserForm {
    @NotNull
    private String userId;

    private String error;

    private User user;
}
