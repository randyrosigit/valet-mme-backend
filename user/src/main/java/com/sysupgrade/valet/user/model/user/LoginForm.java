package com.sysupgrade.valet.user.model.user;

import com.sysupgrade.valet.user.orm.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginForm {

    @NotNull
    private String phone;

    @NotNull
    private String password;

    private String error;

    private User user;

    private boolean status;
}
