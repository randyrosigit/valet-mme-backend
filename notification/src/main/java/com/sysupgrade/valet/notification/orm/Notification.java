package com.sysupgrade.valet.notification.orm;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
public class Notification {

    public final static int STATUS_SEND = 1;

    public final static int STATUS_PENDING = 11;

    public final static int STATUS_READ = 10;


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    private String name;
    @Column(name = "chinese_name")

    private String chineseName;

    private String description;

    @Column(name = "chinese_description")
    private String chineseDescription;

    private String reminder;
    @Column(name = "reminder_period")

    private String reminderPeriod;
    @Column(name = "last_reminder_period")

    private String lastReminderPeriod;

    @NotNull
    private int status;

    @CreationTimestamp
    @Column(nullable = false, updatable = false, name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;
}
