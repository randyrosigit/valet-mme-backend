package com.sysupgrade.valet.notification.service;

import com.sysupgrade.valet.notification.dao.NotificationDao;
import com.sysupgrade.valet.notification.model.NotificationForm;
import com.sysupgrade.valet.notification.orm.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class NotificationService {

    @Autowired
    private NotificationDao notificationDao;


    @Transactional
    public Notification createNotification(NotificationForm form) throws Exception {

        Notification notification = new Notification();
        notification.setUserId(form.getUserId());
        notification.setName(form.getName());
        notification.setChineseName(form.getChineseName());
        notification.setDescription(form.getDescription());
        notification.setChineseDescription(form.getChineseDescription());
        notification.setStatus(Notification.STATUS_SEND);
        return notificationDao.save(notification);
    }

    @Transactional
    public void readNotification(String userId) {
        List<Notification> list = listByPending(userId);
        for (Notification notification : list) {

            notification.setStatus(Notification.STATUS_READ);
            notificationDao.save(notification);

        }
    }

    public List<Notification> listByPending(String userId) {
        return notificationDao.findAllByUserIdAndStatus(userId, Notification.STATUS_SEND);
    }
}
