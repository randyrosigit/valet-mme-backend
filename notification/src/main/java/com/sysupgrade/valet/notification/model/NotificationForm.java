package com.sysupgrade.valet.notification.model;

import com.sysupgrade.valet.notification.orm.Notification;
import lombok.Data;

import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;

@Data
public class NotificationForm {

    private String userId;

    @NotNull
    private String name;

    @NotNull
    private String chineseName;

    @NegativeOrZero
    private String description;

    @NotNull
    private String chineseDescription;

    private Notification notification;

    private String error;
}
