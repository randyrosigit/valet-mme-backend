package com.sysupgrade.valet.notification.dao;

import com.sysupgrade.valet.notification.orm.Notification;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface NotificationDao extends PagingAndSortingRepository<Notification, String> {
    List<Notification> findAllByUserIdAndStatus(String userId, int statusSend);
}
