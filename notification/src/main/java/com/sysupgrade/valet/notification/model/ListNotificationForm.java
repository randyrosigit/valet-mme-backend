package com.sysupgrade.valet.notification.model;

import com.sysupgrade.valet.notification.orm.Notification;
import lombok.Data;

import java.util.List;

@Data
public class ListNotificationForm {

    private List<Notification> notifications;

    private String error;
}
